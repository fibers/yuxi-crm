<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
header("Content-Type: text/html; charset=UTF-8");
class TT_Controller extends CI_Controller {

	function __construct()
	{
		parent::__construct();//print_r($this->session);exit;
		if(!isset($this->session->userdata['account'])){
			$this->load->helper('url');
			redirect('/auth/login');
		}
	}
	
	public function aesLocalEncode($emsg)
	{
		$command = './encode ' . EscapeShellArg($emsg);
		$value = `$command`;
		return $value ; //$return;
	}
	
	/**
	 * 计算周时间
	 * @param int $year 年
	 * @param int $weeknum 今年第几周
	 * @return multitype:string
	 */
	function getWeekDate($year, $weeknum) {
		$firstdayofyear = mktime ( 0, 0, 0, 1, 1, $year );
		$firstweekday = date ( 'N', $firstdayofyear );
		$firstweenum = date ( 'W', $firstdayofyear );
		if ($firstweenum == 1) {
			$day = (1 - ($firstweekday - 1)) + 7 * ($weeknum - 1);
			$startdate = date ( 'Y-m-d', mktime ( 0, 0, 0, 1, $day, $year ) );
			$enddate = date ( 'Y-m-d', mktime ( 0, 0, 0, 1, $day + 6, $year ) );
		} else {
			$day = (9 - $firstweekday) + 7 * ($weeknum - 1);
			$startdate = date ( 'Y-m-d', mktime ( 0, 0, 0, 1, $day, $year ) );
			$enddate = date ( 'Y-m-d', mktime ( 0, 0, 0, 1, $day + 6, $year ) );
		}
	
		return array (
				$startdate,
				$enddate
		);
	}
	
	/**
	 * 获取指定月份的第一天开始和最后一天结束的时间戳
	 *
	 * @param int $y 年份 $m 月份
	 * @return array(本月开始时间，本月结束时间)
	 */
	function mFristAndLast($y = "", $m = ""){
		if ($y == "") $y = date("Y");
		if ($m == "") $m = date("m");
		$m = sprintf("%02d", intval($m));
		$y = str_pad(intval($y), 4, "0", STR_PAD_RIGHT);
	
		$m>12 || $m<1 ? $m=1 : $m=$m;
		$firstday = strtotime($y . $m . "01000000");
		$firstdaystr = date("Y-m-01", $firstday);
		$lastday = strtotime(date('Y-m-d 23:59:59', strtotime("$firstdaystr +1 month -1 day")));
	
		return array(
				$firstday,
				$lastday
		);
	}
	
	/**
	 * 计算年月日是否可用
	 */
	public function data_unit($start_time,$end_time,&$dataunitrue,&$dataunitfalse,&$dataunit){
		$time_len =  ceil((strtotime($end_time)-strtotime($start_time))/86400)+1;
		$hourly = true;
		if ($time_len<=1){
			$dataunitrue = array ('hourly');
			$dataunitfalse = array ('daily','weekly','monthly');
		}elseif($time_len>1 && $time_len<7){
			$dataunitrue = array ('daily');
			$dataunitfalse = array ('hourly','weekly','monthly');
			$hourly = false;
		}elseif($time_len>=7 && $time_len<=28){
			$dataunitrue = array ('daily','weekly');
			$dataunitfalse = array ('hourly','monthly');
			$hourly = false;
		}else{
			$dataunitrue = array ('daily','weekly','monthly');
			$dataunitfalse = array ('hourly');
			$hourly = false;
		}
		if(!$hourly && $hourly=='hourly'){
			$dataunit = 'daily';
		}
	}
	
	/**
	 * 获取两个时间段中间的周
	 */
	public function weekList($start_time,$end_time){
		$n = (strtotime($end_time)+3600*24-strtotime($start_time))/86400;
		//结束时间加一天(sql语句里用的是小于和大于，如果有等于的话这句可以不要)
		//$end_time = date("Y-m-d 00:00:00",strtotime("$end_time +1 day"));
		//判断，跨度小于7天，可能是同一周，也可能是两周
		if($n<7){
			//查开始时间 在 那周 的 位置
			$day            = date("w",strtotime($start_time))-1;
			//查开始时间  那周 的 周一
			$week_start        = date("Y-m-d 00:00:00",strtotime("$start_time -{$day} day"));
			//查开始时间  那周 的 周末
			$day            = 7-$day;
			$week_end        = date("Y-m-d 00:00:00",strtotime("$start_time +{$day} day"));
			//判断周末时间是否大于时间段的结束时间，如果大于，那就是时间段在同一周，否则时间段跨两周
			if($week_end>=$end_time){
				$weekList[] =array($start_time,$end_time);
			}else{
				$weekList[] =array($start_time,$week_end);
				$weekList[] =array($week_end,$end_time);
			}
		}else{
			//如果跨度大于等于7天，可能是刚好1周或跨2周或跨N周，先找出开始时间 在 那周 的 位置和那周的周末时间
			$day         = date("w",strtotime($start_time))-1;
			$week_start  = date("Y-m-d 00:00:00",strtotime("$start_time -{$day} day"));
			$day         = 7-$day;
			$week_end    = date("Y-m-d 00:00:00",strtotime("$start_time +{$day} day"));
			//先把开始时间那周写入数组
			$weekList[]  =array($start_time,$week_end);
			//判断周末是否大于等于结束时间，不管大于(2周)还是等于(1周)，结束时间都是时间段的结束时间。
			if($week_end >= $end_time){
				$weekList[] = array($week_end,$end_time);
			}else{
				//N周的情况用while循环一下，然后写入数组
				while($week_end <= $end_time){
					$start         = $week_end;
					$week_end    = date("Y-m-d 00:00:00",strtotime("$week_end +7 day"));
					if($week_end <= $end_time){
						$weekList[]  = array($start,$week_end);
					}else{
						$weekList[]  = array($start,$end_time);
					}
				}
			}
		}
		return $weekList;
	}
	
	/**
	 * 跳转提醒
	 * @param string $msg 跳转提醒内容
	 * @param string $url 跳转链接
	 */
	public function prompt_msg($msg,$url){
		echo "<script type='text/javascript' language='javascript'>
	
				alert('".$msg."');parent.location.href='".$url."';
	
			  </script>";
		exit;
	}
	
	/**
	 * 跳转alert
	 * @param string $msg alert内容
	 */
	public function prompt_alert($msg){
		echo "<script type='text/javascript' language='javascript'>
				alert('".$msg."');
				window.history.go(-1);
			  </script>";
		exit;
	}
	
	/**
	 * 检查用户有无当前操作权限
	 * @param unknown_type $url
	 */
	public function check_power($url,$json=false){
		/* if(!isset($this->session->userdata['check_power'])){
			$this->load->helper('url');
			redirect('/auth/login');
		}
		 */
		
		
		if(!isset($this->session->userdata['check_power'])){
			//根据code组织以及导航
			$this->load->model('admin_power_model');
			
			$rolecode = $this->session->userdata['rolecode'];
			
			//将所有权限编码，转换为模块
			$power_code = $this->admin_power_model->listBycode($rolecode);
			$str = "";
			foreach ($power_code as $c){
				if ( ! preg_match('#^https?://#i', $c['module_name'])){
					$uri = $c['module_name'];
					if($c['action_name'])
						$uri = $uri.'/'.$c['action_name'];
					$str .= $uri.',';
				}
			}
			$check_power = $str;
		}else{
				
			$check_power = $this->session->userdata['check_power'];
		}		
		
		$check_power = ','.$check_power;
		$url = ','.$url.',';
		
		if(!strstr($check_power, $url)){
			if($json){
				//json控制权限，待开发
				echo json_encode("没有当前操作权限");
				exit;
			}else{
				$this->prompt_alert("没有当前操作权限");
			}
		}
	}
}	


