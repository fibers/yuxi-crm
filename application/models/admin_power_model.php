<?php
include_once(APPPATH."core/TT_Model.php");
class Admin_power_model extends TT_Model{
	function __construct(){
		parent::__construct();
		$this->table_name = 'admin_power';
	}
	
	public function allList(){
		$param['isshow']='1';
		$list = $this->getList($param,'id,name,pid');

		//数据排序
		$list = $this->tree($list);
		return $list?$list:array();
	}
	
	/**
	 * 树状图
	 * @param array $list 二维数组
	 * @param int $pid 父级id
	 * @param 等级    $level 级别
	 * @param unknown_type $html
	 * @return Ambigous <multitype:unknown , unknown>
	 */
	public function tree(&$list,$pid=0,$level=0,$html='--'){
		static $tree = array();
		foreach($list as $v){
			if($v['pid'] == $pid){
				$v['sort'] = $level;
				//$v['html'] = str_repeat($html,$level);
				if($pid=='0'){
					$tree[$v['id']] = $v;
				}else{
					$tree[$pid][$level][] = $v;
				}
				$this->tree($list,$v['id'],$level+1);
			}
		}
		return $tree;
	}
	
	/**
	 * 获取导航类
	 * @param string $codes 权限ids
	 * @param int $pid 父级id
	 * @param int $isshow 是否显示
	 */
	public function navList($codes=null,$pid,$isshow){
		$q = $this->db->select('name,module_name,action_name', false)->where(array('pid'=>$pid,'isshow'=>$isshow));
		if($codes){
			$codes = explode(',', $codes);
			$q = $this->db->where_in('id',$codes);
		}
		$q = $this->db->get($this->table_name);
		return $list = $q->result_array();
	}
	
	/**
	 * 查看权限编码下的权限
	 * @param string $codes 权限ids
	 * @return array
	 */
	public function listBycode($codes){
		$q = $this->db->select('id,name,module_name,action_name', false);
		if(!$codes){
			return "";
			exit;
		}
		
		if($codes!='all'){
			$codes = explode(',', $codes);
			$q = $this->db->where_in('id',$codes);
		}
		$q = $this->db->get($this->table_name);
		return $list = $q->result_array();
	}
}