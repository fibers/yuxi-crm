<?php
include_once(APPPATH."core/TT_Model.php");
class Onlineusers_model extends TT_Model{
	function __construct(){
		parent::__construct();
		$this->table_name = 'IMOnlineUsers';
	}
	
	public function getOnlineBydate($date1, $date2) {
		$sql = "select count(DISTINCT userid) as num from IMOnlineUsers  where `uptime` >=$date1 and `uptime` < $date2";
		$re = $this->getQuery ( $sql );
		$num = (! empty ( $re )) ? ($re ['0'] ['num']) : (0);
		return $num;
	}
	
	public function getList($d1, $d2) {
		$re = $this->getOnlineBydate ( $d1, $d2 );
		return array (
				'date' => date ( 'Y-m-d H:i:s', $d1 ) . '--' . date ( 'Y-m-d H:i:s', $d2 ),
				'num' => $re 
		);
	}
	
	//按小时统计数据
	public function getListByHours($start_time,$end_time){
		$list = array();
		$sql = "SELECT FROM_UNIXTIME(uptime,'%Y-%m-%d %H:00') date,count(DISTINCT userid) as num FROM `IMOnlineUsers` where uptime>=$start_time and uptime<$end_time  GROUP BY date";
		$list = $this->getQuery ( $sql );
		return $list;
	}
	
	//按日统计数据
	public function getListByDays($start_time,$end_time){
		$list = array();
		$sql = "SELECT FROM_UNIXTIME(uptime,'%Y-%m-%d') date,count(DISTINCT userid) as num FROM `IMOnlineUsers` where uptime>=$start_time and uptime<$end_time  GROUP BY date";
		$list = $this->getQuery ( $sql );
		return $list;
	}
	
	//按周统计数据
	public function getListByWeeks($start_time,$end_time){
		$list = array();
		$sql = "SELECT FROM_UNIXTIME(uptime,'%Y-%u') date,count(DISTINCT userid) as num FROM `IMOnlineUsers` where uptime>=$start_time and uptime<$end_time  GROUP BY date";
		$list = $this->getQuery ( $sql );
		return $list;
	}

	//按月统计数据
	public function getListByMonths($start_time,$end_time){
		$list = array();
		$sql = "SELECT FROM_UNIXTIME(uptime,'%Y-%m') date,count(DISTINCT userid) as num FROM `IMOnlineUsers` where uptime>=$start_time and uptime<$end_time  GROUP BY date";
		$list = $this->getQuery ( $sql );
		return $list;
	}
}