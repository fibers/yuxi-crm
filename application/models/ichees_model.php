<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Ichees_Model extends CI_Model
{

	protected $table_name;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/*
	 * 获得故事相关信息
	 */
	public function getStoryInfo($storyid) {


		$this->table_name = 'IMStory';

		$sql = 'SELECT * FROM `IMStory` WHERE `id` = '.$storyid;

		$q = $this->db->query($sql);
		$storyInfo = $q->row_array();

		return $storyInfo;
	}
	
	/**
	 * 根据戏文id获取对戏群id
	 */
	public function getGroupBystory($storyid){
		$storyInfo = $this->getStoryInfo($storyid);
		$storyid = $storyInfo['id'];
		
		$sql = 'SELECT * FROM `IMStoryGroup` WHERE `storyid` = '.$storyid.' AND `isplay` = 1 LIMIT 0,1';
		$q = $this->db->query($sql);
		
		$groupInfo = $q->row_array();
		if(empty($groupInfo)) {
			return '';
			exit;
		}
		$groupid = $groupInfo['groupid'];
		return $groupid;
	}
	
	
	/**
	 * 获得故事内容
	 * @param unknown_type $storyid
	 * @return multitype:multitype:string  |unknown
	 */
	public function getGroupsMessageList($groupid,$storyid) {
		$dbSwitch = $groupid%8;
		$table_name = 'IMGroupMessage_'.$dbSwitch;
	
		/* $sql = 'SELECT a.userId,a.groupId,a.content,a.msgId,b.roleid FROM `'.$table_name.'` as a LEFT JOIN `IMStoryMember` as b ON a.`userId` = b.`userid` AND a.groupId = b.groupid
				WHERE a.`groupId` = '.$groupid.' ORDER BY a.`created` ASC'; */
		
		$sql = 'SELECT a.userId,a.groupId,a.content,a.msgId FROM `'.$table_name.'` as a  WHERE a.`groupId` = '.$groupid.' ORDER BY a.`created` ASC';
		$q = $this->db->query($sql);
		$chapterList = $q->result_array();
	
		$story_user = array();
		
		// 调用解密接口
		foreach ($chapterList as $key => $value) {
			//戏文转码
			$emsg = $value['content'];
			$dmsg = $this->aesLocalDecode($emsg);
			//$dmsg = decode_msg($emsg, strlen($emsg));
			$dmsg = str_replace(PHP_EOL,'<br>',$dmsg);
			$chapterList[$key]['content'] = $dmsg;
			$chapterList[$key]['content'] = str_replace('&$#@~^@[{:','<img src="',$chapterList[$key]['content']);
			$chapterList[$key]['content'] = str_replace(':}]&$~@#@','">',$chapterList[$key]['content']);
			
			//新剧获得剧角色名
			$userid = $value['userId'];
			
			//判断当前用户是否已查询出姓名
			if(isset($story_user[$userid])){
				$chapterList[$key]['nickname'] = $story_user[$userid];
			}else{
				$chapterList[$key]['nickname'] = $this->getStoryUser($storyid, $userid);
				$story_user[$userid] = $chapterList[$key]['nickname'];
			}
		}
		return $chapterList;
	}
	
	/**
	 * 根据剧名和用户查询，用户在剧里名
	 * @param 剧名 $storyid
	 * @param 用户名 $userid
	 */
	public function getStoryUser($storyid,$userid){
		$roletitle = '';
		//新剧
		$sql = "select rolenatures from applyrole where fromuid=$userid and storyid=$storyid ";
		$q = $this->db->query($sql);
		$res = $q->result_array();
			
		if(isset($res[0]['rolenatures'])){
			$j_str = json_decode($res[0]['rolenatures']);
			foreach((array)$j_str as $k=>$v){
				if($v->id=='1'){
					$roletitle=$v->value;
					break;
				}
			}
			//$chapterList[$key]['nickname'] = $roletitle;
		}
		
		//老剧
		if(empty($roletitle)){
			$sql = "select userrolename,roleid from storyuserroleinfo where userid=$userid and storyid=$storyid ";
			$q = $this->db->query($sql);
			$res = $q->result_array();
		
			$roletitle = $res[0]['userrolename'];
			//$chapterList[$key]['nickname'] = $roletitle;
		}
		
		//用户表中
		if(empty($roletitle)){
			//获取用户真实姓名
			$sql = 'SELECT * FROM `IMUser` WHERE `id` = '.$userid.' LIMIT 0,1';
			$q = $this->db->query($sql);
			$res = $q->result_array();
			$roletitle = $res[0]['nick'];
			//$chapterList[$key]['nickname'] = $roletitle;
		}
		
		return $roletitle;
	}
	
	/**
	 * 获的最大的msgId
	 * @param unknown_type $groupid
	 * @return string
	 */
	public function getMaxMsg($groupid){
		$dbSwitch = $groupid%8;
		$table_name = 'IMGroupMessage_'.$dbSwitch;
		$sql = 'SELECT max(msgId) as max_msgId FROM `'.$table_name.'` WHERE `groupId` = '.$groupid;
		$q = $this->db->query($sql);
		$max_msgId = $q->result_array();
		return $max_msgId?$max_msgId[0]['max_msgId']:'0';
	}
	
	/**
	 * 获取比msgId大的剧内容
	 */
	public function getGmMsg($groupid,$max_msgId,$storyid){
		$dbSwitch = $groupid%8;
		$table_name = 'IMGroupMessage_'.$dbSwitch;
		
		/* $sql = 'SELECT a.userId,a.groupId,a.content,a.msgId,b.roleid FROM `'.$table_name.'` as a LEFT JOIN `IMStoryMember` as b ON a.`userId` = b.`userid` AND a.groupId = b.groupid
				WHERE a.`groupId` = '.$groupid.' and a.msgId >'.$max_msgId.' ORDER BY a.`created` ASC'; */
		$sql = 'SELECT a.userId,a.groupId,a.content,a.msgId FROM `'.$table_name.'` as a  
				WHERE a.`groupId` = '.$groupid.' and a.msgId >'.$max_msgId.' ORDER BY a.`created` ASC';
		$q = $this->db->query($sql);
		$chapterList = $q->result_array();
		
		$story_user = array();
		
		// 调用解密接口
		foreach ((array)$chapterList as $key => $value) {
			//戏文转码
			$emsg = $value['content'];
			$dmsg = $this->aesLocalDecode($emsg);
			//$dmsg = decode_msg($emsg, strlen($emsg));
			
			$dmsg = str_replace(PHP_EOL,'<br>',$dmsg);
			$chapterList[$key]['content'] = $dmsg;
			$chapterList[$key]['content'] = str_replace('&$#@~^@[{:','<img src="',$chapterList[$key]['content']);
			$chapterList[$key]['content'] = str_replace(':}]&$~@#@','">',$chapterList[$key]['content']);
		
			//新剧获得剧角色名
			$userid = $value['userId'];
				
			//判断当前用户是否已查询出姓名
			if(isset($story_user[$userid])){
				$chapterList[$key]['nickname'] = $story_user[$userid];
			}else{
				$chapterList[$key]['nickname'] = $this->getStoryUser($storyid, $userid);
				$story_user[$userid] = $chapterList[$key]['nickname'];
			}
		}
		return $chapterList;
	}

	/*
	 * 获得故事相关信息
	 */
	public function getSelfStoryInfo($storyid) {

		$this->table_name = 'IMStory';

		$sql = 'SELECT table_story.*,table_user.avatar FROM `IMStory` as table_story LEFT JOIN `IMUser` as table_user ON table_story.`creatorid` = table_user.`id` WHERE table_story.`id` = '.$storyid;

		$q = $this->db->query($sql);
		$getSelfStoryInfo = $q->row_array();

		return $getSelfStoryInfo;
	}

	/*
	 * 获得故事内容
	 */

	public function getChapterList($storyid) {

		$storyInfo = $this->getStoryInfo($storyid);
		$storyid = $storyInfo['id'];
		
		$sql = 'SELECT * FROM `IMStoryGroup` WHERE `storyid` = '.$storyid.' AND `isplay` = 1 LIMIT 0,1';
		$q = $this->db->query($sql);
		
		$groupInfo = $q->row_array();
		if(empty($groupInfo)) {
			return $chapterList = array(array('content' => '还没有戏文'));
		}
		$groupid = $groupInfo['groupid'];

		$dbSwitch = $groupid%8;
		$table_name = 'IMGroupMessage_'.$dbSwitch;
		$sql = 'SELECT a.userId,a.groupId,a.content,a.msgId FROM `'.$table_name.'` as a  WHERE a.`groupId` = '.$groupid.' ORDER BY a.`created` ASC';
		/* $sql = 'SELECT * 
				FROM `'.$table_name.'` as a 
				LEFT JOIN `IMStoryMember` as b 
					ON a.`userId` = b.`userid` AND a.groupId = b.groupid 
				WHERE a.`groupId` = '.$groupid.' 
				ORDER BY a.`created` ASC'; */

		$q = $this->db->query($sql);
		$chapterList = $q->result_array();
		
		$story_user = array();
		// 调用解密接口
		foreach ($chapterList as $key => $value) {
			$emsg = $value['content'];
			//$dmsg = $this->aesDecode($emsg);
           //$dmsg = $this->aesLocalDecode($emsg);
	     $dmsg = decode_msg($emsg, strlen($emsg));
             //$dmsg = encode_msg('1', strlen('1'));
	//	$dmsg = $emsg;
			//$chapterList[$key]['content'] = $dmsg['dmsg'];
            $dmsg = str_replace(PHP_EOL,'<br>',$dmsg);
            $chapterList[$key]['content'] = $dmsg;

			$chapterList[$key]['content'] = str_replace('&$#@~^@[{:','<img src="',$chapterList[$key]['content']);
			$chapterList[$key]['content'] = str_replace(':}]&$~@#@','">',$chapterList[$key]['content']);
 
			$userid = $value['userId'];
			
			//新剧获得剧角色名
			$userid = $value['userId'];
			
			//判断当前用户是否已查询出姓名
			if(isset($story_user[$userid])){
				$chapterList[$key]['nickname'] = $story_user[$userid];
			}else{
				$chapterList[$key]['nickname'] = $this->getStoryUser($storyid, $userid);
				$story_user[$userid] = $chapterList[$key]['nickname'];
			}

			/* $nickname = $value['nickname'];
			if(empty($nickname) && !empty($value['roleid'])) {
				$sql = 'SELECT * 
					FROM `IMStoryMember` as table_member 
					LEFT JOIN `IMStoryRole` as table_role
						ON table_member.`roleid` = table_role.`roleid`
					WHERE table_member.`storyid` = '.$storyid.' AND table_member.`groupid` = '.$groupid .' AND table_member.`userid` = '.$userid .'
					LIMIT 0,1';

				$q = $this->db->query($sql);
				$res = $q->result_array();
				
				$roletitle = $res[0]['roletitle'];
				$chapterList[$key]['nickname'] = $roletitle;
				
			}

			$nickname = $chapterList[$key]['nickname'];
			if (empty($roletitle)) {
					$sql = 'SELECT * 
					FROM `IMUser` 
					WHERE `id` = '.$userid.'
					LIMIT 0,1';

					$q = $this->db->query($sql);
					$res = $q->result_array();
					// echo '<pre>';
					// var_dump($res);
					// echo '</pre>';
					$chapterList[$key]['nickname'] = $res[0]['nick'];
				} */
		}

		$data = array('chapterList'=>$chapterList,'story_user'=>$story_user);
		return $data;
	}


    //add by guoq-s
    private function aesLocalDecode($emsg)
    {
        $command = './decode ' . EscapeShellArg($emsg);
        //$value = passthru($command);
	$value = `$command`;
        return $value ; //$return;

    }
    //add by guoq-e

	private function aesDecode($emsg) {


		$msg = array(
               		'msg' => $emsg,
               );

		$res = $this->httpRequest('http://123.56.154.93:8400/query/DecodeContent','post',json_encode($msg));
		// $res = $this->httpRequest('http://www.baidu.com','post',json_encode($msg));
		
		$res = json_decode($res,true);
		return $res;
	}
	private function httpRequest($url,$method,$params=array()){
		$curl=curl_init();
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($curl,CURLOPT_HEADER,0 ) ;
		curl_setopt($curl,CURLOPT_URL,$url);
		curl_setopt($curl,CURLOPT_POST,1 );
		curl_setopt($curl, CURLOPT_POSTFIELDS,$params);
		$result=curl_exec($curl);
		curl_close($curl);
		return $result;
	}

	public function getGreaterList($storyid) {

		$storyInfo = $this->getStoryInfo($storyid);

		$storyid = $storyInfo['id'];
		
		if(empty($storyid)) {
			return false;
		}

		$sql = 'SELECT * FROM `IMStoryGroup` WHERE `storyid` = '.$storyid.' AND `isplay` = 1 LIMIT 0,1';
		$q = $this->db->query($sql);
		
		$groupInfo = $q->row_array();
		if(empty($groupInfo)) {
			return $chapterList = array(array('content' => '还没有戏文'));
		}
		$groupid = $groupInfo['groupid'];

		// $dbSwitch = $groupid%8;
		// $table_name = 'IMGroupMessage_'.$dbSwitch;

		$sql = 'SELECT *,b.`text_cnt` as counter
				FROM `IMUser` as a ,`IMGroupMsgTextCount` as b
				WHERE a.`id` = b.`user_id` AND b.`group_id` = '.$groupid.' ORDER BY text_cnt DESC';

		$q = $this->db->query($sql);

		$greaterList = $q->result_array();
		

		// var_dump($greaterList);
		return $greaterList;
	}

	// this is old codes under the line
	public function getOne($params = array(), $fields = '*')
	{		
		$q = $this->db->select($fields, false)->where($params)->get($this->table_name);
		return $row = $q->row_array();
	}


	public function getCount($params = array(), $like = array())
	{
		if ( ! isset($params['status'])) {
			$params['status !='] = '-1';
		}
		return $count = $this->db->where($params)->or_like($like)->from($this->table_name)
								 ->count_all_results();
	}

	public function getList($params = array(), $fields = '*', $start = 0, $perpage = 0, 
							$order 	= '', $sort = '', $like = array(), $group = array())
	{
		if ($perpage) {
			$this->db->limit($perpage, $start);
		}
		if ($order && $sort) {
			$this->db->order_by($order, $sort);
		}
		if (!empty($group) && count($group) > 0) {
			$this->db->group_by($group);
		}		
		$q = $this->db->select($fields, false)->where($params)->or_like($like)->get($this->table_name);
		return $list = $q->result_array();
	}

	public function getLike($like = array(), $params = array(), $fields = '*', 
							$start = 0, $perpage = 0, $order = '', $sort = '')
	{
		return $list = $this->getList($params, $fields, $start, $perpage, $order, $sort, $like);
	}

	public function getIn($inField, $inArray, $params = array(), $fields = '*', 
						  $start = 0, $perpage = 0, $order = '', $sort = '')
	{
		if ( empty($inArray) ) {
			return array();
		}
		$inString = implode(',', $inArray);
		$params["$inField in ($inString)"] = null;
		return $list = $this->getList($params, $fields, $start, $perpage, $order, $sort);
	}

	public function getInCount($inField, $inArray, $params = array())
	{
		if ( empty($inArray) ) {
			return 0;
		}
		$list = $this->getIn($inField, $inArray, $params, $fields=' count(*) as num ');
		if (isset($list[0]['num'])) {
			return $count = intval($list[0]['num']);
		} else {
			return 0;
		}
	}

	public function getQuery($sql)
	{
		return $list = $this->db->query($sql)->result_array();
	}




}
