<?php
include_once(APPPATH."core/TT_Model.php");
class Storycategory_model extends TT_Model{
	function __construct(){
		parent::__construct();
		$this->table_name = 'IMStoryCategory';
	}
	
	
	function listByIds($categorys){
		
		$q = $this->db->select('GROUP_CONCAT(name) as name', false);
		$this->db->where_in('id', explode(',', $categorys));
		$row = $this->db->get($this->table_name)->row_array();
		return $row;
	}

}