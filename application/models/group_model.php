<?php
include_once(APPPATH."core/TT_Model.php");
class Group_model extends TT_Model{
	function __construct(){
		parent::__construct();
		$this->table_name = 'IMGroup';
	}
	
	/**
	 * 获取话题管理数据--分页/搜索列表
	 * @param 开始页数 $start
	 * @param 条件 $where
	 * @param like条件 $like
	 * @param 排序 $od
	 * @return array
	 */
	public function allList($start,$where,$like,$od){
		
		//$this->config->config['perpage'];
		
		//分页
		$perpage = 10;
		$offset = $start * $perpage;
		
		if($like)
			$this->db->like ($like);
		$this->db->select ( 'IMGroup.* , IMUser.nick' )->from ( 'IMGroup' )->where ( $where );
		$this->db->limit ( $perpage, $offset );
		$this->db->join ( 'IMUser', 'IMUser.id = IMGroup.creator', 'left' );
		$this->db->order_by ( $od, 'desc' );
		$query = $this->db->get ();
		$groups = $query->result_array ();
		
		return $groups?$groups:array();
	}

}