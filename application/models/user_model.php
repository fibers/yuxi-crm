<?php
include_once(APPPATH."core/TT_Model.php");
class User_model extends TT_Model{
	function __construct(){
		parent::__construct();
		$this->table_name = 'IMUser';
	}
        
        
        
        public function getNewUserBydate($date1,$date2){ 
            $sql = " select count(id) as num from IMUser  where   `created` >= ".$date1." and `created` < ".$date2;
            $re =  $this->getQuery($sql);
        	if (isset($re[0]['num'])) {
        		return intval($re[0]['num']);
        	} else {
        		return 0;
        	}
        }
        
        /**
         * 求某段时间内总数
         * @param 时间  int $d2
         * @return number
         */
        public function getCountUser($end_time){
        	$sql = " select count(id) as num from IMUser  where  `created` <= ".$end_time;
        	$re =  $this->getQuery($sql);
        	if (isset($re[0]['num'])) {
        		return intval($re[0]['num']);
        	} else {
        		return 0;
        	}
        }
        
        
        public function getNewUserByMonth($d1,$d2){
            $sql = " select count(id) as num from IMUser  where   `created` >= ".$d1." and `created` <= ".$d2;
            $re =  $this->getQuery($sql);
            $num1 = $re['0']['num'];
             $sql = " select count(id) as num from IMUser  where  `created` <= ".$d2;
            $re =  $this->getQuery($sql);
            $num2 = $re['0']['num'];
            return array('num'=>$num1,'total'=>$num2,'date'=>date('Y-m-d',$d1));
        }

}