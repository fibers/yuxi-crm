<?php
include_once(APPPATH."core/TT_Model.php");
class Group_Message_model extends TT_Model{
	function __construct(){
		parent::__construct();
		$this->table_name = 'IMGroupMessage_';
	}
	
	/**
	 * 获取话题管理数据--分页/搜索列表
	 * @param 话题ID $groupId int
	 * @return array
	 */
	public function allList($groupId){
		//当前群记录表
		$table_message = $this->table_name.($groupId%8);
		
		$this->db->select ( $table_message.'.* , IMUser.nick' )->from ( $table_message )->where ( array('groupId'=>$groupId,$table_message.'.status'=>0) );
		$this->db->join ( 'IMUser', 'IMUser.id = '.$table_message.'.userId', 'left' );
		$this->db->order_by ( 'msgId', 'asc' );
		$query = $this->db->get ();
		$groups = $query->result_array ();
		return $groups?$groups:array();
	}
	
	public function message_del($id,$groupId){
		//当前群记录表
		$table_message = $this->table_name.($groupId%8);
		
		$result = $this->db->where ( array (
				'id' => $id,
				'groupId' => $groupId 
		) )->update ( $table_message, array (
				'status' => 1 
		) );

		return $result;
	}

}