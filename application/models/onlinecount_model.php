<?php
include_once(APPPATH."core/TT_Model.php");
class Onlinecount_model extends TT_Model{
	function __construct(){
		parent::__construct();
		$this->table_name = 'IMOnlineCount';
	}
        
        
        
        public function getNewUserBydate($date1,$date2){ 
            $sql = " select oninecount from IMOnlineCount  where   `counttime` >= ".$date1." and `counttime` < ".$date2;
            $re =  $this->getQuery($sql);
            $num = (!empty($re))?($re['0']['oninecount']):(0);
            return $num;
        }
        
        
        public function getDayList($d1,$d2){
            $re = $this->getNewUserBydate($d1,$d2);
            return array('date'=>date('Y-m-d H:i:s',$d1).'--'.date('Y-m-d H:i:s',$d2),'num'=>$re);
        }

}