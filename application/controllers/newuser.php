<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH."core/TT_Controller.php");

class NewUser extends TT_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('user_model');
		$this->load->model('depart_model');
        date_default_timezone_set('Asia/Shanghai');
        $this->check_power('newuser');
	}

	public function index()
	{
		$this->config->site_url();

		$this->load->view('base/header');
		$this->load->view('base/newuser');
		$this->load->view('base/footer');
	}
	
	/**
	 * 获取新用户数
	 */
	public function getnewuser() {
		//搜索时间
		$start_time = $this->input->get('keystarttime');
		$end_time = $this->input->get('keyendtime');
		//默认当前时间
		if(!$start_time){
			$start_time = date('Y-m-d',time());
		}
		
		//存在开始时间，不存在结束时间时，默认和开始时间一致
		if($start_time && !$end_time){
			$end_time = $start_time;
		}
		//默认当前时间
		if(!$end_time){
			$end_time = date('Y-m-d',time());
		}
		
		$dataunit = $this->input->get('dataunit');
		//时间趋向不存在时
		if(!$dataunit){
			$dataunit = 'daily';
		}
		
		//当开始时间和结束时间为同一天时，时间区间为小时
		if(strtotime($start_time)==strtotime($end_time)){
			$dataunit = 'hourly';
		}
		
		//开始-结束时间差，确定可查看范围
		$dataunitrue = $dataunitfalse = array();
		$this->data_unit($start_time,$end_time,$dataunitrue,$dataunitfalse,$dataunit);
		
		//计算模式计算
		$maxNum = $i_next= '0';
		$res = $labels = $list = array();
		if($dataunit=='hourly'){
			$str_start_time = strtotime($start_time);
			$str_end_time = strtotime($end_time)+3600*24;
			
			//当前时间是否存在
			/* if($str_end_time>time()){
				$str_end_time = time();
			} */
			
			for($i = $str_start_time; $i < $str_end_time; $i=$i_next) {
				$i_next = $i+3600;
				$num = $this->user_model->getNewUserBydate ( $i, $i_next );
				$res [] = $num;
				$labels[]= date('G',$i);
				if($num>$maxNum){
					$maxNum = $num;
				}
				$total = $this->user_model->getCountUser($i_next);
				$list[] = array('num'=>$num,'date'=>date('Y-m-d H:i',$i),'total'=>$total);
			}
		}elseif($dataunit=='daily'){
			$str_start_time = strtotime($start_time);
			$str_end_time = strtotime($end_time)+3600*24;
			//86400
			
			for($i = $str_start_time; $i < $str_end_time; $i=$i_next) {
				$i_next = $i+86400;
				
				$num = $this->user_model->getNewUserBydate ( $i, $i_next );
				$labels[]= date('m-d',$i);
				$res [] = $num;
				/* if(count($res)==1){
					$res = array_merge(array(0),$res);
					$labels = array_merge(array(0),$labels);
				} */
				
				if($num>$maxNum){
					$maxNum = $num;
				}
				$total = $this->user_model->getCountUser($i_next);
				$list[] = array('num'=>$num,'date'=>date('m-d',$i),'total'=>$total);
			}
		}elseif($dataunit=='weekly'){
			//获取时间段内，周个数
			$weekList = $this->weekList($start_time,$end_time);
			
			foreach($weekList as $k=>$v){
				$labels[]= date('m-d',strtotime($v[0])).'——'.date('m-d',strtotime($v[1]));
				
				//查询数据
				$num = $this->user_model->getNewUserBydate ( strtotime($v[0]), strtotime($v[1]) );
				$res [] = $num;
				if($num>$maxNum){
					$maxNum = $num;
				}
				$total = $this->user_model->getCountUser(strtotime($v[1]));
				$list[] = array('num'=>$num,'date'=>date('m-d',strtotime($v[0])).'——'.date('m-d',strtotime($v[1])),'total'=>$total);
			}
			$res = array_merge(array(0),$res);
			$labels = array_merge(array(0),$labels);
		}elseif($dataunit=='monthly'){
			$month_end = $str_start_time = strtotime($start_time);
			$str_end_time = strtotime($end_time)+3600*24;
			while ( $month_end <= $str_end_time ) {
				$start = $month_end;
				$month_end =  strtotime(date("Y-m",$month_end)." +1 month");

				//结束时间
				$end = $month_end <= $str_end_time?$month_end:$str_end_time;
				
				//横坐标
				$labels[]= date('Y-m',$start);
				
				//查询数据
				$num = $this->user_model->getNewUserBydate ( $start, $end );
				$res [] = $num;
				if($num>$maxNum){
					$maxNum = $num;
				}
				$total = $this->user_model->getCountUser($end);
				$list[] = array('num'=>$num,'date'=>date('Y-m',$start),'total'=>$total);
			}
			
			$res = array_merge(array(0),$res);
			$labels = array_merge(array(0),$labels);
		}
		
		// $res =  [99,66,55,28,48,40,19,96,27,100,28,48];//,40,19,96,27,100,28,48,40,19,0,0,0;
		if ($res) {

			//纵轴每个刻度的宽度
			$scaleStepWidth = ceil($maxNum/8);
			
			echo json_encode ( array (
					'users' => $list,
					'labels' => $labels,
					'data' => $res,
					'dataunit' => $dataunit,
					'dataunitrue' =>$dataunitrue,
					'dataunitfalse' => $dataunitfalse,
					'scaleSteps' => '8',
					'scaleStepWidth' => $scaleStepWidth ? $scaleStepWidth : '1' 
			) );
		
		}
	}
	
		
	//获取日期
	public function getDayData() {
		$date_now = date ( 'j' );
		$month_now = date ( 'n' );
		for($month_temp = $month_now - 1; $month_temp <= $month_now; $month_temp ++) {
			if ($month_temp < $month_now) {
				$days = 31;
			} else {
				$days = $date_now + 1;
			}
			for($i = 1; $i < $days; $i ++) {
				$d = ($i < 10) ? ('0' . $i) : ($i);
				$list [] = $this->user_model->getNewUserByMonth ( strtotime ( date ( 'Y-' . $month_temp . '-' . $d . ' 00:00:00' ) ), strtotime ( date ( 'Y-' . $month_temp . '-' . $d . ' 23:59:59' ) ) );
				;
			}
		}
		
		$result = array (
				'users' => $list,
				'page' => 1 
		);
		echo json_encode ( $result );
	}
	
	
	public function getMonthData() {
		$date_now = date ( 'j' );
		for($i = 1; $i < $date_now + 1; $i ++) {
			$d = ($i < 10) ? ('0' . $i) : ($i);
			$list [] = $this->user_model->getNewUserByMonth ( strtotime ( date ( 'Y-m-' . $d . ' 00:00:00' ) ), strtotime ( date ( 'Y-m-' . $d . ' 23:59:59' ) ) );
			;
		}
		
		$result = array (
				'users' => $list,
				'page' => 1 
		);
		echo json_encode ( $result );
	}

	public function all()
	{
		$perpage = 10000;
		$departs = $this->depart_model->getList(array('status'=>0), '*', 0, $perpage);
		$_departs = array();
		foreach ($departs as $key => $value) {
			$_departs[$value['id']] = $value;
		}
		$start = $this->input->get('start');
		if(!$start){
			$start =  0;
		}
		$perpage = 10;
		$users = $this->user_model->getList(array('status'=>0), '*', $start*$perpage, $perpage);
		foreach ($users as $key => $value) {
			if($value['sex'] == 0){
				$users[$key]['sex'] = '女';
			}else{
				$users[$key]['sex'] = '男';
			}
			if(isset($_departs[$value['departId']])){
				$users[$key]['depart_value'] = $_departs[$value['departId']]['departName'];
			}else{
				$users[$key]['depart_value'] = '数据错误';
			}
			if($users[$key]['avatar']){
				$users[$key]['avatar_value'] = $users[$key]['avatar'];
			}
		}
		$count = $this->user_model->getCount(array('status'=>0));
		$result = array(
			'users'=>$users,
			'page'=>$start,
			'count'=>ceil($count/10),
			'departs'=>$_departs
		);
		echo json_encode($result);
	}

	public function del()
	{
		$id = $this->input->post('id');
		$result = $this->user_model->update(array('status'=>1), $id);
		if($result){
			echo 'success';
		}
	}

	public function add()
	{
		$salt = rand()%10000;
		$params = array(
			'sex'=>$this->input->post('sex'),
			'name'=>$this->input->post('name'),
			'domain'=>$this->input->post('domain'),
			'nick'=>$this->input->post('nick'),
			'password'=>md5(md5($this->input->post('password')).$salt),
			'salt'=>$salt,
			'phone'=>$this->input->post('phone'),
			'email'=>$this->input->post('email'),
			'avatar'=>$this->input->post('avatar'),
			'departId'=>$this->input->post('departId'),
			'status'=>$this->input->post('status'),
			'created'=>time(),
			'updated'=>time()
		);
		$result = $this->user_model->insert($params);
		if($result){
			echo 'success';
		}
	}

	public function edit()
	{
		$params = array(
			'sex'=>$this->input->post('sex'),
			'name'=>$this->input->post('name'),
			'domain'=>$this->input->post('domain'),
			'nick'=>$this->input->post('nick'),
			'phone'=>$this->input->post('phone'),
			'email'=>$this->input->post('email'),
			'avatar'=>$this->config->config['msfs_url'].$this->input->post('avatar'),
			'departId'=>$this->input->post('departId'),
			'status'=>$this->input->post('status'),
			'updated'=>time()
		);
		$id = $this->input->post('id');
		$result = $this->user_model->getOne(array('id'=>$id));
		$pwd = $this->input->post('pwd');
		if($pwd){
			$params['pwd'] = md5(md5($this->input->post('password')).$result["salt"]);
		}
		$result = $this->user_model->update($params,$id);
		if($result){
			echo 'success';
		}
	}

	public function get()
	{
		$id = $this->input->post('id');
		$result = $this->user_model->getOne(array('id'=>$id));
		if($result){
			echo json_encode($result);
		}
	}

	public function upload()
	{
		include_once APPPATH."libraries/image_moo.php";

		try{
		    $filename=$this->input->get('filename');
		    $ext = pathinfo($filename, PATHINFO_EXTENSION);
		    $filename = time().".".$ext;
		    $input = file_get_contents("php://input");
		    file_put_contents('./download/'.$filename, $input);
		    // $image = new Image_moo();
		    // $image
		    // 	->load('./download/'.$filename)
		    // 	->resize_crop(100,100)
		    // 	->save('./download/1.jpg');
		    // 裁剪头像
		    // $targ_w = $targ_h = 100;
		    // $jpeg_quality = 90;

		    // $img_r = imagecreatefromjpeg('./download/'.$filename);
		    // $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

		    // imagecopyresampled($dst_r,$img_r,0,0,0,0,
		    //     $targ_w,$targ_h,100,100);

		    // header('Content-type: image/jpeg');
		    // imagejpeg($dst_r, './download/'.$filename, $jpeg_quality);



		    $res = $this->_upload('./download/'.$filename);
		    if($res['error_code'] == 0){	    	
			    $array = array(
			    	'status' =>'success',
			    	'file' =>$res['path'],
			    	'real_path'=>$this->config->config['msfs_url'].$res['path']
			    );
		    }else{
		    	$array = array(
			    	'status' =>'fail',
			    	'file' =>'',
			    	'real_path'=>''
			    );
		    }
			echo json_encode($array);
		}
		catch(Exception $e)
		{
			$array = array(
				'status' =>'fail',
				'file' =>0
			);
			echo json_encode($array);
		}
	}

	public function _upload($filename)
	{
		$ch = curl_init();
		$data = array('filename'=>'@'.$filename);
		curl_setopt($ch,CURLOPT_URL,$this->config->config['msfs_url']);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch,CURLOPT_POST,true);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
		$result = curl_exec($ch);
		curl_close($ch);
		return json_decode($result,1);
	}

}
