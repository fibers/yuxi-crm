<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH."core/TT_Controller.php");

class Packageinfo extends TT_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->check_power('packageinfo');
		$this->load->helper('url');
		$this->load->model('packageinfo_model');
	}

	public function index()
	{
		$this->config->site_url();
		$this->load->view('base/header');
		$this->load->view('base/packageinfo');
		$this->load->view('base/footer');
	}

	public function all()
	{
		//分页
		$start = $this->input->get('start');
		if(!$start){
			$start =  0;
		}
		$perpage = 10;
		
		$param = array();
		
		//搜索ID
		$keyID = intval($_GET['keyID']);
		if($keyID){
			$param['id'] = $keyID;
		}
		
		//查询数据
		$infos = $this->packageinfo_model->getList($param, '*', $start*$perpage, $perpage,'id','desc');
		
		//总计总数
		$count = $this->packageinfo_model->getCount($param);
		$result = array(
			'infos'=>$infos,
			'page'=>$start,
			'count'=>ceil($count/10),
		);
		echo json_encode($result);
	}

	public function del()
	{
		$id = $this->input->post('id');
		$result = $this->packageinfo_model->delete($id,true);
		if($result){
			echo 'success';
		}
	}

	/**
	 * 添加版本信息
	 */
	public function add()
	{
		$params = array (
			'versioncode' => $this->input->post ( 'versioncode' ),
			'versionnumber_android' => $this->input->post ( 'versionnumber_android' ),
			'versionnumber_ios' => $this->input->post ( 'versionnumber_ios' ),
			'downurl_android' => $this->input->post ( 'downurl_android' ),
			'downurl_ios' => $this->input->post ( 'downurl_ios' ),
			'desc_android' => $this->input->post ( 'desc_android' ),
			'desc_ios' => $this->input->post ( 'desc_ios' ),
			'mustflag_android' => $this->input->post ( 'mustflag_android' ),
			'mustflag_ios' => $this->input->post ( 'mustflag_ios' ),
			//'producer' =>  $this->input->post ( 'producer' )
	);
		$result = $this->packageinfo_model->insert($params);
		if($result){
			echo 'success';
		}
	}

	/**
	 * 更新版本
	 */
	public function edit()
	{
		//获取数据
		$params = array(
			'versioncode' => $this->input->post ( 'versioncode' ),
			'versionnumber_android' => $this->input->post ( 'versionnumber_android' ),
			'versionnumber_ios' => $this->input->post ( 'versionnumber_ios' ),
			'downurl_android' => $this->input->post ( 'downurl_android' ),
			'downurl_ios' => $this->input->post ( 'downurl_ios' ),
			'desc_android' => $this->input->post ( 'desc_android' ),
			'desc_ios' => $this->input->post ( 'desc_ios' ),
			'mustflag_android' => $this->input->post ( 'mustflag_android' ),
			'mustflag_ios' => $this->input->post ( 'mustflag_ios' ),
			//'producer' =>  $this->input->post ( 'producer' )
		);
		$id = $this->input->post('id');
		//数据修改
		$result = $this->packageinfo_model->update($params,$id);
		if($result){
			echo 'success';
		}
	}

	public function get()
	{
		$id = $this->input->post('id');
		$result = $this->packageinfo_model->getOne(array('id'=>$id));
		if($result){
			echo json_encode($result);
		}
	}
}