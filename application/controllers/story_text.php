<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 抓取剧字数 and 广告访问记录
 * 方法为story,为了安全起见，已注释掉
 */
// error_reporting(0);
error_reporting(E_ERROR | E_WARNING | E_PARSE);
// include_once(APPPATH."core/TT_Controller.php");

class Story_text extends CI_Controller {

	public function __construct()
	{
		//exit;
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Ichees_Model');
		$this->load->model('story_model');
	}
	
	/**
	 * 广告访问记录
	 * 点击H5页面访问广告页面时触发
	 */
	public function banners_visit(){

		//数据接收
		$params = array (
				'title' => trim ( $_POST ['title'] ) ,
				'url' => trim ( $_POST ['url'] ) ,
				'remove_ip' => trim ( $_POST ['remove_ip'] ) ,
				'remove_agent' => trim ( $_POST ['remove_agent'] ) ,
				'created' => time() 
		);
		
		//数据库连接
		$this->load->database('db_im',true);
		
		//数据添加
		$this->db->insert('bannersVisit',$params);	
		$this->db->close();
	}
	
	/**
	 * 用户通过访问主页次数，通过访问量得到，带渠道商
	 */
	public function down_visit(){
		//数据接收
		$params = array (
				'url' => trim ( $_POST ['url'] ) ,
				'remove_ip' => trim ( $_POST ['remove_ip'] ) ,
				'remove_agent' => trim ( $_POST ['remove_agent'] ) ,
				'created' => time()
		);
		
		//数据库连接
		$this->load->database('db_im',true);
		
		//数据添加
		$this->db->insert('downVisit',$params);
		$this->db->close();
	}
	
	/**
	 * 用户通过访问主页次数，通过访问量得到，带渠道商
	 */
	public function index_visit(){
		//数据接收
		$params = array (
				'url' => trim ( $_POST ['url'] ) ,
				'remove_ip' => trim ( $_POST ['remove_ip'] ) ,
				'remove_agent' => trim ( $_POST ['remove_agent'] ) ,
				'created' => time()
		);
	
		//数据库连接
		$this->load->database('db_im',true);
	
		//数据添加
		$this->db->insert('indexVisit',$params);
		$this->db->close();
	}

	/**
	 * 转码
	 * @param 要转换的数据 string $emsg
	 * @return string
	 */
	private function aesLocalDecode($emsg)
	{
		$command = './decode ' . EscapeShellArg($emsg);
		//$value = passthru($command);
		$value = `$command`;
		return $value ; //$return;
	
	}
	
	/**
	 * 统计文字数
	 * @param string 字符串  $str
	 * @return mixed
	 */
	public function yang_count_word($str){
		$str = preg_replace('/[\x80-\xff]{1,3}/', ' ', $str,-1,$n);
		$n += str_word_count($str);
		return $n;
	}

	//重新统计剧字数
	/* public function story() {
		//查询剧
		$q = $this->db->select('storyid,groupid')->where(array(isplay=>'1'))->get('IMStoryGroup');
		$storyList = $q->result_array();
		
		//循环获取对戏所属id
		foreach($storyList as $s){
			//对戏表
			$table_fix = $s['groupid']%8;
			$table_name = 'IMGroupMessage_'.$table_fix;
			
			//每个表里的groupid
			@$table[$table_name] .= $s['groupid'].',';
		}
		
		$count_msg = count($table);
		
		//查询某个表中数据
		//$num = '0';
		for($i=0;$i<$count_msg;$i++){
			$table_name = 'IMGroupMessage_'.$i;
			$groupids = rtrim($table[$table_name],',');
			if($groupids){
				$sql = 'SELECT groupId,userId,content FROM `'.$table_name.'` as a WHERE a.`groupId` in ('.$groupids.') ORDER BY a.`msgId` ASC';
				$q = $this->db->query($sql);
				$chapterList = $q->result_array();
				
				//循环统计字数
				foreach($chapterList as $c){
					
					//内容转换
					$dmsg = $this->aesLocalDecode($c['content']);
					
					//正则替换图片
					$dmsg = str_replace('&$#@~^@[{:','<img src="',$dmsg);
					$dmsg = str_replace(':}]&$~@#@','">',$dmsg);
					$dmsg = preg_replace("/<img([^>]*)\s*src=('|\")([^'\"]+)('|\"\>)/",'',$dmsg);
					
					//文字统计
					$ustrlen = $this->yang_count_word($dmsg);
					
					$content[$c['groupId'].'_'.$c['userId']] += $ustrlen;
					//$num++;
				}
			}
			//exit;
			//sleep(5);
		}
		
		//循环检查数据or更新or新增
		foreach ($content as $key=>$text){
			//截取groupid && userId
			$id_a = explode('_', $key);
		
			//有无当前记录
			$q = $this->db->select ( 'id', false )->where ( array (
					'group_id' => $id_a [0],
					'user_id' => $id_a [1]
			) )->get ( 'IMGroupMsgTextCount' );
			$text_row = $q->row_array();
			
			if($text_row){
				//数据更新
				$data_u [] = array (
						'id' => $text_row ['id'],
						'text_cnt' => $text 
				);
			}else{
				//数据添加
				if($text){
					$data_in [] = array (
							'group_id' => $id_a[0],
							'user_id' => $id_a[1],
							'text_cnt' => $text,
							'created' => time (),
							'updated' => time () 
					);
				}
			}
		}

		//增加数据
		if(is_array($data_in))
			$this->db->insert_batch('IMGroupMsgTextCount',$data_in);
		
		//更新数据
		if(is_array($data_in))
			$this->db->update_batch('IMGroupMsgTextCount',$data_u,'id');
		
		echo 'ok';
	} */


}