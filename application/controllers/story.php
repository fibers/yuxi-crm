<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH."core/TT_Controller.php");

class Story extends TT_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('story_model');
		$this->load->model('user_model');
		$this->load->model('storycategory_model');
		$this->check_power('story');
	}

	/**
	 * 剧
	 */
	public function index()
	{
		//剧分类
		$data['cate_list'] = $this->storycategory_model->getList();
		$data['self']=0;
		$this->load->view('base/header');
		$this->load->view('base/story',$data);
		$this->load->view('base/footer');
	}
	
	/**
	 * 自戏
	 */
	public function indexSelf()
	{
		$data['self'] = 1;
		$this->load->view('base/header');
		$this->load->view('base/story',$data);
		$this->load->view('base/footer');
	}

	public function all()
	{
		$title = $this->input->get('title');
		
		//查询
		$like=array();
		$str_like='';
		if($title){
			$like=array('title'=>$title);
			$str_like=" and title like '%".$title."%' ";
		}
		
		//标签
		$categoryids = $this->input->get('category');
		
		//搜索条件
		$param['storystatus'] = 0;
		
		//剧or自戏
		$param['selfstory'] = $this->input->get('self');
		if(!$param['selfstory']){
			$param['selfstory'] =  0;
		}
		$str_like .= " and selfstory=".$param['selfstory'];
		
		//分页
		$start = $this->input->get('start');
		if(!$start){
			$start =  0;
		}
		$perpage = 10;
		$offset = $start*$perpage;
		
		//获取排序数据
		$od = $this->input->get ( 'od' );
		$od = (! empty ( $od )) ? ($od) : ('id');
		
		if($categoryids){
			$this->db->where("FIND_IN_SET('".$categoryids."',category) != 0");
			$str_like .= " and FIND_IN_SET('".$categoryids."',category) ";
		}

		$storyList = $this->story_model->getList($param, '*', $start*$perpage, $perpage,$od,'desc',$like);
		//echo $this->db->last_query();
		
		if(count($storyList)>0){
			foreach($storyList as $key=>$list){
				$category=array();
				if($list['category']){
					//$category=$this->storycategory_model->getOne(array('id'=>$list['category']));
					
					$category=$this->storycategory_model->listByIds($list['category']);
				}
				$storyList[$key]['category_name']='';
				if($category){
					$storyList[$key]['category_name']=$category['name'];
				}
				
				//获取创建者最新名称
				$user=$this->user_model->getOne(array('id'=>$list['creatorid']),'id,nick');
				if($user){
					$storyList[$key]['creatorname']=$user['nick'];
				}
				
				//总字数
				$text_cn['text_cnt'] = '0';
				if($param['selfstory']){//自戏
					$this->db->select('wordcount as text_cnt');
					$this->db->from('selfstoryword');
					$this->db->where(array('storyid'=>$list['id']));
					$this->db->order_by('time','desc');
					$this->db->limit('1');
					$query = $this->db->get();
					$text_cn = $query->row_array();
				}else{//剧
					$this->db->select('sum(IMGroupMsgTextCount.text_cnt) as text_cnt');
					$this->db->from('IMStoryGroup');
					$this->db->join('IMGroupMsgTextCount', 'IMStoryGroup.groupid=IMGroupMsgTextCount.group_id');
					$this->db->where(array('storyid'=>$list['id']));
					$query = $this->db->get();
					$text_cn = $query->row_array();					
				}
				@$storyList[$key]['text_cnt']=$text_cn['text_cnt']?$text_cn['text_cnt']:'0';
			}
		}
		
		//获取剧总数
		$count = $this->story_model->getQuery('select count(id) as nums from IMStory where storystatus !=1 '.$str_like);
		$result = array(
			'od'=>$od,
			'title'=>$title,
			'story'=>$storyList,
			'page'=>$start,
			'count'=>@ceil((int)$count[0]['nums']/$perpage)
		);
		
		echo json_encode($result);
	}
	
	/**
	 * 剧字数排序
	 */
	public function odword(){
		//分页
		$start = $this->input->get('start');
		if(!$start){
			$start =  0;
		}
		$perpage = 10;
		$offset = $start*$perpage;
		
		//查询
		$title = $this->input->get('title');
		$str_like='';
		if($title){
			$str_like=" and s.title like '%".$title."%' ";
		}
		
		//标签
		$categoryids = $this->input->get('category');
		if($categoryids){
			$str_like .= " and FIND_IN_SET('".$categoryids."',s.category) ";
		}
		
		$sql = "select sg.storyid,cm.text_cnts,s.id,s.title,s.category,s.creatorid,s.portrait,s.praisenum,s.intro,s.content,s.priority,s.version,s.permission from  IMStoryGroup sg join 
				(select sum(text_cnt) text_cnts,group_id from IMGroupMsgTextCount group by group_id) cm on sg.groupid=cm.group_id 
 				join IMStory s on sg.storyid=s.id where s.storystatus!=1 ".$str_like." order by cm.text_cnts desc limit $offset,$perpage";
		$storyList = $this->story_model->getQuery($sql);
		
		if(count($storyList)>0){
			foreach($storyList as $key=>$list){
				$category=array();
				$category=$this->storycategory_model->getOne(array('id'=>$list['category']));
				$storyList[$key]['category_name']='';
				if($category){
					$storyList[$key]['category_name']=$category['name'];
				}
		
				//获取创建者最新名称
				$user=$this->user_model->getOne(array('id'=>$list['creatorid']),'id,nick');
				if($user){
					$storyList[$key]['creatorname']=$user['nick'];
				}
		
				//总字数
				$storyList[$key]['text_cnt'] = $list['text_cnts'];				
			}
		}
		
		//获取剧总数
		$count = $this->story_model->getQuery('select count(sg.storyid) nums from  IMStoryGroup sg join 
				(select sum(text_cnt) text_cnts,group_id from IMGroupMsgTextCount group by group_id) cm on sg.groupid=cm.group_id 
 				join IMStory s on sg.storyid=s.id where s.storystatus!=1 '.$str_like);
		$result = array(
				'story'=>$storyList,
				'page'=>$start,
				'count'=>@ceil((int)$count[0]['nums']/$perpage)
		);
		
		echo json_encode($result);
	}
	
	public function odword_self(){
		//分页
		$start = $this->input->get('start');
		if(!$start){
			$start =  0;
		}
		$perpage = 10;
		$offset = $start*$perpage;
	
		//查询
		$title = $this->input->get('title');
		$str_like='';
		if($title){
			$str_like=" and s.title like '%".$title."%' ";
		}
	
		$sql = "select w.text_cnt,s.id,s.title,s.category,s.creatorid,s.portrait,s.praisenum,s.intro,s.content,s.priority,s.version,s.permission from IMStory s
				join (SELECT A.storyid as story_id, A.wordcount as text_cnt FROM selfstoryword A,
				(SELECT storyid, MAX(time) max_time FROM selfstoryword GROUP BY storyid ) B 
				WHERE A.storyid = B.storyid AND A.time = B.max_time ) w on s.id= w.story_id
 				where s.storystatus!=1 ".$str_like." ORDER BY w.text_cnt DESC limit $offset,$perpage";
		$storyList = $this->story_model->getQuery($sql);
	
		if(count($storyList)>0){
			foreach($storyList as $key=>$list){
				$category=array();
				$category=$this->storycategory_model->getOne(array('id'=>$list['category']));
				$storyList[$key]['category_name']='';
				if($category){
					$storyList[$key]['category_name']=$category['name'];
				}
	
				//获取创建者最新名称
				$user=$this->user_model->getOne(array('id'=>$list['creatorid']),'id,nick');
				if($user){
					$storyList[$key]['creatorname']=$user['nick'];
				}
			}
		}
	
		//获取剧总数
		$count = $this->story_model->getQuery('select count(s.id) as nums
				from IMStory s
				join ( SELECT A.storyid as story_id, A.wordcount as text_cnt FROM selfstoryword A,
				(SELECT storyid, MAX(time) max_time FROM selfstoryword GROUP BY storyid ) B 
				WHERE A.storyid = B.storyid AND A.time = B.max_time ) w on s.id= w.story_id
				where s.storystatus!=1  '.$str_like);
		$result = array(
				'story'=>$storyList,
				'page'=>$start,
				'count'=>@ceil((int)$count[0]['nums']/$perpage)
		);
	
		echo json_encode($result);
	}

	public function del()
	{
		$id = $this->input->post('id');
		$result = $this->story_model->updateByWhere(array('storystatus'=>1), 'id', $id);
		if($result){
			echo 'success';
		}
	}

	/**
	 * 剧置顶功能、取消置顶
	 */
	/* public function top()
	{
		$id = $this->input->post('id');
		$rel= $this->input->post('rel');
        $flag = 100;
		if(!$rel){//推荐
			//查询当前story信息
			$story = $this->story_model->getOne(array('id'=>$id));
			$selfstory = $story['selfstory']?$story['selfstory']:'0';
			
			//推荐剧/自戏统计
			$count = $this->story_model->getQuery('select count(id) as nums from IMStory where priority >0 and storystatus=0 and selfstory='.$selfstory);
			
			if($count[0]['nums']>=3){
				echo 'overtop';
				exit;
			}else{
				if($count[0]['nums']<1){
					$priority=$flag;
				}else{
					$priority=$flag+$count[0]['nums'];
				}
			}
		}else{
			$priority=0;
		}
		
		$result = $this->story_model->updateByWhere(array('priority'=>$priority), 'id', $id);
		if($result){
			echo 'success';
		}
		exit;
	} */
        
        public function edit()
	{
		$params = array(
			'priority'=>$this->input->post('priority')
		);
		$id = $this->input->post('id');
		$result = $this->story_model->update($params,$id);
		if($result){
			echo 'success';
		}
	}
	
	/**
	 * 修改阅读权限
	 */
	public function permission(){
		$params = array(
				'permission'=>$this->input->post('permission')==1?0:1
		);
		$id = $this->input->post('id');
		$result = $this->story_model->update($params,$id);
		if($result){
			echo 'success';
		}
	}
        
        public function get()
	{
		$id = $this->input->post('id');
		$result = $this->story_model->getOne(array('id'=>$id));
		if($result){
			echo json_encode($result);
		}
	}
	
	/**
	 * story 精选设置
	 */
	public function priority(){
		$this->load->view('base/header');
		$this->load->view('base/story');
		$this->load->view('base/footer');
	}
	
	/**
	 * 导出昨日剧
	 */
	public function export(){
		
		$sql = "select id,title,FROM_UNIXTIME(createtime,'%Y-%m-%d %H:%i:%s') as datetime,creatorid from IMStory
				where createtime>=".strtotime('yesterday')." and createtime<".strtotime('today')." and selfstory=0 and storystatus=0";
		$list = $this->story_model->getQuery($sql);
		$this->list_export($list);
	}
	
	public function list_export($list){
		header("Content-type: text/html; charset=utf-8");
		include_once APPPATH . "libraries/PHPExcel/IOFactory.php";
	
		//创建一个excel对象
		$objPHPExcel = new PHPExcel();
		// Set properties
	
		$objPHPExcel->getProperties()->setCreator("ctos")
		->setLastModifiedBy("ctos")
		->setTitle("Office 2007 XLSX Test Document")
		->setSubject("Office 2007 XLSX Test Document")
		->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
		->setKeywords("office 2007 openxml php")
		->setCategory("Test result file");
	
		//set width
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
	
		//set font size bold
		$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
		$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFont()->setBold(true);
	
		$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	
		//$objPHPExcel->getActiveSheet()->mergeCells('A1:J1');
	
		// set table header content
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A1', '编号')
		->setCellValue('B1', '剧名')
		->setCellValue('C1', '创建时间')
		->setCellValue('D1', '创建人');
	
	
		// Miscellaneous glyphs, UTF-8
		foreach ($list as $i=>$l){
			$objPHPExcel->getActiveSheet(0)->setCellValue('A' . ($i + 2), $l['id']);
			$objPHPExcel->getActiveSheet(0)->setCellValue('B' . ($i + 2), $l['title']);
			$objPHPExcel->getActiveSheet(0)->setCellValue('C' . ($i + 2), $l['datetime']);
			$objPHPExcel->getActiveSheet(0)->setCellValue('D' . ($i + 2), $l['creatorid']);
		}
	
		// Rename sheet
		$objPHPExcel->getActiveSheet()->setTitle(date('Y-m-d',strtotime('yesterday')).'创建的剧');
	
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
	
		// Redirect output to a client’s web browser (Excel5)
		ob_end_clean();//清除缓冲区,避免乱码
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="数据统计11.19.xls"');
		header('Cache-Control: max-age=0');
	
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	}
}