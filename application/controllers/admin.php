<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH."core/TT_Controller.php");

class Admin extends TT_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('admin_model');
		$this->load->model('admin_power_model');
		$this->load->model('permission_model');
		//$this->check_power('admin');
	}

	public function index()
	{
		$this->config->site_url();
		$this->load->view('base/header');
		$this->load->view('auth/admin');
		$this->load->view('base/footer');
	}

	public function all()
	{
		//分页
		$start = $this->input->get('start');
		if(!$start){
			$start =  0;
		}
		$perpage = 10;
		
		//搜索ID
		$keyID = intval($_GET['keyID']);
		$param['status'] = 0;
		if($keyID){
			$param['id'] = $keyID;
		}
		
		//搜索昵称
		$like = array();
		$keyuname = trim($_GET['keyuname']);
		if($keyuname){
			$like = array('uname'=>$keyuname);
		}
		
		$admin = $this->admin_model->getList($param, '*', $start*$perpage, $perpage,'id','asc',$like);
		
		//权限管理
		$permissions = $this->permission_model->getList();
		foreach($permissions as $p){
			$per[$p['id']] = $p['rolename'];
		}
		
		//数据组合
		foreach($admin as $key=>$a){
			$admin[$key]['rolename'] = $a['permission']?$per[$a['permission']]:'';
			$admin[$key]['updated'] = $a['updated']?date('Y-m-d H:i',$a['updated']):'';
		}
		
		//统计个数
		$count = $this->admin_model->getCount($param,$like);
		
		//删除超级管理员 
		unset ($permissions[0]);
		
		$result = array(
			'admins'=>$admin,
			'permissions' => $permissions,
			'page'=>$start,
			'count'=>ceil($count/10),
		);
		echo json_encode($result);
	}

	public function del()
	{
		$id = $this->input->post('id');
		$result = $this->admin_model->update(array('status'=>1), $id);
		if($result){
			echo 'success';
		}
	}

	/**
	 * 添加
	 */
	public function add()
	{
		$params = array(
			'nick'=>$this->input->post('nick'),
			'uname'=>$this->input->post('uname'),
			'permission'=>$this->input->post('permission'),
			'pwd'=>md5($this->input->post('pwd')),
			'status'=>'0',
			'created'=>time(),
			//'updated'=>time()
		);
		$result = $this->admin_model->insert($params);
		if($result){
			echo 'success';
		}
	}

	/**
	 * 修改
	 */
	public function edit()
	{
		$params = array(
			'nick'=>$this->input->post('nick'),
			'uname'=>$this->input->post('uname'),
			'permission'=>$this->input->post('permission'),
			//'updated'=>time()
		);
		$pwd = $this->input->post('pwd');
		if($pwd)
			$params['pwd'] = md5($pwd);
		
		$id = $this->input->post('id');
		
		//数据修改
		$result = $this->admin_model->update($params,$id);
		if($result){
			echo 'success';
		}
	}

	public function get()
	{
		$id = $this->input->post('id');
		$result = $this->admin_model->getOne(array('id'=>$id));
		if($result){
			echo json_encode($result);
		}
	}

	public function upload()
	{
		include_once APPPATH."libraries/image_moo.php";

		try{
		    $filename=$this->input->get('filename');
		    $ext = pathinfo($filename, PATHINFO_EXTENSION);
		    $filename = time().".".$ext;
		    $input = file_get_contents("php://input");
		    file_put_contents('./download/'.$filename, $input);
		    // $image = new Image_moo();
		    // $image
		    // 	->load('./download/'.$filename)
		    // 	->resize_crop(100,100)
		    // 	->save('./download/1.jpg');
		    // 裁剪头像
		    // $targ_w = $targ_h = 100;
		    // $jpeg_quality = 90;

		    // $img_r = imagecreatefromjpeg('./download/'.$filename);
		    // $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

		    // imagecopyresampled($dst_r,$img_r,0,0,0,0,
		    //     $targ_w,$targ_h,100,100);

		    // header('Content-type: image/jpeg');
		    // imagejpeg($dst_r, './download/'.$filename, $jpeg_quality);



		    $res = $this->_upload('./download/'.$filename);
		    if($res['error_code'] == 0){	    	
			    $array = array(
			    	'status' =>'success',
			    	'file' =>$res['path'],
			    	'real_path'=>$this->config->config['msfs_url'].$res['path']
			    );
		    }else{
		    	$array = array(
			    	'status' =>'fail',
			    	'file' =>'',
			    	'real_path'=>''
			    );
		    }
			echo json_encode($array);
		}
		catch(Exception $e)
		{
			$array = array(
				'status' =>'fail',
				'file' =>0
			);
			echo json_encode($array);
		}
	}

	public function _upload($filename)
	{
		$ch = curl_init();
		$data = array('filename'=>'@'.$filename);
		curl_setopt($ch,CURLOPT_URL,$this->config->config['msfs_url']);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch,CURLOPT_POST,true);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
		$result = curl_exec($ch);
		curl_close($ch);
		return json_decode($result,1);
	}

}