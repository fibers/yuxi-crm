<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH."core/TT_Controller.php");

class Priority extends TT_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('story_model');
	}

	/**
	 * 推荐模块
	 */
	public function index()
	{
		//剧精选查询
		$data['story'] = $this->story_model->getQuery('select id,priority from IMStory where `priority` in (600,601,602) and storystatus=0 and selfstory=0 order by priority desc');
		
		//自戏精选查询
		$data['selfstory'] = $this->story_model->getQuery('select id,priority from IMStory where `priority` in (600,601,602) and storystatus=0 and selfstory=1 order by priority desc');
		
		$this->load->view('base/header');
		$this->load->view('base/priority',$data);
		$this->load->view('base/footer');
	}
	
	/**
	 * 更改剧推荐
	 */
	public function updatepriority(){
		//排序
		$ids = $this->input->post('ids');
		$selfstory = $this->input->post('selfstory');
		if(!$ids){
			echo 'no';
		}
		
		//将之前推荐的数据清空
		$this->db->query("UPDATE `IMStory` SET `priority` = '200' WHERE `priority` in (600,601,602) AND `storystatus` =  '0' AND `selfstory`=$selfstory ");
		
		//更新推荐管理三个数据值
		$ids = rtrim($ids,',');
		$id_array = explode(',', $ids);
		$flag = 600;
		foreach ($id_array as $key => $id){
			if($id){
				$param = array('priority'=>$flag+(2-$key));
				$this->story_model->update($param,$id);
			}
		}
		echo 'ok';
	}

}