<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH."core/TT_Controller.php");

class Banners extends TT_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
        $this->load->model('banners_model');
		$this->load->model('story_model');
		$this->check_power('banners');
	}
	
	/**
	 * 细菌加好友
	 */
	/* public function user_friend()
	{
	
		//数据条件
		$list = array();
		$strsql = "select id from IMUser where id >1000050";
	
		mysql_query("SET NAMES UTF8");
		//数据连接
		$im_conn = $this->im_db_zs();
		$query	= mysql_query($strsql, $im_conn);
		while($_list=mysql_fetch_assoc($query)){
			$list[] = $_list;
		}
	
	
		if ($list) {
			//数据组合
			$time = time();
			foreach($list as $key => $l){
	
				$ids =  ($key%6)+1;
				$key_id = '100000'.$ids;
	
	
				$sql = "insert into IMFriend (`uid`,`friendid`,`created`,`uidgroup`,`friendidgroup`) VALUES (".$l['id'].",$key_id,$time,0,0)";
				//echo $sql;
				$query	= mysql_query($sql, $im_conn);
	
				//组合数据
				$param[] = array (
				 		'uid' => $l['id'],
						'friendid' => $key_id,
						'created' =>$time,
						'uidgroup' => 0,
						'friendidgroup' =>0
				); 
			}
				
			//IMFriend
			if($param){
			 	$this->selfstoryworday_model->insert_batch ( $param );
			}
		}
	} */

	public function index()
	{
		$this->load->view('base/header');
		$this->load->view('base/banners');
		$this->load->view('base/footer');
	}

	/**
	 * 全部列表
	 */
	public function all()
	{
		$perpage = 10000;
		$start = $this->input->get('start');
		$title = $this->input->get('title');
		if(!$start){
			$start =  0;
		}
		$like=array();
		$str_like='';
		if($title){
			$like=array('title'=>$title);
			$str_like=" and title like '%".$like."%' ";
		}
		$perpage = 200;
		$list = $this->banners_model->getList(array(), '*', $start*$perpage, $perpage,'id','asc',$like);
		if(count($list)>0){
			foreach($list as $key=>$li){
				$story=array();
				$story=$this->story_model->getOne(array('id'=>$li['storyid']));
				$list[$key]['story_name']='';
				if($story){
					$list[$key]['story_name']=$story['title'];
				}
				
				//点击量
				$list[$key]['banners_hit'] = '0';
				if($li['format']==1){
					$this->load->database('db_im',true);
					$list[$key]['banners_hit'] = $this->db->where(array('url'=>$li['url']))->from('bannersVisit')->count_all_results();
				}
				
				$this->load->database('default',true);
			}
		}
		
		//获取总数
		$count = $this->banners_model->getQuery('select count(id) as nums from banners where 1=1 '.$str_like);
		
		//获取剧列表
		$result = array(
			'title'=>$title,
			'banners'=>$list,
			'page'=>$start,
			'count'=>@ceil((int)$count[0]['nums']/$perpage)
		);
		
		echo json_encode($result);
	}
	
	public function story()
	{
		$perpage = 10000;
		$title = $this->input->get('title');
		$like = $storyList = array();
		if($title){
			$like=array('title'=>$title);
		}
		$storyList = $this->story_model->getList(array('storystatus'=>0), '*', 0,$perpage,'id','desc',$like);
		$result = array(
			'story' =>$storyList?$storyList:'fail'
		);
		
		echo json_encode($result);
	}

	public function del()
	{
		$id = $this->input->post('id');
		$result = $this->banners_model->delete($id,true);
		if($result){
			echo 'success';
		}
	}

	public function add()
	{
		$format=0;
		$url=$this->input->post('url');
		if($url){
			$format=1;
		}
		$params = array(
			'storyid'   => $this->input->post('storyid'),
			'image' 	=> $this->input->post('image'),
			'title'		=> $this->input->post('title'),
			'url'	    => $url,
			'format'	=> $format
		);
		
		$result = $this->banners_model->insert($params);
		if($result){
			echo 'success';
		}
	}
	

	
	public function upload()
	{
		include_once APPPATH."libraries/image_moo.php";
		include_once APPPATH."libraries/upyun/config.php";
		include_once APPPATH."libraries/upyun/upyun.class.php";
		//require_once('../upyun.class.php');
		//require_once('../config.php');
		$upyun = new UpYun($config['bukername'], $config['user_name'], $config['pwd']);
		try{
		    $filename=$this->input->get('filename');
		    $ext = pathinfo($filename, PATHINFO_EXTENSION);
		    $filename = time().".".$ext;
		    $input = file_get_contents("php://input");
		    
		    
		    //图片上传
		    $upyunFileName = '/banner/'.date('Ymd',time()).'/'.$filename;
		    $rsp = $upyun->writeFile($upyunFileName, $input, True);   // 上传图片，自动创建目录
		    
		    /* file_put_contents('./download/'.$filename, $input);
			
			//图片上传
			$upyunFileName = '/banner/'.date('Ymd',time()).'/'.$filename;
			$fh = fopen('./download/'.$filename, 'rb');
			$rsp = $upyun->writeFile($upyunFileName, $fh, True);   // 上传图片，自动创建目录
			fclose($fh); */
			$array = array(
			    	'status' =>'success',
			    	'file' =>'http://cosimage.b0.upaiyun.com'.$upyunFileName,
			    	'real_path'=>'http://cosimage.b0.upaiyun.com'.$upyunFileName
			    );
			
		    //$res = $this->_upload('./download/'.$filename);
		    /*if($res['error_code'] == 0){	   	
			    $array = array(
			    	'status' =>'success',
			    	'file' =>'http://'.$this->config->config['msfs_url'].'/download/'.$filename,
			    	'real_path'=>'http://'.$this->config->config['msfs_url'].'/download/'.$filename
			    );
				//$upyun = new UpYun($config['bukername'], $config['user_name'], $config['pwd']);
				//$dir=str_replace("\\", '/', substr(dirname(__FILE__), 0, -23));
				//$fh = fopen($dir.'download/'.$filename, 'rb');
				//$rsp = $upyun->writeFile($dir.'download/'.$filename, $fh, true);   // 上传图片，自动创建目录
				//fclose($fh);
				//var_dump($rsp);exit;
				
		    }else{
		    	$array = array(
			    	'status' =>'fail',
			    	'file' =>'',
			    	'real_path'=>''
			    );
		    }*/
			echo json_encode($array);
		}
		catch(Exception $e)
		{
			$array = array(
				'status' =>'fail',
				'file' =>0
			);
			echo json_encode($array);
		}
	}

	public function _upload($filename)
	{
		$ch = curl_init();
		$data = array('filename'=>'@'.$filename);
		curl_setopt($ch,CURLOPT_URL,$this->config->config['msfs_url']);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch,CURLOPT_POST,true);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
		$result = curl_exec($ch);
		curl_close($ch);
		return json_decode($result,1);
	}
	
	

	public function edit()
	{
		$format=0;
		$url=$this->input->post('url');
		if($url){
			$format=1;
		}
		$id = $this->input->post('id');
		$params = array(
			'storyid'   => $this->input->post('storyid'),
			'image' 	=> $this->input->post('image'),
			'title'		=> $this->input->post('title'),
			'url'	    => $url,
			'format'	=> $format
		);
		$orders=$this->input->post('orders');
		if($orders !=$id){
			$info = $this->banners_model->getOne(array('id'=>$orders));
			if($info){
				echo 'orderErr';
				exit;
			}
		}
		$params['id']=$orders;
		$result = $this->banners_model->updateByWhere($params, 'id', $id);
		if($result){
			echo 'success';
		}
	}

	public function get()
	{
		$id = $this->input->post('id');
		$info = $this->banners_model->getOne(array('id'=>$id));
		
		$story=array();
		//剧编号是否存在
		if($info['storyid'])
			$story=$this->story_model->getOne(array('id'=>$info['storyid']));
		
		$info['story_name']='';
		if($story){
			$info['story_name']=$story['title'];
		}
		echo json_encode($info);
	}
}