<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH."core/TT_Controller.php");

class Common extends TT_Controller {

	public function upload()
	{
		include_once APPPATH."libraries/image_moo.php";
		include_once APPPATH."libraries/upyun/config.php";
		include_once APPPATH."libraries/upyun/upyun.class.php";
		$upyun = new UpYun($config['bukername'], $config['user_name'], $config['pwd']);
		
		try{
		    $filename=$this->input->get('filename');
		    
		    $ext = pathinfo($filename, PATHINFO_EXTENSION);
		    $filename = time().'_'.rand(1000,9999).".".$ext;
		    $input = file_get_contents("php://input");
		    
		    //图片上传
		    $upyunFileName = '/yuxi/'.date('Y',time()).'/'.date('n',time()).'/'.$filename;
		    $rsp = $upyun->writeFile($upyunFileName, $input, True);   // 上传图片，自动创建目录
		    
		    /* file_put_contents('./download/'.$filename, $input);
			//图片上传
			$upyunFileName = '/banner/'.date('Ymd',time()).'/'.$filename;
			$fh = fopen('./download/'.$filename, 'rb');
			$rsp = $upyun->writeFile($upyunFileName, $fh, True);   // 上传图片，自动创建目录
			fclose($fh); */
			$array = array(
			    	'status' =>'success',
			    	'file' =>'http://cosimage.b0.upaiyun.com'.$upyunFileName,
			    	'real_path'=>'http://cosimage.b0.upaiyun.com'.$upyunFileName
			    );
			
			echo json_encode($array);
		}
		catch(Exception $e)
		{
			$array = array(
				'status' =>'fail',
				'file' =>0
			);
			echo json_encode($array);
		}
	}

}