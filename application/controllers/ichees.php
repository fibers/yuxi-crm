<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// error_reporting(0);
error_reporting(E_ERROR | E_WARNING | E_PARSE);
// include_once(APPPATH."core/TT_Controller.php");

class Ichees extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Ichees_Model');
	}

	public function index()
	{
		// $this->load->view('base/header');
		// $this->load->view('base/home');
		// $this->load->view('base/footesr');
		
		$this->copyright();
		
	}


       
	// 版权页面
	public function copyright() {

		//获得groupid
		// http://127.0.0.1/chudong/index.php/ichees/copyright?groupid=82
		$storyid = $this->input->get('storyid');
		if(empty($storyid)) {
			$storyid = 1;
		}

		$data['storyInfo'] = $this->Ichees_Model->getStoryInfo($storyid);

		$data['greaterList'] = $this->Ichees_Model->getGreaterList($storyid); 

		$totleCount = 0;

		if(empty($data['greaterList'])) {
			$data['greaterList'][0]['creatorname'] = '暂无数据';
		}
		else {
			foreach ($data['greaterList'] as $key => $value) {
				$totleCount += $value['counter'];
			}
			foreach ($data['greaterList'] as $key => $value) {
				if($totleCount) {
					$rate = $value['counter']/$totleCount*100;
				}
				else {
					$rate = 0;
				}
				$data['greaterList'][$key]['rate'] = sprintf("%.2f",$rate);
			}
		}
		

			
		$this->load->view('ichees/copyright',$data);
	}

        public function test() {
           echo 2;
            echo encode_msg('1', 1);
         }
	// 阅读页面
	public function read() {
		//微信分享
		include_once APPPATH."libraries/jssdk.php";
		$jssdk = new JSSDK("wxaee58422b655da32", "9d533f13e7d43a50a200be2c0393e88d");
		$data['signPackage'] = $jssdk->GetSignPackage();
		
		$storyid = $this->input->get('storyid');
		if(empty($storyid)) {
			$storyid = 1;
		}

		$data['storyInfo'] = $this->Ichees_Model->getStoryInfo($storyid);
		if(empty($data['storyInfo']['groupid'])) {
			$data['chapterList'][] = array('content' => 0);
		}
		else {
			
			$msg_list = $this->Ichees_Model->getChapterList($storyid);
			$data['chapterList'] = $msg_list['chapterList'];
			$data['story_user'] = $msg_list['story_user'];
		}
		
		
		// echo '<pre>';
		// var_dump($data['chapterList']);
		// echo '</pre>';
		//print_r($data);
		$this->load->view('ichees/read',$data);
	}
	
	public function active_page(){
		$storyid = $this->input->get('storyid');
		if(empty($storyid)) {
			$storyid = '1';
		}
		$data['storyid'] = $storyid;
		$this->load->view('ichees/active_read',$data);
	}
	
	//11..16活动测试用第一次调用页面
	public function active_read(){
		$story_id = (int)$this->input->get('story_id');
		if(empty($story_id)) {
			echo json_encode(array('code'=>'-1','msg'=>'刷新重试'));
			exit;
		}
		
		$data['storyInfo'] = $this->Ichees_Model->getStoryInfo($story_id);
		$max_msgId = '0';
		if(empty($data['storyInfo']['groupid'])) {
			$data['chapterList'] = array();
		}else {
			//根据戏文id获得对戏群id
			$group_id = $this->Ichees_Model->getGroupBystory($story_id);
			if($group_id){
				$data['chapterList'] = $this->Ichees_Model->getGroupsMessageList($group_id,$story_id);
				
				//获取当前剧最大的msgId
				$max_msgId = $this->Ichees_Model->getMaxMsg($group_id);
			}
		}
		
		$data['max_msgId'] = $max_msgId;
		$data['code'] = '1';
		$data['msg'] = 'ok';
		//print_r($data);
		echo json_encode($data);
	}
	
	//11.16活动测试，第二次调用页面
	public function ajax_read(){
		$story_id = (int)$this->input->get('story_id');
		$max_msgId = (int)$this->input->get('max_msgId');
		
		$group_id = $this->Ichees_Model->getGroupBystory($story_id);
		if($group_id){
			$data['chapterList'] = $this->Ichees_Model->getGmMsg($group_id,$max_msgId,$story_id);
		
			//获取当前剧最大的msgId
			$max_msgId = $this->Ichees_Model->getMaxMsg($group_id);
		}
		
		$data['max_msgId'] = $max_msgId;
		$data['code'] = '1';
		$data['msg'] = 'ok';
		echo json_encode($data);
	}

	// 自戏页面
	public function selfstory() {
		//微信分享
		include_once APPPATH."libraries/jssdk.php";
		$jssdk = new JSSDK("wxaee58422b655da32", "9d533f13e7d43a50a200be2c0393e88d");
		$data['signPackage'] = $jssdk->GetSignPackage();
		
		$storyid = $this->input->get('storyid');
		if(empty($storyid)) {
			$storyid = 1;
		}
		
		$data['selfStoryInfo'] = $this->Ichees_Model->getSelfStoryInfo($storyid);

		$this->load->view('ichees/selfstory',$data);
	}
	

}
