<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH."core/TT_Controller.php");

class Topic extends TT_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->check_power('topic');
		$this->load->helper('url');
		$this->load->model('group_model');
		$this->load->model('group_message_model');
		$this->load->model('user_model');
		$this->load->model('grouprelation_model');
	}

	public function index()
	{
		$this->load->view('base/header');
		$this->load->view('base/topic');
		$this->load->view('base/footer');
	}

	public function all() {
		//获取数据
		$od = $this->input->get ( 'od' );
		$od = (! empty ( $od )) ? ($od) : ('id');
		
		$name = $this->input->get ( 'name' );
		$like = array();
		if(! empty ( $name )) {
			$like = array( 'IMGroup.name'=>$name );
		}
		
		//统计个数
		$count = $this->group_model->getCount ( array (
				'status' => 0,
				'isTopic' => 1 
		) ,$like);
		
		// $groups =
		// $this->group_model->getList(array('status'=>0,'isTopic'=>1),
		// '*',$offset, $perpage);
		
		//当前页数
		$start = $this->input->get ( 'start' );
		if (! $start) {
			$start = 0;
		}
		
		//条件
		$where = array (
				'IMGroup.status' => 0,
				'IMGroup.isTopic' => 1
		);
		$groups = $this->group_model->allList($start,$where,$like,$od);
		
		// $data = array();
		// foreach ($groups as $key => $value) {
		// if($groups[$key]['avatar']){
		// $groups[$key]['avatar_value'] =
		// $this->config->config['msfs_url'].$groups[$key]['avatar'];
		// }
		// }
		$result = array (
				'groups' => $groups,
				'page' => $start,
				'count' => ceil ( $count / 10 ) 
		);
		echo json_encode ( $result );
	}
	
	/* public function search() {
		$kwd = $_GET ['kwd'];
		$groups = $this->group_model->getLike ( array (
				'name' => $kwd 
		), array (
				'status' => 0,
				'isTopic' => 1 
		), '*', 0, 100 );
		$status = (! empty ( $groups ) && count ( $groups ) > 0) ? ('ok') : ('empty');
		$result = array (
				'groups' => $groups,
				'status' => $status 
		);
		echo json_encode ( $result );
	} */

	public function del()
	{
		$id = $this->input->post('id');
		$result = $this->group_model->updateByWhere(array('status'=>1), 'id', $id);
		if($result){
			echo 'success';
		}
	}
        
    /**
     * 置顶
     */
    /* public function settop()
	{
		$id = $this->input->post('id');
                $re = $this->group_model->getQuery("select count(*) as num from IMGroup where status = 0 and isTopic=1 and priority >0 limit 5 ");
                $num = $re['0']['num'];
                $flag = 100;
                if($num > 0){
                    if($num>2){
                        echo 'full';exit;
                    }
                    $rankNum = ($num == 1)?($flag+1):($flag+2);
                }else{
                    $rankNum  = $flag;
                }
		$result = $this->group_model->updateByWhere(array('priority'=>$rankNum), 'id', $id);
		if($result){
			echo 'success';
		}
	} */
	
	/**
	 * 取消置顶
	 */
	/* public function unsettop() {
		$id = $this->input->post ( 'id' );
		$result = $this->group_model->updateByWhere ( array (
				'priority' => 0 
		), 'id', $id );
		if ($result) {
			echo 'success';
		}
	} */

	public function add()
	{
		$array = array(
			'req_user_id' 	=> 0,
			'app_key'		=> 'asdfasdf',
			'group_name'	=> $this->input->post('name'),
			'group_type'	=> 1,
			'group_avatar'	=> '',
			'user_id_list'	=> array(1)
		);
		$res = $this->httpRequest($this->config->config['http_url'].'/query/CreateGroup','post',json_encode($array));
		$res = json_decode($res,1);
		if($res['error_code'] == 0){
			echo 'success';
		}else{
			echo 'fail';
		}
	}

	public function edit()
	{
		$params = array(
			'priority'=>$this->input->post('priority'),
			'avatar'=>$this->input->post('avatar')
		);
		
		$id = $this->input->post('id');
		$result = $this->group_model->updateByWhere($params, 'id', $id);
		if($result){
			echo 'success';
		}
	}

	public function get()
	{
		$id = $this->input->post('id');
		$result = $this->group_model->getOne(array('id'=>$id));
		if($result){
			echo json_encode($result);
		}
	}

	public function getMember()
	{
		$id = $this->input->post('id');
		$perpage = 10000;
		$users = $this->grouprelation_model->getList(array('status'=>0,'groupId'=>$id), '*', 0, $perpage);
		foreach ($users as $key => $value) {
			$_data = $this->user_model->getOne(array('id'=>$value['userId']));
			$users[$key]['name'] = $_data['name'];
		}
		$data = array();
		$result = array(
			'users'=>$users,
		);
		echo json_encode($result);
	}

	public function changeMember()
	{
		$add = array(
			'req_user_id'   => 0,
			'app_key'       => 'asdfasdf',
			'group_id'      => intval($this->input->post('id')),
			'modify_type'   => intval($this->input->post('change')),
			'user_id_list'  => array(intval($this->input->post('userId')))
		);                  
	    $res = $this->httpRequest($this->config->config['http_url'].'/query/ChangeMembers','post',json_encode($add));
	}

	public function httpRequest($url,$method,$params=array()){
		$curl=curl_init();
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($curl,CURLOPT_HEADER,0 ) ;
		curl_setopt($curl,CURLOPT_URL,$url);
		curl_setopt($curl,CURLOPT_POST,1 );
		curl_setopt($curl, CURLOPT_POSTFIELDS,$params);
		$result=curl_exec($curl);
		curl_close($curl);
		return $result;
	}
        
	/**
	 * 查询话题的回复信息
	 */
    public function topicMessage(){
    	//话题基础数据
    	$groupid = $this->input->get('groupid');
    	$data['topic'] = $this->group_model->getOne(array('id'=>$groupid));
    	
    	//评论数据
    	$data['message'] = $this->group_message_model->allList($groupid);
    	foreach ($data['message'] as $key=>$m){
    		$data['message'][$key]['content'] = $this->aesLocalDecode($m['content']);;
    		$data['message'][$key]['created'] = date('Y-m-d H:i',$m['created']);
    	}
    	
    	$this->load->view('base/header');
    	$this->load->view('base/topic_message',$data);
    	$this->load->view('base/footer');
    }

    //回复信息列表
    /* public function messageAll(){
    	$groupid = $this->input->get('groupid');
    	
    	$result = $this->group_message_model->allList($groupid);
    	foreach ($result as $key=>$m){
    		$result[$key]['content'] = $this->aesLocalDecode($m['content']);;
    		$result[$key]['created'] = date('Y-m-d H:i',$m['created']);
    	}
    	
    	if($result){
    		echo json_encode($result);
    	}
    } */
    
    /**
     * 话题回复消息删除
     */
	public function message_del() {
		//参数检查
		$id = $this->input->post ( 'id' );
		$groupId = $this->input->post ( 'groupId' );
		if(!$id || !$groupId){
			echo 'no';exit;
		}
		
		$result = $this->group_message_model->message_del($id,$groupId);
		if ($result) {
			echo 'success';
		}
	}
	
    //add by guoq-s
    private function aesLocalDecode($emsg)
    {
    	$command = './decode ' . EscapeShellArg($emsg);
    	$value = `$command`;
    	$value = str_replace('&$#@~^@[{:','<img src="',$value);
    	$value = str_replace(':}]&$~@#@','">',$value);
    	return $value ; 
    }
}