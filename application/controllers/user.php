<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH."core/TT_Controller.php");

class User extends TT_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->check_power('user');
		$this->load->helper('url');
		$this->load->model('user_model');
		$this->load->model('depart_model');
	}
	
	public function count_user(){
		//用户整理		
		
		
		/* $sql ="select sc.id,u.issigned from story_count sc join IMUser u on sc.creatorid = u.id ";
		$list = $this->db->query($sql)->result_array();
		
		$this->db->update_batch('story_count',$list,'id'); */
		
		//剧分类拆分
		/* $sql ="select s_id,title,category,group_id,text_cnt,user_c,message_c,category_name from story_count";
		$list = $this->db->query($sql)->result_array();
		
		foreach ($list as $k=>$v){
			if($v['category_name']){
				$cate_name = explode(',', $v['category_name']);
			}
			
			if(isset($cate_name)){
				foreach ($cate_name as $c){
					$v['category_name'] = $c;
					$list_c[] = $v;
				}
			}else{
				$list_c[] = $v;
			}
		}
		
		$this->db->insert_batch('story_count_cate', $list_c); */
		
		//剧自戏字数叠加
		/* $sql ="select id,story_text,self_text from IMUser";
		$list = $this->db->query($sql)->result_array();
		foreach($list as $k=>$v){
			
			$text_cnt = $v['story_text']+$v['self_text'];
				
			$list_f['id'] = $v['id'];
			$list_f['text_cnt'] = $text_cnt;
			$list_text[] =  $list_f;
		}
		$this->db->update_batch('IMUser',$list_text,'id'); */
		
		
		//好友个数
		/* $sql ="select id from IMUser";
		$list = $this->db->query($sql)->result_array();
		foreach($list as $k=>$v){
			$sql_f = "select count(id) as friend_count from IMFriend where uid=".$v['id']." or friendid=".$v['id'];
			$list_f = $this->db->query($sql_f)->row_array();
			
			if(isset($list_f)){
				$list_f['id'] = $v['id'];
				$list_f['friend_count'] = isset($list_f)?$list_f['friend_count']:'0';
				$list_friend[] =  $list_f;
			}
			
		}

		$this->db->update_batch('IMUser',$list_friend,'id'); */
		
		
		//写作天数
		/* for($i=0;$i<8;$i++){
			$table_id = 'IMGroupMessage_'.$i;
			$sql = "select userid as userid,FROM_UNIXTIME(created,'%Y-%m-%d') as mes_days from ".$table_id." group by mes_days,userid";
			$list = $this->db->query($sql)->result_array();
			
			$this->db->insert_batch('user_message', $list);
		}
		$sql = "select userid as id,count(DISTINCT(mes_days)) as mes_days from user_message group by userId";
		$list = $this->db->query($sql)->result_array();
		$this->db->update_batch('IMUser',$list,'id');*/
		
		//登陆天数
		/* $sql = "select userid as id,count(DISTINCT(FROM_UNIXTIME(uptime,'%Y-%m-%d'))) as online_days from IMOnlineUsers group by userid";
		$list = $this->db->query($sql)->result_array();
		//print_r($list);
		
		$this->db->update_batch('IMUser',$list,'id'); */
		
		//剧整理
		/* $sql = "select id,s_id,group_id from story_count";
		$list = $this->db->query($sql)->result_array();
		//print_r($list);
		
		foreach($list as $k=>$v){
			$message_i = $v['group_id']%8;
			$table_id = 'IMGroupMessage_'.$message_i;
			echo $table_id;
			$list_c = $list_u = array();
			if($v['group_id']){
				$sql_c = " select count(id) as ids from ".$table_id." where groupId =".$v['group_id'];
				$list_c = $this->db->query($sql_c)->row_array();
	
	
				$sql_u = " select count(DISTINCT(userId)) as userIds from ".$table_id." where groupId =".$v['group_id'];
				$list_u = $this->db->query($sql_u)->row_array();
			}
			
			$message_c = $list_c?$list_c['ids']:'0';
			$user_c = $list_u?$list_u['userIds']:'0';
				
			$this->db->set('message_c', $message_c);
			$this->db->set('user_c', $user_c);
			$this->db->where('id', $v['id']);
			$this->db->update('story_count');
		}
		 */
		//$this->db->insert_batch($this->table_name, $list);
		
		//剧分类
		/* foreach($list as $k=>$v){
			$list_c = array();
			if($v['category']){
				$sql = "select GROUP_CONCAT(name) as category_name from IMStoryCategory where id in (".$v['category'].")";
				$list_c = $this->db->query($sql)->row_array();
			}
			
			$category_name = $list_c?$list_c['category_name']:'0';
			$this->db->set('category_name', $category_name);
			$this->db->where('id', $v['id']);
			$this->db->update('story_count');
		} */
		
		
		//循环剧字数写入数据库
		/*$q = $this->db->select('*', false)->get('IMGroupMsgTextCount');
		$list = $q->result_array();
		
		foreach($list as $k=>$v){
			$this->db->set('story_text', 'story_text+'.$v['text_cnt'],false);
			$this->db->where('id', $v['user_id']);
			$this->db->update('IMUser');
		}*/
		
		//循环自戏字数写入数据库
		/*
		$sql = "SELECT A.storyid as story_id, A.wordcount as text_cnt, A.userid as user_id, A.time as updated  FROM selfstoryword A,
		(SELECT storyid, MAX(time) max_time FROM selfstoryword GROUP BY storyid ) B
		WHERE A.storyid = B.storyid AND A.time = B.max_time ORDER BY A.time DESC";
		$list = $this->db->query($sql)->result_array();

		
		foreach($list as $k=>$v){
			$this->db->set('self_text', 'self_text+'.$v['text_cnt'],false);
			$this->db->where('id', $v['user_id']);
			$this->db->update('IMUser');
		}
		*/
		
		/*$q = $this->db->select('id,creatorid,selfstory', false)->get('IMStory');
		$list = $q->result_array();
		
		foreach($list as $k=>$v){
			if($v['selfstory']=='1'){
				$this->db->set('self_created', 'self_created+1',false);
			}else{
				$this->db->set('story_created', 'story_created+1',false);
			}
			$this->db->where('id', $v['creatorid']);
			$this->db->update('IMUser');
		}*/
		
		//where uptime>=1441814400 and uptime<1446998400
		/*$sql = "select FROM_UNIXTIME(uptime,'%Y-%m-%d') days,count(id) count,userid from IMOnlineUsers  group by days,userid order by userid";
		$list = $this->db->query($sql)->result_array();
		$lists = array();
		foreach($list as $k=>$v){
			if(isset($lists[$v['userid']])){
				$lists[$v['userid']] += $v['count'];
			}else{
				$lists[$v['userid']] = $v['count'];
			}
		}
		
		
		foreach($lists as $kk=>$v){
			$this->db->set('total_online_time', 'total_online_time+'.$v,false);
			$this->db->where('id', $kk);
			$this->db->update('IMUser');
		}*/
		
		/*在线时长分布数据*/
		/* $sql = "select FROM_UNIXTIME(uptime,'%Y-%m-%d') days,count(id) count,userid from IMOnlineUsers group by days,userid order by days";
		$list = $this->db->query($sql)->result_array();
		$lists = array();
		foreach($list as $k=>$v){
			if($v['count']<=1){
				$count_time = 1;
			}elseif($v['count']>1 && $v['count']<=3){
				$count_time = 2;
			}elseif($v['count']>3 && $v['count']<=5){
				$count_time = 3;
			}elseif($v['count']>5 && $v['count']<=10){
				$count_time = 4;
			}elseif($v['count']>10 && $v['count']<=30){
				$count_time = 5;
			}elseif($v['count']>30 && $v['count']<=60){
				$count_time = 6;
			}else{
				$count_time = 7;
			}
			
			//函数赋值
			if(isset($lists[$v['days']])){
				if(isset($lists[$v['days']][$count_time])){
					$lists[$v['days']][$count_time]++;
				}else{
					$lists[$v['days']][$count_time] = 1;
				}
			}else{
				$lists[$v['days']][$count_time] = 1;
			}
		}
		
		$this->online_export($lists); */
		
		//剧分类导出
		/* $sql = "select category from IMStory where selfstory=0";
		$list = $this->db->query($sql)->result_array();
		$lists = array();
		foreach($list as $k=>$v){
			//echo $v['category'].',';
			if($v['category']){
				$category = explode(',',$v['category']);
				foreach ($category as $kk=>$vv){
					if(isset($lists[$vv])){
						$lists[$vv]  = $lists[$vv]+1;
					}else{
						$lists[$vv] = 0;
					}
				}
			}
		}
		
		
		$sql = "select * from IMStoryCategory";
		$list = $this->db->query($sql)->result_array();
		foreach($list as $kc=>$vc){
			$list[$kc]['num'] = $lists[$vc['id']];
		}
		
		$this->online_export($list); */
		
	}
	
	/* public function online_export($list){
		header("Content-type: text/html; charset=utf-8");
		include_once APPPATH . "libraries/PHPExcel/IOFactory.php";
	
		//创建一个excel对象
		$objPHPExcel = new PHPExcel();
		// Set properties
	
		$objPHPExcel->getProperties()->setCreator("ctos")
		->setLastModifiedBy("ctos")
		->setTitle("Office 2007 XLSX Test Document")
		->setSubject("Office 2007 XLSX Test Document")
		->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
		->setKeywords("office 2007 openxml php")
		->setCategory("Test result file");
	
		//set width
		//$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
		
		//set font size bold
		$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
		$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFont()->setBold(true);
	
		$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	
		//$objPHPExcel->getActiveSheet()->mergeCells('A1:J1');
	
		// set table header content
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A1', '编号')
		->setCellValue('B1', '标签')
		->setCellValue('C1', '数量');
	
	
		// Miscellaneous glyphs, UTF-8
		foreach ($list as $i=>$l){
			$objPHPExcel->getActiveSheet(0)->setCellValue('A' . ($i + 2), $l['id']);
			$objPHPExcel->getActiveSheet(0)->setCellValue('B' . ($i + 2), $l['name']);
			$objPHPExcel->getActiveSheet(0)->setCellValue('C' . ($i + 2), $l['num']);
		}
	
		// Rename sheet
		$objPHPExcel->getActiveSheet()->setTitle('剧标签');
	
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
	
		// Redirect output to a client’s web browser (Excel5)
		ob_end_clean();//清除缓冲区,避免乱码
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="数据统计11.19.xls"');
		header('Cache-Control: max-age=0');
	
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	} */

	public function index()
	{
		$this->config->site_url();
		$this->load->view('base/header');
		$this->load->view('base/user');
		$this->load->view('base/footer');
	}

	public function all()
	{
		$perpage = 10000;
		$departs = $this->depart_model->getList(array('status'=>0), '*', 0, $perpage);
		$_departs = array();
		foreach ($departs as $key => $value) {
			$_departs[$value['id']] = $value;
		}
		
		//分页
		$start = $this->input->get('start');
		if(!$start){
			$start =  0;
		}
		$perpage = 10;
		
		//搜索ID
		$keyID = intval($_GET['keyID']);
		$param['status'] = 0;
		if($keyID){
			$param['id'] = $keyID;
		}
		
		//搜索昵称
		$like = array();
		$keynick = trim($_GET['keynick']);
		if($keynick){
			$like = array('nick'=>$keynick);
		}
		
		//搜索性别
		$keysex = intval($_GET['keysex']);
		if($keysex){
			$param['sex'] = $keysex-1;
		}
		
		$users = $this->user_model->getList($param, '*', $start*$perpage, $perpage,'id','asc',$like);
		foreach ($users as $key => $value) {
			if($value['sex'] == 0){
				$users[$key]['sex'] = '女';
			}else{
				$users[$key]['sex'] = '男';
			}
			if(isset($_departs[$value['departId']])){
				$users[$key]['depart_value'] = $_departs[$value['departId']]['departName'];
			}else{
				$users[$key]['depart_value'] = '数据错误';
			}
			if($users[$key]['avatar']){
				$users[$key]['avatar_value'] = $users[$key]['avatar'];
			}
			if($users[$key]['created']){
				$users[$key]['created'] = date('Y-m-d',$users[$key]['created']);
			}
		}
		$count = $this->user_model->getCount($param,$like);
		$result = array(
			'users'=>$users,
			'page'=>$start,
			'count'=>ceil($count/10),
			'departs'=>$_departs
		);
		echo json_encode($result);
	}

	/**
	 * 删除用户
	 * @param int id 用户id
	 * @param bool delmsg 是否删除剧信息
	 */
	public function del()
	{
		//接收用户id
		$user_id = $this->input->post('id');
		$delmsg = $this->input->post('delmsg');
		
		//是否删除群信息
		$gmsg = $groupmsg = array();
		if($delmsg){
			//修改剧or自戏非法信息
			$i=0;
			for ($i;$i<8;$i++){
				$q = $this->db->select('id,groupId,content')->where(array('userId'=>$user_id))->get('IMGroupMessage_'.$i);
				$gmsg = $q->result_array();
				
				//数据追加
				//$lists=array_merge_recursive($lists,$gmsg);
				if($gmsg){
					//整理添加数据
					foreach($gmsg as $m){
						$m['tableSuffix'] = $i;
						$m['gmId'] = $m['id'];
						unset($m['id']);
						$m['created'] = time();
						$m['userId'] = $user_id;
						 $groupmsg[] =$m;
					}
					
					//数据备份
					if($groupmsg){
						$this->load->database('db_im',true);
						$this->db->insert_batch('IMGroupMessage',$groupmsg);
						$groupmsg = array();
						
						//替换内容
						$this->load->database ( 'default', true );
						$this->db->where('userId', $user_id);
						//此段内容涉及哲♂学已被删除~
						$this->db->update('IMGroupMessage_'.$i, array('content'=>'NNauMaJPCyrSqa2+oA0X9BqbYzml8sYDwN7wXH7C4+4ZldcVFN7Pz7PmnCSHSnae'));
					}
				}
			}
			
			//删除新的评论信息
			$q = $this->db->select('id,parentid')->where(array('creator'=>$user_id,'status'=>'0','parentid'=>'0'))->get('CommentTopicGrade');
			$new_list = $q->result_array();
			if($new_list){
				//获取父级id
				$parentids = '';
				foreach ($new_list as $nl){
					$parentids[] = $nl['id'];
				}
				
				//删除用户评论信息
				$this->db->where('creator', $user_id);
				$this->db->where('status', '0');
				$this->db->update('CommentTopicGrade',array('status'=>'1'));
				
				if($parentids){
					//删除一级下所有二级
					$this->db->where_in('parentid', $parentids);
					$this->db->update('CommentTopicGrade',array('status'=>'1'));
				}
			}
		}
		
		$result = $this->user_model->update(array('status'=>1), $user_id);
		if($result){
			echo 'success';
		}
	}
	
	
	/**
	 * 查看剧信息
	 */
	public function viewMessage(){
		//接收用户id
		$user_id = $this->input->post('id');
		
		//查询管理的剧or自戏信息----从评论群里获取
		$gmsg = $list = array();
		$i=$n=0;
		for ($i;$i<8;$i++){
			$q = $this->db->select('groupId,content')->where(array('userId'=>$user_id))->get('IMGroupMessage_'.$i);
			$gmsg = $q->result_array();
			//循环数据
			foreach($gmsg as $l){
				//内容转换
				$dmsg = $this->aesLocalDecode($l['content']);
					
				//正则替换图片
				$dmsg = str_replace('&$#@~^@[{:','<img src="',$dmsg);
				$dmsg = str_replace(':}]&$~@#@','">',$dmsg);
				$l['content'] = preg_replace("/<img([^>]*)\s*src=('|\")([^'\"]+)('|\"\>)/",'',$dmsg);
					
				//数组赋值
				$list[] = $l;
				$n++;
				//获取前20条记录
				if($n>5){
					break;
				}
			}
			if($n>5){
				break;
			}
		}
		
		//2015.11.13评论里获取
		$q = $this->db->select('intro  as content')->where(array('creator'=>$user_id))->limit('5')->get('CommentTopicGrade');
		$new_list = $q->result_array();
		foreach ($new_list as $nl){
			$list[] = $nl;
		}
		
		echo json_encode(array('msgLists'=>$list,'user_id'=>$user_id));
	}
	
	/**
	 * 转码
	 * @param 要转换的数据 string $emsg
	 * @return string
	 */
	private function aesLocalDecode($emsg)
	{
		$command = './decode ' . EscapeShellArg($emsg);
		//$value = passthru($command);
		$value = `$command`;
		return $value ; //$return;
	
	}
	
	/**
	 * 签约写手
	 */
	public function issigned(){
		//读取数据
		$id = $this->input->post('id');
		$issigned = $this->input->post('issigned');
		$flag = $this->input->post('flag');
		
		//$issigned_val = ($issigned=='1')?'0':'1';
		
		//获取用户信息
		$user = $this->user_model->getOne(array('id'=>$id));
		if((empty($user) && count($user)<=0)){
			echo 'no';
		}
		
		$signed = 0;
		$v = 0;
		
		if($user['issigned']==3){
			$signed = 1;
			$v = 2;
		}elseif($user['issigned']==1){
			$signed = 1;
		}elseif($user['issigned']==2){
			$v = 2;
		}
		
		//签约
		if($flag == '1'){
			if($user['issigned']=='0' || $user['issigned']=='2' ){
				$signed = 1;
			}else{
				$signed = 0;
			}
		}else{
			if($user['issigned']=='0' || $user['issigned']=='1'){
				$v = 2;
			}else{
				$v = 0;
			}
		}
		
		$issigned_val = $v + $signed;
		
		//头像不存在
		if($user['avatar']){
			if(preg_match('/\?issigned=/', $user['avatar'])){
				$avatar_value = substr_replace($user['avatar'], '', strlen($user['avatar'])-11,strlen($user['avatar']));
			}else{
				$avatar_value = $user['avatar'];
			}
			
			$avatar_value = $avatar_value.'?issigned='.$issigned_val;

		}else{
			$avatar_value = 'http://cosimage.b0.upaiyun.com/yuxi/default_head.png?issigned='.$issigned_val;
		}
		
		
		//更新数据
		$params = array (
				'issigned' => $issigned_val,
				'avatar' => $avatar_value
		);
		
		$result = $this->user_model->update($params,$id);
		if($result){
			echo json_encode(array('status'=>'success','issigned'=>$issigned_val));
		}
	}

	public function add()
	{
		$salt = rand()%10000;
		$params = array(
			'sex'=>$this->input->post('sex'),
			'name'=>$this->input->post('name'),
			'domain'=>$this->input->post('domain'),
			'nick'=>$this->input->post('nick'),
			'password'=>md5(md5($this->input->post('password')).$salt),
			'salt'=>$salt,
			'phone'=>$this->input->post('phone'),
			'email'=>$this->input->post('email'),
			'avatar'=>$this->input->post('avatar'),
			'departId'=>$this->input->post('departId'),
			'status'=>$this->input->post('status'),
			'created'=>time(),
			'updated'=>time()
		);
		$result = $this->user_model->insert($params);
		if($result){
			echo 'success';
		}
	}

	public function edit()
	{
		$params = array(
			'sex'=>$this->input->post('sex'),
			'name'=>$this->input->post('name'),
			'domain'=>$this->input->post('domain'),
			'nick'=>$this->input->post('nick'),
			'phone'=>$this->input->post('phone'),
			'email'=>$this->input->post('email'),
			'avatar'=>$this->input->post('avatar'),//$this->config->config['msfs_url'].$this->input->post('avatar'),
			'departId'=>$this->input->post('departId'),
			'status'=>$this->input->post('status'),
			'updated'=>time()
		);
		$id = $this->input->post('id');
		$result = $this->user_model->getOne(array('id'=>$id));
		$pwd = $this->input->post('pwd');
		if($pwd){
			$params['pwd'] = md5(md5($this->input->post('password')).$result["salt"]);
		}
		$result = $this->user_model->update($params,$id);
		if($result){
			echo 'success';
		}
	}

	public function get()
	{
		$id = $this->input->post('id');
		$result = $this->user_model->getOne(array('id'=>$id));
		if($result){
			echo json_encode($result);
		}
	}

	public function upload()
	{
		include_once APPPATH."libraries/image_moo.php";

		try{
		    $filename=$this->input->get('filename');
		    $ext = pathinfo($filename, PATHINFO_EXTENSION);
		    $filename = time().".".$ext;
		    $input = file_get_contents("php://input");
		    file_put_contents('./download/'.$filename, $input);
		    // $image = new Image_moo();
		    // $image
		    // 	->load('./download/'.$filename)
		    // 	->resize_crop(100,100)
		    // 	->save('./download/1.jpg');
		    // 裁剪头像
		    // $targ_w = $targ_h = 100;
		    // $jpeg_quality = 90;

		    // $img_r = imagecreatefromjpeg('./download/'.$filename);
		    // $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

		    // imagecopyresampled($dst_r,$img_r,0,0,0,0,
		    //     $targ_w,$targ_h,100,100);

		    // header('Content-type: image/jpeg');
		    // imagejpeg($dst_r, './download/'.$filename, $jpeg_quality);



		    $res = $this->_upload('./download/'.$filename);
		    if($res['error_code'] == 0){	    	
			    $array = array(
			    	'status' =>'success',
			    	'file' =>$res['path'],
			    	'real_path'=>$this->config->config['msfs_url'].$res['path']
			    );
		    }else{
		    	$array = array(
			    	'status' =>'fail',
			    	'file' =>'',
			    	'real_path'=>''
			    );
		    }
			echo json_encode($array);
		}
		catch(Exception $e)
		{
			$array = array(
				'status' =>'fail',
				'file' =>0
			);
			echo json_encode($array);
		}
	}

	public function _upload($filename)
	{
		$ch = curl_init();
		$data = array('filename'=>'@'.$filename);
		curl_setopt($ch,CURLOPT_URL,$this->config->config['msfs_url']);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch,CURLOPT_POST,true);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
		$result = curl_exec($ch);
		curl_close($ch);
		return json_decode($result,1);
	}
	
	/**
	 * 账户导入
	 */
	/* public function import_user() {
		header("Content-type: text/html; charset=utf-8");
		include_once APPPATH . "libraries/PHPExcel/IOFactory.php";
		
		try {
			if (! file_exists ( "user_id.xls" )) {
				exit ( "not found user_id.xls.\n" );
			}
			
			$reader = PHPExcel_IOFactory::createReader ( 'Excel5' ); // 设置以Excel5格式(Excel97-2003工作簿)
			$PHPExcel = $reader->load ( "user_id.xls" ); // 载入excel文件
			$sheet = $PHPExcel->getSheet ( 0 ); // 读取第一個工作表
			$highestRow = $sheet->getHighestRow (); // 取得总行数
			$highestColumm = $sheet->getHighestColumn (); // 取得总列数
			
			//循环读取每个单元格的数据
			$new_user_str = '';
			for($row = 2; $row <= $highestRow; $row ++) { // 行数是以第1行开始
				for($column = 'A'; $column <= $highestColumm; $column ++) { // 列数是以A列开始
					$dataset [] = $sheet->getCell ( $column . $row )->getValue ();
					// echo $column.$row.":".$sheet->getCell($column.$row)->getValue()."<br/>";
					$user_name = $sheet->getCell ( $column . $row )->getValue ();
					
					if($user_name){
						if($row<10){
							$rid = '000'.$row;
						}elseif($row>9 && $row<100){
							$rid = '00'.$row;
						}elseif($row>99 && $row<1000){
							$rid = '0'.$row;
						}
						
						//nick,password,salt,avatar,created,rid
						$new_user [] = array (
								'nick' => $user_name,
								'password' => '94523f9c78dde67e9010576f471b9f62',
								'salt' => '9455',
								'created' =>time(),
								'updated' => time(),
								'rid' =>'mb:1802015'.$rid,
						);
						
						//新用户名
						$new_user_str .= "'".$user_name."',";
					}
				}
			}
			//木枯无,瞿媚己,东篱缪天,刀歌赋,九烛刃,欧阳凌,第五苏格,邵宸语,沙鹭君迁,冽铭君寒,燊珃曦,兮溯姬,祁屿绥,梺宸睿,莫玄雨,轻依,北拓安和,顾妄年,霜迟,伏羲令淮,夜离慕祭,裕白,汤北嵘,梓冉,未明潜渊,千玖君辞,子靥,解语旻,赤锦槿儿,都尉谚倾,御守千熙,暮微,M5,湑弋染,襄瑾婳,梺宸睿,夏祁辰,鬼书子,业初,朝璟涯,久离,闻人且楼,鬼目九冰,共月洛,寻绿,山崎有知,松冈知念,七濑龙井,鬼门舟,江罄歌,碧落麟月,千茗,百谷川,蛊荩臣,萧玹,游释情,白颜,北凛辰|裴清辰,阙烕仇,琛深,荀煜舒,络绎雨,鹿森栗,汐祈岐,东野糖,凉锦舟,妃流璟,独孤茗汐,辛木/木辛朴,七遥,翼韧,陵汜,迟西鸢,青心镜听,贺兰蛰,赤炼橙,穆席槿,岚宇劭,祁延卿,穆席槿,安泽,苏绾冥,南清歌,重桑,芒源清,笙野澈,禁千泽,枫溪彦,伏水,炘炜,宇少槿,长歌不期,顾龙隐,南轩北辚,九卿夜,君满,慕翎辰,赤撩叶,约,鬼山卿凌,皇劫孤妄,重桑,笙野澈,禁千泽,枫溪彦,伏水,炘炜,长歌不期,君满,慕翎辰,赤撩叶,天束风溶,鬼山卿凌,皇劫孤妄,绝天,亦零,断陌渺,帝陨殇,影竹子墨,十二日那岐,高渐离,夙北涵,叶桥/叶复拾,陌桑,芊晴雨,蓝侬,野城妄,晏州浔,倾言忆,季里安,络绎雨,陌冷殇,戏阳陌,虞亦琛,Charles Auditore,杞暮初,寒忆或晞,闫徵寒,韩以拟,北迟,落辰熙,酃墟药若,月见夏岚,辞汜,西泽治良,神官,楚非言,鹿野凉,白九宸,弭言,夏槿,夏槿,周皓初,
			
			//echo $new_user_str;
			//$result = $this->user_model->insert_batch($new_user);
			//echo $this->db->last_query();
			
			$new_user_str = rtrim($new_user_str,',');
			
			$sql = "select id,nick,rid from IMUser where nick in ($new_user_str) ";
			
			echo $sql;
		} catch ( Exception $e ) {
			$array = array (
					'status' => 'fail',
					'file' => 0 
			);
			echo json_encode ( $array );
		}
	} */
	
	/**
	 * 剧内容转换
	 */
	/* public function storyBywordMore(){
		header("Content-type: text/html; charset=utf-8");
		include_once APPPATH . "libraries/PHPExcel/IOFactory.php";
		
		try {
			if (! file_exists ( "word_more.xls" )) {
				exit ( "not found word_more.xls.\n" );
			}
				
			$reader = PHPExcel_IOFactory::createReader ( 'Excel5' ); // 设置以Excel5格式(Excel97-2003工作簿)
			$PHPExcel = $reader->load ( "word_more.xls" ); // 载入excel文件
			$sheet = $PHPExcel->getSheet ( 0 ); // 读取第一個工作表
			$highestRow = $sheet->getHighestRow (); // 取得总行数
			$highestColumm = $sheet->getHighestColumn (); // 取得总列数
				
			//循环读取每个单元格的数据
			$key = '12345678901234567890123456789012';
			for($row = 2; $row <= $highestRow; $row ++) { // 行数是以第1行开始
					
				for($column = 'A'; $column <= $highestColumm; $column ++) { // 列数是以A列开始
					if($row>101 && $row<112){
						$dataset [$column] = $sheet->getCell ( $column . $row )->getValue ();
						if($column=='H'){
							$dataset [++$column] = '';
						}
					}else{
						$dataset [$column] = $sheet->getCell ( $column . $row )->getValue ();
						if($column=='H'){
							$content = $sheet->getCell($column.$row)->getValue();
							if($content){
								$decrypted= mcrypt_decrypt(
										MCRYPT_RIJNDAEL_128,
										$key,
										base64_decode($content),
										MCRYPT_MODE_ECB
								);
								$dataset [++$column] = $decrypted;
							}
						}
					}
				}
				
				$list[] = $dataset;
				unset($dataset);
				
			}

			$this->exportExcel($list);
			
		} catch ( Exception $e ) {
			$array = array (
					'status' => 'fail',
					'file' => 0
			);
			echo json_encode ( $array );
		}
	}
	
	public function exportExcel($list){
		
		//创建一个excel对象
		$objPHPExcel = new PHPExcel();
		// Set properties  
		
		$objPHPExcel->getProperties()->setCreator("ctos")
		        ->setLastModifiedBy("ctos")
		        ->setTitle("Office 2007 XLSX Test Document")
		        ->setSubject("Office 2007 XLSX Test Document")
		        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
		        ->setKeywords("office 2007 openxml php")
		        ->setCategory("Test result file");
		
		//set width  
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(50);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(50);
		
		//设置行高度  
		//$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(22);
		
		//$objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
		
		//set font size bold  
		$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);
		$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFont()->setBold(true);
		
		$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		
		
		//  
		//$objPHPExcel->getActiveSheet()->mergeCells('A1:J1');
		
		// set table header content  
		$objPHPExcel->setActiveSheetIndex(0)
		        ->setCellValue('A1', 'id')
		        ->setCellValue('B1', 'userId')
		        ->setCellValue('C1', 'nick')
		        ->setCellValue('D1', 'storyid')
		        ->setCellValue('E1', 'groupId')
		        ->setCellValue('F1', 'title')
		        ->setCellValue('G1', 'msgId')
		        ->setCellValue('H1', 'content')
		        ->setCellValue('I1', 'content_m');
		
		// Miscellaneous glyphs, UTF-8  
		$count = count($list);
		for ($i = 0; $i < $count; $i++) {
		    $objPHPExcel->getActiveSheet(0)->setCellValue('A' . ($i + 2), $list[$i]['A']);
		    $objPHPExcel->getActiveSheet(0)->setCellValue('B' . ($i + 2), $list[$i]['B']);
		    $objPHPExcel->getActiveSheet(0)->setCellValue('C' . ($i + 2), $list[$i]['C']);
		    $objPHPExcel->getActiveSheet(0)->setCellValue('D' . ($i + 2), $list[$i]['D']);
		    $objPHPExcel->getActiveSheet(0)->setCellValue('E' . ($i + 2), $list[$i]['E']);
		    $objPHPExcel->getActiveSheet(0)->setCellValue('F' . ($i + 2), $list[$i]['F']);
		    $objPHPExcel->getActiveSheet(0)->setCellValue('G' . ($i + 2), $list[$i]['G']);
		    $objPHPExcel->getActiveSheet(0)->setCellValue('H' . ($i + 2), $list[$i]['I']);
		    $objPHPExcel->getActiveSheet(0)->setCellValue('I' . ($i + 2), $list[$i]['H']);
		   // $objPHPExcel->getActiveSheet(0)->setCellValue('J' . ($i + 2), $list[$i]['delivery_memo']);
		    $objPHPExcel->getActiveSheet()->getStyle('A' . ($i + 2) . ':I' . ($i + 2))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		    $objPHPExcel->getActiveSheet()->getStyle('A' . ($i + 2) . ':I' . ($i + 2))->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		    $objPHPExcel->getActiveSheet()->getRowDimension($i + 2)->setRowHeight(16);
		}
		
		
		// Rename sheet  
		$objPHPExcel->getActiveSheet()->setTitle('剧字数过多');
		
		
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet  
		$objPHPExcel->setActiveSheetIndex(0);
		
		
		// Redirect output to a client’s web browser (Excel5)  
		ob_end_clean();//清除缓冲区,避免乱码
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="剧字数过多(' . date('Ymd-His') . ').xls"');
		header('Cache-Control: max-age=0');
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
			
	} */

}