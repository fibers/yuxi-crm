<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('admin_model');
		$this->load->model('permission_model');
		$this->load->model('admin_power_model');
	}

	public function index(){
		$this->login();
	}

	public function login(){
		if(isset($this->session->userdata['account'])){
			redirect('/home');
			exit();
		}
		$submit = $this->input->post('submit');
		$account = $this->input->post('admin');
		$password = $this->input->post('password');
		if($submit){
			$admin = $this->admin_model->getOne(array('uname'=>$account));
			if(md5($password) == $admin['pwd']){
				//更新时间
				$update['updated'] = time();
				$this->admin_model->update($update,$admin['id']);
				
				//获取权限编码
				$rolecode = $this->permission_model->getOne(array('id'=>$admin['permission']));
				//echo '111111111';
				//根据code组织以及导航
				/* if($rolecode['rolecode']==='all'){//echo '222222';
					$power = $this->admin_power_model->navList("",0,1);//echo '3333333';
				}else{
					$power = $this->admin_power_model->navList($rolecode['rolecode'],0,1);
				} */
				//echo 'sdfasf';
				//将所有权限编码，转换为模块
				$power_code = $this->admin_power_model->listBycode($rolecode['rolecode']);
				$str = "";
				foreach ($power_code as $c){
					if ( ! preg_match('#^https?://#i', $c['module_name'])){
						$uri = $c['module_name'];
						if($c['action_name'])
							$uri = $uri.'/'.$c['action_name'];
						$str .= $uri.',';
					}
				}
				
				//设置登陆session
				$session = array(
					'adminId' => $admin['id'],
					'account' => $account,
					'rolecode' => $rolecode['rolecode']
				);
				$this->session->set_userdata($session);
				
				//echo base64_encode(serialize($power));
				//print_r($this->session);
				//print_r($power);
				//设置导航列表
				//$this->session->set_userdata('navList',base64_encode(serialize($power)));
				//$this->session->set_userdata('navList',$base64);
				//设置权限编号
				$this->session->set_userdata('check_power',$str);
				//print_r($this->session->userdata);
				echo "right";
				exit;
			}else{
				echo "wrong" ;
			}
			exit();
		}
		$this->load->view('auth/login');
	}
	
	public function auth_pwd(){
		$this->load->view('base/header');
		$this->load->view('auth/auth_pwd');
		$this->load->view('base/footer');
	}
	
	public function save_pwd(){
		if(!$this->session->userdata['adminId'] && !$this->session->userdata['account']){
			echo "登陆过期，请刷新页面，重新登陆";
			exit;
		}
	
		$yuanpwd = $this->input->post('yuanpwd');
		$newpwd = $this->input->post('newpwd');
		$aginpwd = $this->input->post('aginpwd');
	
		//密码是否一致
		if($newpwd!=$aginpwd || !$newpwd || !$aginpwd){
			echo '两次输入的密码不一致';
			exit;
		}
	
		//验证密码正确与否
		$admin = $this->admin_model->getOne(array('uname'=>$this->session->userdata['account']));
		if(md5($yuanpwd) != $admin['pwd']){
			echo '原密码不正确';
			exit;
		}
	
		//密码修改operatorsId
		$this->admin_model->update(array('pwd'=>md5($newpwd)),$this->session->userdata['adminId']);
		echo "success";
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('/auth/login');
	}
}