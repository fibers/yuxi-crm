<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH."core/TT_Controller.php");

class Online extends TT_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->check_power('online');
		$this->load->helper('url');
		$this->load->model('user_model');
        $this->load->model('onlinecount_model');
        $this->load->model('onlineusers_model');
		$this->load->model('depart_model');
        date_default_timezone_set('Asia/Shanghai');
	}

	public function index()
	{
		$this->config->site_url();
               
               
		$this->load->view('base/header');
		$this->load->view('base/online');
		$this->load->view('base/footer');
	}
	
	/**
	 * 今日在线人数
	 */
	public function getnewuser() {
		//搜索时间
		$start_time = $this->input->get('keystarttime');
		$end_time = $this->input->get('keyendtime');
		//默认当前时间
		if(!$start_time){
			$start_time = date('Y-m-d',time());
		}
	
		//存在开始时间，不存在结束时间时，默认和开始时间一致
		if($start_time && !$end_time){
			$end_time = $start_time;
		}
		//默认当前时间
		if(!$end_time){
			$end_time = date('Y-m-d',time());
		}
	
		$dataunit = $this->input->get('dataunit');
		//时间趋向不存在时
		if(!$dataunit){
			$dataunit = 'daily';
		}
	
		//当开始时间和结束时间为同一天时，时间区间为小时
		if(strtotime($start_time)==strtotime($end_time)){
			$dataunit = 'hourly';
		}
	
		//开始-结束时间差，确定可查看范围
		$dataunitrue = $dataunitfalse = array();
		$this->data_unit($start_time,$end_time,$dataunitrue,$dataunitfalse,$dataunit);
	
		//计算模式计算
		$maxNum = $i_next= '0';
		$res = $labels = $list = array();
		$str_start_time = strtotime($start_time);
		$str_end_time = strtotime($end_time)+3600*24;
		if($dataunit=='hourly'){
			$list = $this->onlineusers_model->getListByHours ( $str_start_time, $str_end_time );
			foreach ($list as $key=>$v){
				$res [] = $v['num'];
				$labels[] = $v['date'];
				if($v['num']>$maxNum){
					$maxNum = $v['num'];
				}
			}
		}elseif($dataunit=='daily'){
			$list = $this->onlineusers_model->getListByDays ( $str_start_time, $str_end_time );
			foreach ($list as $key=>$v){
				$res [] = $v['num'];
				$labels[] = $v['date'];
				if($v['num']>$maxNum){
					$maxNum = $v['num'];
				}
			}
		}elseif($dataunit=='weekly'){
			$list = $this->onlineusers_model->getListByWeeks ( $str_start_time, $str_end_time );
			$countList = count($list)-1;
			foreach ($list as $key=>$v){
				$res [] = $v['num'];
				$dates = explode('-', $v['date']);
				$date_weeks = $this->getWeekDate($dates['0'],$dates['1']);
				if($key==0 && $date_weeks['0']<$start_time){
					$date_weeks['0'] = $start_time;
				}elseif($key==$countList && $date_weeks['1']>$end_time){
					$date_weeks['1'] = $end_time;
				}
				$labels[] = $date_weeks;
				if($v['num']>$maxNum){
					$maxNum = $v['num'];
				}
				$list[$key]['date'] = $date_weeks;
			}
			$res = array_merge(array(0),$res);
			$labels = array_merge(array(0),$labels);
		}elseif($dataunit=='monthly'){
			$list = $this->onlineusers_model->getListByMonths ( $str_start_time, $str_end_time );
			foreach ($list as $key=>$v){
				$res [] = $v['num'];
				$labels[] = $v['date'];
				if($v['num']>$maxNum){
					$maxNum = $v['num'];
				}
			}
			$res = array_merge(array(0),$res);
			$labels = array_merge(array(0),$labels);
		}
	
		if ($res) {
	
			//纵轴每个刻度的宽度
			$scaleStepWidth = ceil($maxNum/8);
				
			echo json_encode ( array (
					'users' => $list,
					'labels' => $labels,
					'data' => $res,
					'dataunit' => $dataunit,
					'dataunitrue' =>$dataunitrue,
					'dataunitfalse' => $dataunitfalse,
					'scaleSteps' => '8',
					'scaleStepWidth' => $scaleStepWidth ? $scaleStepWidth : '1'
			) );
	
		}
	}
	
	/* public function getnewuser() {
		//搜索时间
		$keytime = $this->input->get('keytime');
		if(strtotime($keytime)<time()){
			$hour = '24';
		}else{
			$keytime = 'Y-m-d';
			$hour = date ( 'H' );
			$hour = ($hour < 10) ? (substr ( $hour, - 1 )) : ($hour);
		}
		
		for($i = 0; $i < $hour; $i ++) {
			$h = ($i < 10) ? ('0' . $i) : ($i);
			$dt = date ( $keytime . $h . ':00:00' );
			$timeArr [] = strtotime ( $dt );
		}
		
		for($i = 0; $i < $hour - 1; $i ++) {
			// $tArr[] = array($timeArr[$i],$timeArr[$i+1]);
			$res [] = $this->onlinecount_model->getNewUserBydate ( $timeArr [$i], $timeArr [$i + 1] ); 
			// strtotime(date('2000-01-01  00:00:00')), strtotime(date('Y-m-d  H:i:s'))
		}
		// $res =  [99,66,55,28,48,40,19,96,27,100,28,48];//,40,19,96,27,100,28,48,40,19,0,0,0;
		if ($res) {
			$maxNum = '0';
			$res = array_merge ( array ( 0 ), $res );
			foreach ( $res as $k => $v ) {
				$num = ( int ) $v;
				$res [$k] = $num;
				if($num>$maxNum){
					$maxNum = $num;
				}
			}
			
			//纵轴每个刻度的宽度
			$scaleStepWidth = ceil($maxNum/8);
			
			echo json_encode ( array('data'=>$res,'scaleSteps'=>'8','scaleStepWidth'=>$scaleStepWidth?$scaleStepWidth:'1') );
		}
	} */
        
	public function getDayList() {
		$hour = date ( 'H' );
		$hour = ($hour < 10) ? (substr ( $hour, - 1 )) : ($hour);
		for($i = 0; $i < $hour + 1; $i ++) {
			$h = ($i < 10) ? ('0' . $i) : ($i);
			$list [] = $this->onlinecount_model->getDayList ( strtotime ( date ( 'Y-m-d ' . $h . ':00:00' ) ), strtotime ( date ( 'Y-m-d ' . $h . ':59:59' ) ) );
			;
		}
		
		$result = array (
				'users' => $list,
				'page' => 1 
		);
		echo json_encode ( $result );
	}
	
	
	
	/**
	 * 活跃用户
	 */
	public function active_user(){
		$this->load->view('base/header');
		$this->load->view('base/active_user');
		$this->load->view('base/footer');
	}
    
	/**
	 * 日活跃用户
	 */
	public function activeDayList(){
		//月第一天
		$month_frist = strtotime(date('Y-m'));
		//月当天
		$month_today = strtotime(date('Y-m-d',time()));
		
		//当前多少天
		$day = date('d',$month_today-$month_frist);
		
		for($i = $day; $i >0; $i--) {
			$list [] = $this->onlineusers_model->getList ( strtotime ( date ( 'Y-m-'.$i.' 00:00:00' ) ), strtotime ( date ( 'Y-m-'.$i.' 23:59:59' ) ) );
		}
		
		$result = array (
				'users' => $list
		);
		echo json_encode ( $result );
	}
	
	/**
	 * 周活跃用户
	 */
	public function activeWeekList(){
		
		$datearr = getdate();
		$year = strtotime($datearr['year'].'-1-1');
		$startdate = getdate($year);
		$firstweekday = 7-$startdate['wday'];//获得第一周几天
		$yday = $datearr['yday']+1-$firstweekday;//今年的第几天
		$weeknum =  ceil($yday/7)+1;//取到第几周
		
		$toYear = date('Y');
		for($i = 0; $i <8; $i++) {
			//周开始时间-结束时间
			$week = $this->getWeekDate($toYear, $weeknum-$i);
			$list [] = $this->onlineusers_model->getList ( strtotime ( date ( $week[0].' 00:00:00' ) ), strtotime ( date (  $week[1].' 23:59:59' ) ) );
		}
	
		$result = array (
				'users' => $list
		);
		echo json_encode ( $result );
	}
	
	/**
	 * 月活跃用户
	 */
	public function activeMonthList(){
		
		for($i = 0; $i <6; $i ++) {
			//获取月份数据
			$year = date("Y",strtotime("- $i month"));
			$month = date("m",strtotime("- $i month"));
			$date = $this->mFristAndLast($year,$month);
			$list [] = $this->onlineusers_model->getList ( $date[0], $date[1] );
		}
	
		$result = array (
				'users' => $list
		);
		echo json_encode ( $result );
	}
	
	/**
	 * 日流失用户
	 */
	public function loss_user(){
		$this->load->view('base/header');
		$this->load->view('base/loss_user');
		$this->load->view('base/footer');
	}

	/**
	 * 日流失用户
	 */
	public function lossDayList(){
		//月第一天
		$month_frist = strtotime(date('Y-m'));
		//月当天
		$month_today = strtotime(date('Y-m-d',time()));
	
		//当前多少天
		$day = date('d',$month_today-$month_frist);
	
		for($i = $day; $i >0; $i--) {
			$list [] = $this->users_model->getLossList ( strtotime ( date ( 'Y-m-'.$i.' 00:00:00' ) ), strtotime ( date ( 'Y-m-'.$i.' 23:59:59' ) ) );
		}
	
		$result = array (
				'users' => $list
		);
		echo json_encode ( $result );
	}
	
	/**
	 * 周流失用户
	 */
	public function lossWeekList(){
	
		$datearr = getdate();
		$year = strtotime($datearr['year'].'-1-1');
		$startdate = getdate($year);
		$firstweekday = 7-$startdate['wday'];//获得第一周几天
		$yday = $datearr['yday']+1-$firstweekday;//今年的第几天
		$weeknum =  ceil($yday/7)+1;//取到第几周
	
		$toYear = date('Y');
		for($i = 0; $i <8; $i++) {
			//周开始时间-结束时间
			$week = $this->getWeekDate($toYear, $weeknum-$i);
			$list [] = $this->onlineusers_model->getList ( strtotime ( date ( $week[0].' 00:00:00' ) ), strtotime ( date (  $week[1].' 23:59:59' ) ) );
		}
	
		$result = array (
				'users' => $list
		);
		echo json_encode ( $result );
	}
	
	/**
	 * 月流失用户
	 */
	public function lossMonthList(){
	
		for($i = 0; $i <6; $i ++) {
			//获取月份数据
			$year = date("Y",strtotime("- $i month"));
			$month = date("m",strtotime("- $i month"));
			$date = $this->mFristAndLast($year,$month);
			$list [] = $this->onlineusers_model->getList ( $date[0], $date[1] );
		}
	
		$result = array (
				'users' => $list
		);
		echo json_encode ( $result );
	}
	
	/**
	 * 日存留用户
	 */
	public function keep_user(){
		$this->load->view('base/header');
		$this->load->view('base/keep_user');
		$this->load->view('base/footer');
	}
	
	/**
	 * 日存留用户
	 */
	public function keepDayList(){
		//月第一天
		$month_frist = strtotime(date('Y-m'));
		//月当天
		$month_today = strtotime(date('Y-m-d',time()));
	
		//当前多少天
		$day = date('d',$month_today-$month_frist);
	
		for($i = $day; $i >0; $i--) {
			$list [] = $this->users_model->getLossList ( strtotime ( date ( 'Y-m-'.$i.' 00:00:00' ) ), strtotime ( date ( 'Y-m-'.$i.' 23:59:59' ) ) );
		}
	
		$result = array (
				'users' => $list
		);
		echo json_encode ( $result );
	}
	
	/**
	 * 周存留用户
	 */
	public function keepWeekList(){
	
		$datearr = getdate();
		$year = strtotime($datearr['year'].'-1-1');
		$startdate = getdate($year);
		$firstweekday = 7-$startdate['wday'];//获得第一周几天
		$yday = $datearr['yday']+1-$firstweekday;//今年的第几天
		$weeknum =  ceil($yday/7)+1;//取到第几周
	
		$toYear = date('Y');
		for($i = 0; $i <8; $i++) {
			//周开始时间-结束时间
			$week = $this->getWeekDate($toYear, $weeknum-$i);
			$list [] = $this->onlineusers_model->getList ( strtotime ( date ( $week[0].' 00:00:00' ) ), strtotime ( date (  $week[1].' 23:59:59' ) ) );
		}
	
		$result = array (
				'users' => $list
		);
		echo json_encode ( $result );
	}
	
	/**
	 * 月存留用户
	 */
	public function keepMonthList(){
	
		for($i = 0; $i <6; $i ++) {
			//获取月份数据
			$year = date("Y",strtotime("- $i month"));
			$month = date("m",strtotime("- $i month"));
			$date = $this->mFristAndLast($year,$month);
			$list [] = $this->onlineusers_model->getList ( $date[0], $date[1] );
		}
	
		$result = array (
				'users' => $list
		);
		echo json_encode ( $result );
	}
	
	public function all()
	{
		$perpage = 10000;
		$departs = $this->depart_model->getList(array('status'=>0), '*', 0, $perpage);
		$_departs = array();
		foreach ($departs as $key => $value) {
			$_departs[$value['id']] = $value;
		}
		$start = $this->input->get('start');
		if(!$start){
			$start =  0;
		}
		$perpage = 10;
		$users = $this->user_model->getList(array('status'=>0), '*', $start*$perpage, $perpage);
		foreach ($users as $key => $value) {
			if($value['sex'] == 0){
				$users[$key]['sex'] = '女';
			}else{
				$users[$key]['sex'] = '男';
			}
			if(isset($_departs[$value['departId']])){
				$users[$key]['depart_value'] = $_departs[$value['departId']]['departName'];
			}else{
				$users[$key]['depart_value'] = '数据错误';
			}
			if($users[$key]['avatar']){
				$users[$key]['avatar_value'] = $users[$key]['avatar'];
			}
		}
		$count = $this->user_model->getCount(array('status'=>0));
		$result = array(
			'users'=>$users,
			'page'=>$start,
			'count'=>ceil($count/10),
			'departs'=>$_departs
		);
		echo json_encode($result);
	}

	public function del()
	{
		$id = $this->input->post('id');
		$result = $this->user_model->update(array('status'=>1), $id);
		if($result){
			echo 'success';
		}
	}

	public function add()
	{
		$salt = rand()%10000;
		$params = array(
			'sex'=>$this->input->post('sex'),
			'name'=>$this->input->post('name'),
			'domain'=>$this->input->post('domain'),
			'nick'=>$this->input->post('nick'),
			'password'=>md5(md5($this->input->post('password')).$salt),
			'salt'=>$salt,
			'phone'=>$this->input->post('phone'),
			'email'=>$this->input->post('email'),
			'avatar'=>$this->input->post('avatar'),
			'departId'=>$this->input->post('departId'),
			'status'=>$this->input->post('status'),
			'created'=>time(),
			'updated'=>time()
		);
		$result = $this->user_model->insert($params);
		if($result){
			echo 'success';
		}
	}

	public function edit()
	{
		$params = array(
			'sex'=>$this->input->post('sex'),
			'name'=>$this->input->post('name'),
			'domain'=>$this->input->post('domain'),
			'nick'=>$this->input->post('nick'),
			'phone'=>$this->input->post('phone'),
			'email'=>$this->input->post('email'),
			'avatar'=>$this->config->config['msfs_url'].$this->input->post('avatar'),
			'departId'=>$this->input->post('departId'),
			'status'=>$this->input->post('status'),
			'updated'=>time()
		);
		$id = $this->input->post('id');
		$result = $this->user_model->getOne(array('id'=>$id));
		$pwd = $this->input->post('pwd');
		if($pwd){
			$params['pwd'] = md5(md5($this->input->post('password')).$result["salt"]);
		}
		$result = $this->user_model->update($params,$id);
		if($result){
			echo 'success';
		}
	}

	public function get()
	{
		$id = $this->input->post('id');
		$result = $this->user_model->getOne(array('id'=>$id));
		if($result){
			echo json_encode($result);
		}
	}

	public function upload()
	{
		include_once APPPATH."libraries/image_moo.php";

		try{
		    $filename=$this->input->get('filename');
		    $ext = pathinfo($filename, PATHINFO_EXTENSION);
		    $filename = time().".".$ext;
		    $input = file_get_contents("php://input");
		    file_put_contents('./download/'.$filename, $input);
		    // $image = new Image_moo();
		    // $image
		    // 	->load('./download/'.$filename)
		    // 	->resize_crop(100,100)
		    // 	->save('./download/1.jpg');
		    // 裁剪头像
		    // $targ_w = $targ_h = 100;
		    // $jpeg_quality = 90;

		    // $img_r = imagecreatefromjpeg('./download/'.$filename);
		    // $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

		    // imagecopyresampled($dst_r,$img_r,0,0,0,0,
		    //     $targ_w,$targ_h,100,100);

		    // header('Content-type: image/jpeg');
		    // imagejpeg($dst_r, './download/'.$filename, $jpeg_quality);



		    $res = $this->_upload('./download/'.$filename);
		    if($res['error_code'] == 0){	    	
			    $array = array(
			    	'status' =>'success',
			    	'file' =>$res['path'],
			    	'real_path'=>$this->config->config['msfs_url'].$res['path']
			    );
		    }else{
		    	$array = array(
			    	'status' =>'fail',
			    	'file' =>'',
			    	'real_path'=>''
			    );
		    }
			echo json_encode($array);
		}
		catch(Exception $e)
		{
			$array = array(
				'status' =>'fail',
				'file' =>0
			);
			echo json_encode($array);
		}
	}

	public function _upload($filename)
	{
		$ch = curl_init();
		$data = array('filename'=>'@'.$filename);
		curl_setopt($ch,CURLOPT_URL,$this->config->config['msfs_url']);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch,CURLOPT_POST,true);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
		$result = curl_exec($ch);
		curl_close($ch);
		return json_decode($result,1);
	}

}