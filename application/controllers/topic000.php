<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH."core/TT_Controller.php");

class Topic extends TT_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('group_model');
		$this->load->model('user_model');
		$this->load->model('grouprelation_model');
	}

	public function index()
	{
		$this->load->view('base/header');
		$this->load->view('base/topic');
		$this->load->view('base/footer');
	}

	public function all()
	{
                
                $start = $this->input->get('start');
		if(!$start){
			$start =  0;
		}
		$perpage = 10;
                $offset =  $start*$perpage;
                $count = $this->group_model->getCount(array('status'=>0,'isTopic'=>1));
		//$groups = $this->group_model->getList(array('status'=>0,'isTopic'=>1), '*',$offset, $perpage);
                
                $this->db->select('IMGroup.* , IMUser.nick');
                $this->db->from('IMGroup');
                $this->db->where(array('IMGroup.status'=>0,'IMGroup.isTopic'=>1));
                $this->db->limit($perpage,$offset);
                $this->db->join('IMUser', 'IMUser.id = IMGroup.creator','left');
                
                $query = $this->db->get();
                $groups = $query->result_array();
//		$data = array();
//		foreach ($groups as $key => $value) {
//			if($groups[$key]['avatar']){
//				$groups[$key]['avatar_value'] = $this->config->config['msfs_url'].$groups[$key]['avatar'];
//			}
//		}
		$result = array(
			'groups'=>$groups,
			'page'=>$start,
			'count'=>ceil($count/10),
		);
		echo json_encode($result);
	}
        
        public function search(){
            $kwd = $_GET['kwd'];
            $groups = $this->group_model->getLike(array('name'=>$kwd),array('status'=>0,'isTopic'=>1), '*', 0, 100);
            $status = (!empty($groups) && count($groups)>0)?('ok'):('empty');
            $result = array(
			'groups'=>$groups,
			'status'=>$status,
		);
            echo json_encode($result);
        }

	public function del()
	{
		$id = $this->input->post('id');
		$result = $this->group_model->updateByWhere(array('status'=>1), 'id', $id);
		if($result){
			echo 'success';
		}
	}
        
        
        public function settop()
	{
		$id = $this->input->post('id');
                $re = $this->group_model->getQuery("select count(*) as num from IMGroup where status = 0 and isTopic=1 and priority >0 limit 5 ");
                $num = $re['0']['num'];
                $flag = 100;
                if($num > 0){
                    if($num>2){
                        echo 'full';exit;
                    }
                    $rankNum = ($num == 1)?($flag+1):($flag+2);
                }else{
                    $rankNum  = $flag;
                }
		$result = $this->group_model->updateByWhere(array('priority'=>$rankNum), 'id', $id);
		if($result){
			echo 'success';
		}
	}
        
        
        public function unsettop()
	{
		$id = $this->input->post('id');
		$result = $this->group_model->updateByWhere(array('priority'=>0), 'id', $id);
		if($result){
			echo 'success';
		}
	}

	public function add()
	{
		$array = array(
			'req_user_id' 	=> 0,
			'app_key'		=> 'asdfasdf',
			'group_name'	=> $this->input->post('name'),
			'group_type'	=> 1,
			'group_avatar'	=> '',
			'user_id_list'	=> array(1)
		);
		$res = $this->httpRequest($this->config->config['http_url'].'/query/CreateGroup','post',json_encode($array));
		$res = json_decode($res,1);
		if($res['error_code'] == 0){
			echo 'success';
		}else{
			echo 'fail';
		}
	}

	public function edit()
	{
		$params = array(
			'priority'=>$this->input->post('priority')
		);
		$id = $this->input->post('id');
		$result = $this->group_model->updateByWhere($params, 'id', $id);
		if($result){
			echo 'success';
		}
	}

	public function get()
	{
		$id = $this->input->post('id');
		$result = $this->group_model->getOne(array('id'=>$id));
		if($result){
			echo json_encode($result);
		}
	}

	public function getMember()
	{
		$id = $this->input->post('id');
		$perpage = 10000;
		$users = $this->grouprelation_model->getList(array('status'=>0,'groupId'=>$id), '*', 0, $perpage);
		foreach ($users as $key => $value) {
			$_data = $this->user_model->getOne(array('id'=>$value['userId']));
			$users[$key]['name'] = $_data['name'];
		}
		$data = array();
		$result = array(
			'users'=>$users,
		);
		echo json_encode($result);
	}

	public function changeMember()
	{
		$add = array(
			'req_user_id'   => 0,
			'app_key'       => 'asdfasdf',
			'group_id'      => intval($this->input->post('id')),
			'modify_type'   => intval($this->input->post('change')),
			'user_id_list'  => array(intval($this->input->post('userId')))
		);                  
	    $res = $this->httpRequest($this->config->config['http_url'].'/query/ChangeMembers','post',json_encode($add));
	}

	public function httpRequest($url,$method,$params=array()){
		$curl=curl_init();
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($curl,CURLOPT_HEADER,0 ) ;
		curl_setopt($curl,CURLOPT_URL,$url);
		curl_setopt($curl,CURLOPT_POST,1 );
		curl_setopt($curl, CURLOPT_POSTFIELDS,$params);
		$result=curl_exec($curl);
		curl_close($curl);
		return $result;
	}
        
        

}