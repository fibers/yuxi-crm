<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once(APPPATH."core/TT_Controller.php");

class Kibana extends TT_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->check_power('kibana');
	}

	/**
	 * 用户关联信息
	 */
	public function index()
	{
		$this->config->site_url();    
		$this->load->view('base/header');
		$this->load->view('base/kibana_user');
		$this->load->view('base/footer');
	}
	
	/**
	 * 新增剧信息
	 */
	public function newStory()
	{
		$this->check_power('kibana/newStory');
		$this->config->site_url();
		$this->load->view('base/header');
		$this->load->view('base/kibana_story');
		$this->load->view('base/footer');
	}
	
	/**
	 * 新增自戏信息
	 */
	public function newSelf()
	{
		$this->check_power('kibana/newSelf');
		$this->config->site_url();
		$this->load->view('base/header');
		$this->load->view('base/kibana_self');
		$this->load->view('base/footer');
	}
	
	/**
	 * 新增家族统计
	 */
	public function newFamilies(){
		$this->check_power('kibana/newFamilies');
		$this->config->site_url();
		$this->load->view('base/header');
		$this->load->view('base/kibana_families');
		$this->load->view('base/footer');
	}
	
}