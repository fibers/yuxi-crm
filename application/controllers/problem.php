<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH."core/TT_Controller.php");

class Problem extends TT_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->check_power('problem');
		$this->load->helper('url');
		$this->load->model('custom_problem_model');
	}

	public function index()
	{
		$this->config->site_url();
		$this->load->view('base/header');
		$this->load->view('base/problem');
		$this->load->view('base/footer');
	}

	public function all()
	{
		//分页
		$start = $this->input->get('start');
		if(!$start){
			$start =  0;
		}
		$perpage = 10;
		
		//搜索是否解除签约
		$is_deal = intval($_GET['keyisdeal']);
		$param = array();
		if($is_deal){
			$param['is_deal'] = $is_deal-1;
		}
		
		//获取用户反馈数据
		$startPage = $start*$perpage;
		$problem = $this->custom_problem_model->getList($param, 'id,content,qqno,uid,is_deal', $startPage, $perpage,'id','desc');

		//获取用户反馈数据总数
		$count = $this->custom_problem_model->getCount($param);
		
		$result = array(
			'problem'=>$problem,
			'page'=>$start,
			'count'=>ceil($count/$perpage),
		);
		echo json_encode($result);
	}
	
	/**
	 * 处理用户反馈信息
	 */
	public function is_deal(){
		$id = $this->input->post('id');
		if($id){
			$this->custom_problem_model->update(array('is_deal'=>'1'),$id);
			echo "success";
			exit;
		}
		echo "error";
	}
	
	/**
	 * 检查非法字
	 */
	public function shield(){
		$this->load->view('base/header');
		$this->load->view('base/shield');
		$this->load->view('base/footer');
	}
	
	public function shield_word(){
		$type = (int)$this->input->get('type');
		$content = $this->input->get('content');
		//1内容，2介绍
		if($type==1){
			$sql = "select id,Shieldword from IMShield_intro";
		}else{
			$sql = "select id,Shieldword from IMShield_title";
		}
		
		$list = $this->custom_problem_model->getQuery($sql);
		foreach($list as $v){
			$num = substr_count($content,$v['Shieldword']);
			if($num > 0){
				$content = str_replace($v['Shieldword'],'<span style="color:red">'.$v['Shieldword'].'</span>',$content);
			}
		}
		
		echo json_encode(array('new_content'=>$content)); 
	}
	
}