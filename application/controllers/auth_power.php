<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once(APPPATH."core/TT_Controller.php");
class Auth_power extends TT_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('admin_power_model');
		$this->load->model('permission_model');
	}

	//权限页面
	public function index(){
		$this->load->view('base/header');
		$this->load->view('auth/power');
		$this->load->view('base/footer');
	}
	
	/**
	 * 获取权限列表
	 */
	public function all(){
		//分页
		$start = $this->input->get('start');
		if(!$start){
			$start =  0;
		}
		$perpage = 10;
		
		//搜索昵称
		$param = array();
		$keyrolename = trim($_GET['keyrolename']);
		if($keyrolename){
			$param = array('rolename'=>$keyrolename);
		}
		
		$permissions = $this->permission_model->getList($param, '*', $start*$perpage, $perpage,'id','asc');
		
		//统计个数
		$count = $this->permission_model->getCount($param);
		
		$result = array(
				'permissions' => $permissions,
				'page'=>$start,
				'count'=>ceil($count/10),
		);
		echo json_encode($result);
	}
	
	public function get()
	{
		$id = $this->input->post('id');
		$result = $this->permission_model->getOne(array('id'=>$id));
		if($result){
			echo json_encode($result);
		}
	}
	
	/**
	 * 修改
	 */
	public function edit()
	{
		$params = array('rolename'=>$this->input->post('rolename')	);
		$id = $this->input->post('id');
	
		//数据修改
		$result = $this->permission_model->update($params,$id);
		if($result){
			echo 'success';
		}
	}
	
	/**
	 * 添加
	 */
	public function add()
	{
		$params = array('rolename'=>$this->input->post('rolename'));
		$result = $this->permission_model->insert($params);
		if($result){
			echo 'success';
		}
	}
	
	//修改权限
	public function add_power(){
		
		$id = $this->input->get('id');
		$data['permission'] = $this->permission_model->getOne(array('id'=>$id));
		
		//获取权限列表
		$data['plist'] = $this->admin_power_model->allList();
		
		//print_r($data);
		$this->load->view('base/header');
		$this->load->view('auth/add_power',$data);
		$this->load->view('base/footer');
	}
	
	/**
	 * 权限保存
	 */
	public function save_power(){
		$id = $this->input->post('hid');
		$power = $this->input->post('power');
		
		if(!$power){
			$this->prompt_alert('不允许设置空权限');
		}
		
		//权限值
		$rolecode = implode($power, ',');
		
		//数据修改
		$this->permission_model->update(array('rolecode'=>$rolecode),$id);
		
		$this->prompt_msg("保存成功", "/auth_power");
	}
}