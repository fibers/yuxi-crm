<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH."core/TT_Controller.php");

class Story extends TT_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('story_model');
		$this->load->model('storycategory_model');
	}

	public function index()
	{
		$this->load->view('base/header');
		$this->load->view('base/story');
		$this->load->view('base/footer');
		
	}

	public function all()
	{
		
		$perpage = 10000;
		$start = $this->input->get('start');
		$title = $this->input->get('title');
		if(!$start){
			$start =  0;
		}
		$like=array();
		$str_like='';
		if($title){
			$like=array('title'=>$title);
			$str_like=" and title like '%".$title."%' ";
		}
		$perpage = 10;
		$storyList = $this->story_model->getList(array('storystatus'=>0), '*', $start*$perpage, $perpage,'id','desc',$like);
		if(count($storyList)>0){
			foreach($storyList as $key=>$list){
				$category=array();
				$category=$this->storycategory_model->getOne(array('id'=>$list['category']));
				$storyList[$key]['category_name']='';
				if($category){
					$storyList[$key]['category_name']=$category['name'];
				}
			}
		}
		
		//获取剧总数
		
		$count = $this->story_model->getQuery('select count(id) as nums from IMStory where storystatus !=1 '.$str_like);
		$result = array(
			'title'=>$title,
			'story'=>$storyList,
			'page'=>$start,
			'count'=>@ceil((int)$count[0]['nums']/$perpage)
		);
		
		echo json_encode($result);
	}

	public function del()
	{
		$id = $this->input->post('id');
		$result = $this->story_model->updateByWhere(array('storystatus'=>1), 'id', $id);
		if($result){
			echo 'success';
		}
	}

	
	public function top()
	{
		$id = $this->input->post('id');
		$rel= $this->input->post('rel');
                $flag = 100;
		if(!$rel){//推荐
			$count = $this->story_model->getQuery('select count(id) as nums from IMStory where priority >0');
			if($count[0]['nums']>=3){
				echo 'overtop';
				exit;
			}else{
				if($count[0]['nums']<1){
					$priority=$flag;
				}else{
					$priority=$flag+$count[0]['nums'];
				}
			}
		}else{
			$priority=0;
		}
		
		$result = $this->story_model->updateByWhere(array('priority'=>$priority), 'id', $id);
		if($result){
			echo 'success';
		}
		exit;
	}
        
        public function edit()
	{
		$params = array(
			'priority'=>$this->input->post('priority')
		);
		$id = $this->input->post('id');
		$result = $this->story_model->update($params,$id);
		if($result){
			echo 'success';
		}
	}
        
        public function get()
	{
		$id = $this->input->post('id');
		$result = $this->story_model->getOne(array('id'=>$id));
		if($result){
			echo json_encode($result);
		}
	}
}