<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH."core/TT_Controller.php");

class qr_code extends TT_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('qrcode_model');
		//数据库连接
		$this->load->database('db_im',true);
		//error_reporting(0);
	}

	public function index()
	{
		$this->config->site_url();
		$this->load->view('base/header');
		$this->load->view('base/qrcode');
		$this->load->view('base/footer');
	}

	public function all()
	{
		//分页
		$start = $this->input->get('start');
		if(!$start){
			$start =  0;
		}
		$perpage = 10;
		
		//搜索关键词
		$param = array();
		$keywords = trim($_GET['keywords']);
		if($keywords){
			$param = array('keywords'=>$keywords);
		}
		
		$qrcode = $this->qrcode_model->getList($param, '*', $start*$perpage, $perpage,'id','desc');
		
		//获取二维码扫描次数
		foreach($qrcode as $k=>$v){
			$sql = "select count(id) as nums from downVisit where url='".$v['keywords']."'";
			
			$row = $this->db->query($sql)->row_array();
			$qrcode[$k]['nums'] = isset($row['nums'])?$row['nums']:'0';
		}
		
		$count = $this->qrcode_model->getCount($param);
		$result = array(
			'qrcode'=>$qrcode,
			'page'=>$start,
			'count'=>ceil($count/$perpage),
		);
		echo json_encode($result);
	}
	
	/**
	 * 添加二维码
	 */
	public function add()
	{
	
		$keywords = $this->input->post ( 'keywords' );
		if(!$keywords){
			echo '关键词不能为空';
			exit;
		}
		
		//判断这个语戏ID是否录入过数据库中
		if($keywords){
			$row = $this->qrcode_model->getOne(array('keywords'=>$keywords,'status'=>'0'));
			if($row){
				echo '该关键词已经使用过';
				exit;
			}
		}
		
		$url = 'http://www.yuxip.com/down.php?from='.trim($keywords);
		$filename = './download/qrcode/'.trim($keywords).'.png';
		$this->phpqrcode($filename, $url);
		
		$params = array (
			'item' => $this->input->post ( 'item' ),
			'form' => $this->input->post ( 'form' ),
			'keywords' => $this->input->post ( 'keywords' ),
			'url' => $url,
			'code' => $filename,
			'created' => time(),
			'updated' => time(),
		);
		
		$result = $this->qrcode_model->insert($params);
		if($result){
			echo 'success';
		}
	}

	
	/**
	 * 
	 * @param string $filename 生成的文件名
	 * @param string $value 二维码数据
	 * @return string
	 */
	public function phpqrcode($filename,$value){
		//引入phpqrcode库文件
		include_once APPPATH."libraries/phpqrcode.php";
		// 纠错级别：L、M、Q、H
		$errorCorrectionLevel = 'H';
		// 点的大小：1到10
		$matrixPointSize = 10;
		
		//生成二维码图片
		QRcode::png($value, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
		$logo = './download/qrcode/logo.png';//准备好的logo图片
		$QR = $filename;//已经生成的原始二维码图
		
		
		if ($logo !== FALSE) {
			$QR = imagecreatefromstring(file_get_contents($QR));
			$QR_width = imagesx($QR);//二维码图片宽度
			$QR_height = imagesy($QR);//二维码图片高度
		
			$corner_png = './download/qrcode/corner.png';
			if($corner_png !== FALSE){
				//圆角图片
				$corner = file_get_contents($corner_png);
				$corner = imagecreatefromstring($corner);
				$corner_width = imagesx($corner);
				$corner_height = imagesy($corner);
		
				//计算圆角图片的宽高及相对于二维码的摆放位置,将圆角图片拷贝到二维码中央
				$corner_qr_height = $corner_qr_width = $QR_width/5;
				$from_width = ($QR_width-$corner_qr_width)/2;
				imagecopyresampled($QR, $corner, $from_width, $from_width, 0, 0, $corner_qr_width, $corner_qr_height, $corner_width, $corner_height);
			}
		
			$logo = imagecreatefromstring(file_get_contents($logo));
			$logo_width = imagesx($logo);//logo图片宽度
			$logo_height = imagesy($logo);//logo图片高度
			$logo_qr_width = $QR_width / 5-6;
			$scale = $logo_width/$logo_qr_width;
			$logo_qr_height = $logo_height/$scale;
			$from_width = ($QR_width - $logo_qr_width) / 2;
			//重新组合图片并调整大小
			imagecopyresampled($QR, $logo, $from_width, $from_width, 0, 0, $logo_qr_width,$logo_qr_height, $logo_width, $logo_height);
		}
		//输出图片
		imagepng($QR, $filename);
		
		//return $filename;
	}
	
	/**
	 * 下载图片
	 */
	public function downjpg(){
		$file = trim($_GET['file']);
		header ("Content-type: octet/stream");
		header ("Content-disposition: attachment; filename=".$file.";");
		header("Content-Length: ".filesize($file));
		readfile($file);
		exit;
	}
	
	/**
	 * 删除
	 */
	public function del()
	{
		$id = $this->input->post('id');
		$result = $this->qrcode_model->delete($id,true);
		if($result){
			echo 'success';
		}
	}
}