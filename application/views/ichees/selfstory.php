<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8">
<meta name = "format-detection" content="telephone = no" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title><?php echo $selfStoryInfo['title'];?>_语戏</title>
<link rel="stylesheet" type="text/css" href="/ichees/css/duixi.css" />
<script type="text/javascript" src="/ichees/js/jquery-1.7.js"></script>
<script type="text/javascript">
/*图片缩放*/
function AutoResizeImage(maxWidth,maxHeight,objImg){
	var img = new Image();
	img.src = objImg.src;
	var hRatio;
	var wRatio;
	var Ratio = 1;
	var w = img.width;
	var h = img.height;
	wRatio = maxWidth / w;
	hRatio = maxHeight / h;
	if (maxWidth ==0 && maxHeight==0){
	Ratio = 1;
	}else if (maxWidth==0){//
	if (hRatio<1) Ratio = hRatio;
	}else if (maxHeight==0){
	if (wRatio<1) Ratio = wRatio;
	}else if (wRatio<1 || hRatio<1){
	Ratio = (wRatio<=hRatio?wRatio:hRatio);
	}
	if (Ratio<1){
	w = w * Ratio;
	h = h * Ratio;
	}
	objImg.height = h;
	objImg.width = w;
}
</script>
</head>
<body>
<div class="pc_width">
	<div class="image_mobile"><img src="<?php echo $selfStoryInfo['portrait'];?>" width="100%"></div>
	<div class="image_pc">
		<div class="image_pc_1"><img src="<?php echo $selfStoryInfo['portrait'];?>" onload="AutoResizeImage(1024,478,this)" ></div>
	</div>
	<div class="head">
		<h1><?php echo $selfStoryInfo['title'];?></h1>
	</div>
	<div class="zixi_Author">作者：<?php echo $selfStoryInfo['creatorname'];?></div>
	<div class="time">
		<div class="time_left"><?php echo date('Y-m-d',$selfStoryInfo['createtime']);?></div>
	    <div class="time_right"><?php echo date('H:i:s',$selfStoryInfo['createtime']);?></div>
	    <div class="clear"></div>
	</div>
	<div class="line"></div>
	<div class="line_pc"></div>
	<div class="content">
		<div class="partend partend_c">
			<?php 
				$aa =str_replace ( "\n",'<br>',$selfStoryInfo['content']);
	
				echo read_str($aa) ; ?>
		</div>
	</div>
	
	<div class="layer_pc">
		<div class="layer_left_pc"><img src="images/duixi_icon.png" width="391" height="76" /></div>
	    <div class="layer_right_pc" onClick="dj()"><img src="images/duixi_sm.png" width="71" height="83" /></div>
	    <div class="clear"></div>
	</div>
</div>
<div class="layer">
	<div class="layer_left"><img src="images/duixi_icon.png" width="100%" /></div>
    <div class="layer_right" onClick="dj()"><img src="images/duixi_ljxz.png" width="100%" /></div>
    <div class="clear"></div>
</div>
	
<script type="text/javascript">
	$(document).ready(function(){
		$(".content").toggle(function(){
				$(".partend").addClass("yanse").removeClass("partend_c");
			},
			function(){
				$(".partend").removeClass("yanse").addClass("partend_c");
			});
	});

	var width = $(window).width()
	if(width>=800){//判断pc端body的padding
		var pc_body=$("body");
		pc_body.css({paddingBottom:"160px"});
	}

	function dj(){
		window.location.href="http://www.yuxip.com/down.php";
	}
</script>


	<?php 	
	function str_split_unicode($str, $l = 0) {
	
		if ($l > 0) {
			$ret = array();
			$len = mb_strlen($str, "UTF-8");
			for ($i = 0; $i < $len; $i += $l) {
				$ret[] = mb_substr($str, $i, $l, "UTF-8");
			}
			return $ret;
		}
		//         return preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY);
		preg_match_all("/./us", $str, $cont_v);
		return $cont_v[0];
	}
	
	function get_sentence_type($str){
		$arr = str_split_unicode($str);
		$first = $arr[0];
		$end = $arr[count($arr)-1];
		 
		if($first == '［' || $end == '］'){
			return 1;
		}else if($first == '【' || $end == '】'){
			return 2;
		}else if($first == '[' || $end == ']'){
			return 3;
		}else if($first == '{' || $end == '}'){
			return 4;
		}else if($first == '“' || $end == '”'){
			return 5;
		}else{
			return 6;
		}
	}
	
	class Sentence{
		public $type; // 1 '［', 2 '【', 3 '[', 4 '{', 5 '“', 6 文本
		public $is_chat;
		public $content;
	
		function __construct($content) {
			$this->content = $content;
			$this->is_chat = false;
	
			$this->type = get_sentence_type($content);
		}
	}
	
	 
	function read_str($str){
	
		$delimiters = array();
	
		$start_delimiters = array('［','【','[', '{', '“');
		$end_delimiters = array('］','】',']', '}', '”');
		$chat_punctuations = array('，','.','。','‘','’','？','！','……','...','…');
	
		$arr_text = str_split_unicode($str);
		$temp_str = '';
		$arr = array();
	
		foreach($arr_text as $key => $value){
	
			if(in_array($value, $start_delimiters)){
	
				if(strlen($temp_str) > 0){
					$temp_sentence = new Sentence($temp_str);
					array_push($arr, $temp_sentence);
					$temp_str = '';
				}
	
				$temp_str .= $value;
	
			}else if(in_array($value, $end_delimiters)){
					
				$temp_str .= $value;
				$temp_sentence = new Sentence($temp_str);
				array_push($arr, $temp_sentence);
				$temp_str = '';
	
			}else{
				$temp_str .= $value;
			}
		}
	
		if(strlen($temp_str)){
			$temp_sentence = new Sentence($temp_str);
			array_push($arr, $temp_sentence);
		}
	
		$quotation_first = false;
	
		foreach($arr as $k=>$v){
			if($v->type == 5){
				foreach($chat_punctuations as $key=>$value){
					if(strstr($v->content, $value) || strlen($v->content) > 4){
						$quotation_first = true;
						break;
					}
				}
			}
		}
	
		foreach($arr as $k=>$v){
			if($quotation_first){
				if($v->type == 5){
					foreach($chat_punctuations as $key=>$value){
						if(strstr($v->content, $value) || strlen($v->content) > 4){
							$v->is_chat = true;
							break;
						}
					}
				}else{
					$v->is_chat = false;
				}
			}else{
				if($v->type < 5){
					$v->is_chat = false;
				}else{
					foreach($chat_punctuations as $key=>$value){
						if(strstr($v->content, $value) || strlen($v->content) > 4){
							$v->is_chat = true;
							break;
						}
					}
				}
			}
		}
	
		$content ='';
		$h_start = '<div class="partend_son">';
		$h_end = '</div>';
	
		foreach($arr as $k=>$v){
			if($v->is_chat==1){
				$content .= $h_start . $v->content . $h_end;
			}else{
				$content .=  $v->content;
			}
		}
		return $content;
	
	}	
	
	?>



	<?php $info_content=str_replace ( PHP_EOL,'',$selfStoryInfo['content']);
		$info_content = substr($info_content, '0','120');
		//echo $info_content;
	?>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script>
  wx.config({
    debug: false,
    appId: '<?php echo $signPackage["appId"];?>',
    timestamp: <?php echo $signPackage["timestamp"];?>,
    nonceStr: '<?php echo $signPackage["nonceStr"];?>',
    signature: '<?php echo $signPackage["signature"];?>',
    jsApiList: [
      // 所有要调用的 API 都要加到这个列表中
        'checkJsApi',
        'onMenuShareTimeline',
        'onMenuShareAppMessage',
        'onMenuShareQQ',
        'onMenuShareWeibo',
        'onMenuShareQZone'
     ]

  });
  wx.ready(function () {
    // 在这里调用 API
  wx.onMenuShareAppMessage({
      title: '<?php echo $selfStoryInfo['title'];?>',
      link: '<?php echo $signPackage["url"];?>',
      desc : '<?php echo $info_content;?>',
      type : 'link',
      imgUrl: '<?php echo ($selfStoryInfo["portrait"])?$selfStoryInfo["portrait"]:"http://yuxip.com/images/icon.png";?>',
      trigger: function (res) {
        //alert('用户点击发送给朋友');
      },
      success: function (res) {
        //alert('已分享');
      },
      cancel: function (res) {
        //alert('已取消');
      },
      fail: function (res) {
        //alert(JSON.stringify(res) + ' error');
      }
    });

     wx.onMenuShareTimeline({
      title: '<?php echo $selfStoryInfo['title'];?>',
      link: '<?php echo $signPackage["url"];?>',
      desc : '<?php echo $info_content;?>',
      type : 'link',
      imgUrl: '<?php echo ($selfStoryInfo["portrait"])?$selfStoryInfo["portrait"]:"http://yuxip.com/images/icon.png";?>',   
      });

  wx.onMenuShareQQ({
      title: '<?php echo $selfStoryInfo['title'];?>',
      link: '<?php echo $signPackage["url"];?>',
      desc : '<?php echo $info_content;?>',
      type : 'link',
      imgUrl: '<?php echo ($selfStoryInfo["portrait"])?$selfStoryInfo["portrait"]:"http://yuxip.com/images/icon.png";?>',
      trigger: function (res) {
        //alert('用户点击发送给朋友');
      },
      success: function (res) {
        //alert('已分享');
      },
      cancel: function (res) {
        //alert('已取消');
      },
      fail: function (res) {
        //alert(JSON.stringify(res) + ' error');
      }
    });

  wx.onMenuShareQZone({
	  title: '<?php echo $selfStoryInfo['title'];?>',
      link: '<?php echo $signPackage["url"];?>',
      desc : '<?php echo $info_content;?>',
      type : 'link',
      imgUrl: '<?php echo ($selfStoryInfo["portrait"])?$selfStoryInfo["portrait"]:"http://yuxip.com/images/icon.png";?>',
      trigger: function (res) {
        //alert('用户点击发送给朋友');
      },
      success: function (res) {
        //alert('已分享');
      },
      cancel: function (res) {
        //alert('已取消');
      },
      fail: function (res) {
        //alert(JSON.stringify(res) + ' error');
      }
    });

  });

</script>
</body>
</html>
