<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title><?php echo $storyInfo['title'];?>_语戏</title>
<link rel="stylesheet" type="text/css" href="/ichees/css/style.css" />
</head>
<body>
<section>
	<div class="head"><h1><?php echo $storyInfo['title']?></h1></div>
    <div class="time_logo">
    	<div class="time"><span id="tt"><?php echo date('Y-m-d',$storyInfo['createtime'])?></span></div>
        <div class="logo"></div>
        <div class="clear"></div>
    </div>
</section>
<article class="article_1">
	<div class="paragraph">
		<?php foreach($chapterList as $value) {?>
			<h3><?php echo $value['nickname'] ?></h3>
			<p>
				<?php if($value['content'] and $value['content']!= '还没有戏文') {
						echo $value['content'] ;
					} else {?>
					<div class="noContent"><img src="/ichees/images/noContent.png"></div>
				<?php }?>
			</p>
		<?php }?>
    </div>
</article>
<div class="wwdx"><img src="/ichees/images/wwdx.jpg" width="100%" alt="" /></div>
<footer>
<div class="footer_1">
	<div class="footer_left"><img src="/ichees/images/img_1.jpg" width="100%" alt="" /></div>
    <div class="footer_right"><a href='http://www.yuxip.com/down.php' target="_blank" title=""><img src="images/img_2.jpg" width="100%" alt="" /></a>
    </div>
    <div class="clear"></div>
</div>
</footer>

<?php $info_intro=str_replace ( PHP_EOL,'', $storyInfo['intro'] );?>

<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script>
  wx.config({
    debug: false,
    appId: '<?php echo $signPackage["appId"];?>',
    timestamp: '<?php echo $signPackage["timestamp"];?>',
    nonceStr: '<?php echo $signPackage["nonceStr"];?>',
    signature: '<?php echo $signPackage["signature"];?>',
    jsApiList: [
      // 所有要调用的 API 都要加到这个列表中
        'checkJsApi',
        'onMenuShareTimeline',
        'onMenuShareAppMessage',
        'onMenuShareQQ',
        'onMenuShareWeibo',
        'onMenuShareQZone'
     ]

  });
  wx.ready(function () {
    // 在这里调用 API
  wx.onMenuShareAppMessage({
      title: '<?php echo $storyInfo['title'];?>',
      link: '<?php echo $signPackage["url"];?>',
      desc : '<?php echo $info_intro;?>',
      type : 'link',
      imgUrl: '<?php echo ($storyInfo["portrait"])?$storyInfo["portrait"]:"http://yuxip.com/images/icon.png";?>',
      trigger: function (res) {
        //alert('用户点击发送给朋友');
      },
      success: function (res) {
        //alert('已分享');
      },
      cancel: function (res) {
        //alert('已取消');
      },
      fail: function (res) {
        //alert(JSON.stringify(res) + ' error');
      }
    });

     wx.onMenuShareTimeline({
      title: '<?php echo $storyInfo['title'];?>',
      link: '<?php echo $signPackage["url"];?>',
      desc : '<?php echo $info_intro;?>',
      type : 'link',
      imgUrl: '<?php echo ($storyInfo["portrait"])?$storyInfo["portrait"]:"http://yuxip.com/images/icon.png";?>',   
      });

  wx.onMenuShareQQ({
      title: '<?php echo $storyInfo['title'];?>',
      link: '<?php echo $signPackage["url"];?>',
      desc : '<?php echo $info_intro;?>',
      type : 'link',
      imgUrl: '<?php echo ($storyInfo["portrait"])?$storyInfo["portrait"]:"http://yuxip.com/images/icon.png";?>',
      trigger: function (res) {
        //alert('用户点击发送给朋友');
      },
      success: function (res) {
        //alert('已分享');
      },
      cancel: function (res) {
        //alert('已取消');
      },
      fail: function (res) {
        //alert(JSON.stringify(res) + ' error');
      }
    });

  wx.onMenuShareQZone({
	  title: '<?php echo $storyInfo['title'];?>',
      link: '<?php echo $signPackage["url"];?>',
      desc : '<?php echo $info_intro;?>',
      type : 'link',
      imgUrl: '<?php echo ($storyInfo["portrait"])?$storyInfo["portrait"]:"http://yuxip.com/images/icon.png";?>',
      trigger: function (res) {
        //alert('用户点击发送给朋友');
      },
      success: function (res) {
        //alert('已分享');
      },
      cancel: function (res) {
        //alert('已取消');
      },
      fail: function (res) {
        //alert(JSON.stringify(res) + ' error');
      }
    });

  });

</script>
    
</body>
</html>
