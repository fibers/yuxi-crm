<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<!-- 强制使IE使用edge模式渲染 -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- 强制指定360浏览器以急速模式渲染 -->
<meta name="renderer" content="webkit">

<meta name="format-detection" content="telephone=no">

<title>ichees</title>
<!-- /chudong/ichees/ -->
<link href="/ichees/css/icheesReset.css" rel="stylesheet" type="text/css">
<link href="/ichees/css/icheesTools.css" rel="stylesheet" type="text/css">
<link href="/ichees/css/icheesStyle.css" rel="stylesheet" type="text/css">
<script src="/ichees/js/jquery-1.10.1.min.js" type="text/javascript"></script>
</head>

<body>
	<!-- <nav>
		<ul>
			<li><a href="#"><img src="/ichees/images/back.png"></a></li>
			<li><a href="#">剧情</a></li>
			<li><a href="/ichees/read">阅读</a></li>
			<li><a href="/ichees/copyright" class="on">版权</a></li>
		</ul>
	</nav> -->
	<div class="coverBox">
		<div class="cover" style="background-image: url('<?php echo $storyInfo['portrait'] ?>');"></div>
		<div class="coverMask">
			<div class="coverMaskLeft"></div>
			<div class="coverMaskArrow"></div>
			<div class="coverMaskRight"></div>
		</div>
	</div>
	<div class="bookInfo clearfix">
		<div class="bookThumb"><img src="/ichees/images/thumb.png"></div>
		<div class="bookDes">
			<h1><?php echo $storyInfo['title']?></h1>
			<p>创建者:<?php echo $storyInfo['creatorname']?></p>
			<p><?php echo date("m月d日 H:i",$storyInfo['createtime'])?></p>
			<p>版权编号:<?php echo $storyInfo['version']?></p>
		</div>
	</div>
	<div class="copyrightList">
		<?php foreach($greaterList as $value) { ?>
		<div class="copyrightItem clearfix">
			<div class="avatar">
				<img src="<?php

				echo $value['avatar']?$value['avatar']:'/ichees/images/blank.png';
				?>">
			</div>
			<div class="itemDes">
				<h2><?php echo $value['nick'] ?>
					<?php if($storyInfo['creatorid'] == $value['userid']) { ?>
					<span><b>/</b>创建者</span>
					<?php } else if ($value['bid']) {?>
					<span><b>/</b>主线人物</span>
					<?php }?>
				</h2>
				<p><?php echo $value['rate'] ?>% 版权份额</p>
				<p>创作 <?php echo $value['counter'] ?> 字</p>
			</div>
		</div>
		<?php } ?>
	</div>
	
	<footer>
		<ul class="clearfix">
			<li>
				<a href="#"><img src="/ichees/images/share.png"><p>分享</p></a>
			</li>
			<li>
				<a href="#" class="on"><img src="/ichees/images/like_on.png"><p>喜欢</p></a>
			</li>
			<li>
				<a href="#"><img src="/ichees/images/onshow.png"><p>对戏</p></a>
			</li>
		</ul>
	</footer>
</body>
</html>