<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>语戏_在线戏阅读</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<script src="/ui/js/jquery-1.11.1.min.js"></script>
<script src="/ui/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/ui/js/juicer.js" type="text/javascript"></script>
<script src="/ui/js/simple-modal.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="/ichees/css/zimu.css">
</head>
<body>
<div class="bg">
	<div class="bg_content">
        <div class="bg_left">
        	<div class="bg_left_title">
            	<h1 class="s_title"></h1>
                <p class="s_intro"></p>
            </div>
            <div class="s_content">
	            
            </div>
            <div id="msg_end" style="height:0px; overflow:hidden"></div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<input type="hidden" class="story_id" value="<?php echo $storyid;?>" />
    <script>
        var Read = {
            compiledTpl : null,
            compiledAddTpl : null,
            getRead : function(){
            	var story_id = $(".story_id").val();
                $.getJSON('/ichees/active_read', {
                    story_id:story_id
                }, function(data) {
                	if(data.code=='-1'){
						alert(data.msg);
                	}else{
                		$(".s_title").html(data.storyInfo.title);
                		$(".s_intro").html(data.storyInfo.intro);
                		Read.tpl_js(data.chapterList);
                		
                		var _tpl = Read.compiledTpl.render();
                        $(".s_content").data('max_msgId',data.max_msgId);
                        $(".s_content").html(_tpl);

                        //Read.onloadRead();
                        window.setInterval("Read.onloadRead()",3000);
                	}
                });
            },
           /* tpl : function(){
                var tpl=[
                    '{@each chapterList as chap}',
                    '	<div class="bg_left_name">${chap.nickname}</div>',
    	            '	<div class="bg_left_content">',
    	            '		<p>${chap.content}'+'<br><br><br>'+'</p>',
    	            '	</div>',
                    '{@/each}'
                ].join('\n');
                Read.compiledTpl = juicer(tpl);
            },*/
            tpl_js : function(chapterList){
                var  tpl_html = '';
            	$.each(chapterList, function(i) {     
            	    //alert(chapterList[i]['userId']);   
            		tpl_html += '	<div class="bg_left_name">'+chapterList[i]["nickname"]+'</div>',
            		tpl_html += '	<div class="bg_left_content">',
            		tpl_html += '		<p>'+chapterList[i]["content"]+'</p>',
            		tpl_html += '	</div>';
            	});  
            	Read.compiledTpl = juicer(tpl_html);
            	//alert(tpl_html);
            },
            onloadRead:function(){
            	var story_id = $(".story_id").val();
            	var max_msgId = $(".s_content").data('max_msgId');
                $.getJSON('/ichees/ajax_read', {
                    story_id:story_id,max_msgId:max_msgId
                }, function(data) {
                	if(data.code=='-1'){
						alert(data.msg);
                	}else{
                		Read.tpl_js(data.chapterList);
                		//Read.tpl();
                		var _tpl = Read.compiledTpl.render();
                        $(".s_content").data('max_msgId',data.max_msgId);
                        $(".s_content").append(_tpl);
                       // 
                       // alert($(window).height());

                        //$(document).scrollTop()>=$(document).height()-$(window).height()
                        /*var offsetTop = defaultTop + $(window).scrollTop()+'px';
                    	thisBox.animate({top:offsetTop},
                    	{	duration: 600,	//滑动速度
                    		queue: false    //此动画将不进入动画队列
                    	});
                    	
                        $('html').animate({height:-($(document).height()-$(window).height())},
                    	{	duration: 600,	//滑动速度
                    		queue: false    //此动画将不进入动画队列
                    	});

                    	*/

                    	//$(document).scrollTop($(document).height()); 

                        //alert($(".s_content").height());
                       // $(".s_content").scrollTop($(".s_content").height()); 

                        msg_end.scrollIntoView();
                        
                	}
                });
            }


        }
        
        $(function(){
            Read.getRead();
            
        })
    </script>
    <style>
    	.bg_left_content p{word-wrap:break-word;}
    </style>
</body>
</html>