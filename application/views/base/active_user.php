<aside class="right-side">
    
    <section class="content-header">
        <h1>
            活跃用户
            <small>欢迎来到IM</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> 首页</a></li>
            <li class="active">活跃用户</li>
        </ol>
    </section>

    <section class="content">
        <a href="javascript:;" style="margin-right:30px" class="active_day">日活跃用户</a>
        <a href="javascript:;" style="margin-right:30px" class="active_week">周活跃用户</a>
        <a href="javascript:;" class="active_month">月活跃用户</a>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>时间</th>
                    <th>活跃用户数</th>
                </tr>
            </thead>
            <tbody>
            	<tr align="center"><td colspan="2"><img src="/ichees/images/loading.gif" /></td></tr>
            </tbody>
        </table>
    </section>
    <script>
        var User = {
            compiledTpl : null,
            compiledAddTpl : null,
            getDay : function(page){
                if(!page){
                    page = 0;
                }
                $.getJSON('/online/activeDayList', {
                    start : page
                }, function(data) {
                    User.tpl();
                    var _tpl = User.compiledTpl.render(data);
                    $("table").data('departs',data.departs);
                    $("tbody").html(_tpl);

                    $('.content').find('a').removeClass('active_back');
                    $(".active_day").addClass('active_back');
                });
            },
            getWeek : function(){
                $.getJSON('/online/activeWeekList', { }, function(data) {
                    User.tpl();
                    var _tpl = User.compiledTpl.render(data);
                    $("table").data('departs',data.departs);
                    $("tbody").html(_tpl);

                    $('.content').find('a').removeClass('active_back');
                    $(".active_week").addClass('active_back');
                });
            },
            getMonth : function(){
                $.getJSON('/online/activeMonthList', { }, function(data) {
                    User.tpl();
                    var _tpl = User.compiledTpl.render(data);
                    $("table").data('departs',data.departs);
                    $("tbody").html(_tpl);
                    
                    $('.content').find('a').removeClass('active_back');
                    $(".active_month").addClass('active_back');
                });
            },
            tpl : function(){
                var tpl=[
                    '{@each users as user}',
                    '   <tr data-id="${user.date}">',
                    '       <td>${user.date}</td>',
                    '       <td>${user.num}</td>',
                  //  '       <td>${user.total}</td>',                
                   
                    '   </tr>',
                    '{@/each}'
                ].join('\n');
                User.compiledTpl = juicer(tpl);
            }
        }
        
        $(function(){
        	User.getDay();

        	$(".active_day").click(function(){
                User.getDay();
            })
            
        	$(".active_week").click(function(){
                User.getWeek();
            })

            $(".active_month").click(function(){
                User.getMonth();
            })
        })
    </script>
    <style>
        .add_user_div .div_20{
            margin-top: 20px;
        }
        .active_day,.active_week,.active_month{
        	display: inline-block;
			padding: 10px;
        }
        .active_back{background: #eee;}
        
    </style>
</aside>