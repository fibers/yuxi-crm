<aside class="right-side">
    <section class="content-header">
        <h1>
            广告管理
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> 首页</a></li>
            <li class="active">广告管理</li>
        </ol>
    </section>
    <section class="content">
        <button style="margin-bottom:10px;" class="btn btn-primary btn-sm add_banners pull-right">添加广告位</button>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>广告位</th>
                    <th>剧名称</th>
                    <th>说明</th>
                    <th>链接</th>
                    <th>点击量</th>
                    <th>排序</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <span style="margin-bottom:10px;" class="btn btn-info btn-sm pull-left">排序值越小，显示越靠前</span>
    </section>
    <script type="text/javascript">
        var banners = {
            compiledTpl : null,
            compiledAddTpl : null,
            getbanners : function(){
                $.getJSON('/banners/all', {
                }, function(data) {
                    banners.tpl();
                    console.log(data);
                    var _tpl = banners.compiledTpl.render(data);
                    $("tbody").html(_tpl);
                });
            },
			tpl : function(){
                var tpl=[
                    '{@each banners as banner}',
                    '   <tr data-id="${banner.id}">',
					'       <td>{@if banner.image}<img style="height:30px;" src="${banner.image}!w250">{@/if}</td>',
                    '       <td><a target="_blank" href="/ichees/read?storyid=${banner.storyid}">${banner.story_name}</a></td>',
                    '       <td>${banner.title}</td>',
                    '       <td  style="word-break: break-all;">${banner.url}</td>',
                    '       <td>${banner.banners_hit}</td>',
                    '       <td>${banner.id}</td>',
                    '       <td><button style="margin-right:10px;" class="btn btn-warning btn-sm edit_banners">修改</button><button href="javascript:;" class="btn btn-danger btn-sm del_banners">删除</button></td>',
                    '   </tr>',
                    '{@/each}'
                ].join('\n');
                banners.compiledTpl = juicer(tpl);
            },
			addbannersAlert : function(){
                banners.addTpl();
                $.fn.SimpleModal({
                    btn_ok: '添加',
                    model: 'confirm',
                    callback: function(node){
                        banners.addbanners();
                    },
                    overlayClick: false,
                    width: 660,
                    title: '添加广告位',
                    contents: banners.compiledAddTpl.render()
                }).showModal();
				/*$.getJSON('/banners/story', {
                }, function(data) {
					banners.storyTpl(0);
                    var _tpl = banners.compiledTpl.render(data);alert(_tpl);
					$('.storyid').html(_tpl);
                });*/
            },
			storyTpl : function(){
				var tplHtml =  '<option value="">--请选择剧--</option>';
				var tpl =[
                    '  {@each story as s}',
                    '       <option value="${s.id}">${s.title}(${s.id})</option>',
                    '  {@/each}',
                ].join('\n');
                banners.compiledTpl = juicer(tplHtml+tpl);
            },
			
            addTpl : function(){
                var tpl =[
                    '<div class="add_banners_div" role="form">',
					'<div class="search"><input type="text" placeholder="输入搜索的剧名称" class="form-control input-sm search_story" />',
					'<span class="search_banners">搜索</span><span class="search_txt"></span></div>',
					'<span class="span">剧名称：</span>',
                    '<select type="text" class="form-control input-sm storyid" placeholder="storyid"></select>',
					'<span class="span">图片说明：</span>',
					'   <input type="text" class="form-control input-sm title" placeholder="图片说明">',
					'<span class="span orders_span"  style="display:none;">排序：</span>',
                    '   <input type="text" style="display:none;" class="form-control input-sm orders" max_order="" placeholder="排序">',
					'<span class="span">图片：</span>',
                    '   <input type="file" id="file_image" class="file_image" name="file_image">',
                    '<span class="span-load"></span><img src="/ichees/images/loading.gif" class="image_loading" />',
					'   <input type="hidden" id="image" class="image" name="image">',
					'<span class="span">链接：</span>',
					'   <input type="text" class="form-control input-sm url" placeholder="链接">',
                    '</div>'
                ].join('\n');
                banners.compiledAddTpl = juicer(tpl);
            },

            searchbannersAlert : function(){
				$.getJSON('/banners/story', {title: $(".search_story").val(),
                }, function(data) {
	                if(data.story =='fail'){
	                	$('.search_txt').html("没有搜索到相关数据");
	                }else{
	                	banners.storyTpl(0);
	                    var _tpl = banners.compiledTpl.render(data);
	                    $('.storyid').html(_tpl);
	                    $('.search_txt').html("");
		            }
                });
            },
			
			upload : function(node){
                var file = $('.file_image')[0].files[0];
                var reader = new FileReader();
                reader.onload = function (rResult) {
                	var strload = '正在上传图片，请稍后...';
                	$('.image_loading').show();
                	$(".span-load").text(strload);
                    var filename = file.name;
                    var options = {
                        type: 'POST',
                        url: '/banners/upload?filename='+filename,
                        data: reader.result,
                        success:function(result){
                            var data = result;
                            if(data.status == 'success'){
                                $(".image").val(data.file);
                                $('.image_loading').hide();
                                $(".span-load").html('<span style="color:green">图片上传成功</span>');
                            }else{
                                $('.image_loading').hide();
                                $(".span-load").html('<span style="color:red">图片上传失败</span>');
                            }
                        },
                        processData: false,
                        contentType: false,
                        dataType:"json"
                    };
                    $.ajax(options);
                };
                reader.readAsArrayBuffer(file);
           },
           addbanners : function(){
                $.post('/banners/add', {
                    storyid: $(".storyid").val(),
                    title: $(".title").val(),
                    image: $(".image").val(),
					url: $(".url").val()
                }, function(data) {
                    if($.trim(data) == 'success'){
                        $.fn.hideModal();
                        $(".table").before('<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>添加成功</div>')
                        setTimeout(function(){
                            $(".alert").remove();
							 banners.getbanners();
                        },3000);
                    }else{
                        $(".btn-margin").text('添加失败');
                    }
                });
            },
		   
           
            editbanners : function(){
                $.post('/banners/edit', {
                    id:$(".btn-margin-edit").data('id'),
                    storyid: $(".storyid").val(),
                    title: $(".title").val(),
                    image: $(".image").val(),
					url: $(".url").val(),
					orders: $(".orders").val()
                }, function(data) {
                    if($.trim(data) == 'success'){
                        $.fn.hideModal();
                        $(".table").before('<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>编辑成功</div>')
                        setTimeout(function(){
                            $(".alert").remove();
                        },3000);
                        banners.getbanners();
                    }else if($.trim(data) == 'orderErr'){
						 $(".btn-margin-edit").text('无效的排序');
					}else{
                        $(".btn-margin-edit").text('编辑失败');
                    }
                });
            },

            editiscoveryAlert : function(node){
                banners.addTpl();
                $.fn.SimpleModal({
                    btn_ok: '编辑',
                    model: 'confirm',
                    callback: function(node){
                        banners.editbanners();
                    },
                    overlayClick: false,
                    width: 660,
                    title: '修改广告位',
                    contents: banners.compiledAddTpl.render()
                }).showModal();
				
				/*$.getJSON('/banners/story', {
                }, function(data) {
					banners.storyTpl();
                    var _tpl = banners.compiledTpl.render(data);
					$('.storyid').html(_tpl);
                });*/
				
                $.post('/banners/get', {
                    id:node.data('id')
                }, function(obj) {
					var data = JSON.parse(obj);
					//$(".storyid").find("option[value='"+data.id+"']").attr("selected",true);
					if(data.storyid!='0')
                    	$(".storyid").html('<option value="'+data.storyid+'">'+data.story_name+'('+data.storyid+')</option>');
					$(".orders").show();   
					$(".orders_span").show();   					
                    $(".orders").val(data.id);
                    $(".title").val(data.title);
                    $(".url").val(data.url);
					$(".image").val(data.image);
                    $(".btn-margin").addClass("btn-margin-edit");
                    $(".btn-margin-edit").data('id',node.data('id'));
                });
            }
        }
        $(function(){
            banners.getbanners();

            $(".add_banners").click(function(){
                banners.addbannersAlert();
            })
			
			$(".file_image").live("change",function(){
                banners.upload();
            })
            $(".edit_banners").live("click",function(){
                banners.editiscoveryAlert($(this).parents('tr'));
            })

            $(".del_banners").live("click",function(){
            	if(confirm('确认要删除吗？')){
	                var node = $(this).parents('tr')
	                $.post('/banners/del', {id: node.data('id')}, function(data) {
	                    if($.trim(data) == 'success'){
	                        node.fadeOut();
	                    }
	                });
            	}
            })
			
			$(".search_banners").live("click",function(){
                banners.searchbannersAlert($(this).parents('tr'));
            })
            
        })
    </script>
    <style>
        .add_banners_div input{  margin-top: 3px;}
        .file_image{margin-left:20px;margin-bottom:20px;float:left;}
		.add_banners_div .span{margin-top:10px;display: block; clear: both;}
        .image_loading{width:20px;height:20px;float:left;display:none;}
        .search{height:40px;}
        .search_banners{color: #FFF; text-shadow: 0 1px 0 rgba(0, 0, 0, 0.25);background-color: #999;background-repeat: repeat-x;
        margin:3px 0 0 15px;padding: 5px 14px 6px;float:left; cursor: pointer;}
        .search_story{width:50%;float:left;}
        .search_txt{line-height: 35px; padding-left: 5px;}
    </style>
</aside>    