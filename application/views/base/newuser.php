<script src="/ui/js/Chart.js"></script>
<script src="/ui/js/My97DatePicker/WdatePicker.js"></script>

<!-- <script src="/ui/js/gg_bd_ad.js"></script>
<script src="/ui/js/jquery.Jcrop.js"></script>
<script src="/ui/js/follow.js"></script>
<link rel="stylesheet" href="/ui/css/jquery.Jcrop.css" type="text/css" />
-->

<aside class="right-side">
	<section class="content-header">
		<h1>
			新增用户统计 <small>欢迎来到IM</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="/"><i class="fa fa-dashboard"></i> 首页</a></li>
			<li class="active">新增用户统计</li>
		</ol>
	</section>
	<section>
		<div style="height: 30px; margin: 10px 50px;">
			<select class="dateselect" style="height: 30px;"
				onchange="User.dateselect('daily')">
				<option value="60">过去60天</option>
				<option value="30">过去30天</option>
				<option value="7">过去7天</option>
				<option value="0" selected>今天</option>
				<option value="99">自选</option>
			</select> 
			<span class="search_span" style="display: none"> 选择时间：<input type="text"
				id="d4311" onClick="WdatePicker({maxDate:'#F{$dp.$D(\'d4312\')}'})"
				class="Wdate keystarttime" style="height: 30px;" /> - <input
				type="text" id="d4312"
				onClick="WdatePicker({minDate:'#F{$dp.$D(\'d4311\')}'})"
				class="Wdate keyendtime" style="height: 30px;" />
				<button style="margin-bottom: 6px;"
					class="btn btn-primary btn-sm searchkey pull-center">搜索</button>
			</span>
			<div class="particle">
				<ul class="js-timeUnit">
					<li data-unit="hourly" data-enable="true" class="hourly off">小时</li>
					<li data-unit="daily" data-enable="true" data-on="true"
						class="daily on">日</li>
					<li data-unit="weekly" data-enable="false" class="weekly">周</li>
					<li data-unit="monthly" data-enable="false" class="monthly">月</li>
				</ul>
			</div>
		</div>
		<div id="canvasParent">
			<canvas id="canvas" height="450" width="1000"></canvas>
		</div>

		<div style="text-align: center; clear: both;"></div>
		<script>
	</script>
	</section>
	<section class="content">

		<table class="table table-bordered">
			<thead>
				<tr>
					<th>日期</th>
					<th>新增用户数</th>
					<th>总用户数</th>
					<!--<th>平均访问时长</th>-->
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>

	</section>
	<script>
        var User = {
            compiledTpl : null,
            compiledAddTpl : null,
            getUser : function(page){
                if(!page){
                    page = 0;
                }
                $.getJSON('/newuser/getDayData', {
                    start : page
                }, function(data) {
                    User.tpl();
                    var _tpl = User.compiledTpl.render(data);
                   // $("table").data('departs',data.departs);
                    $("tbody").html(_tpl);
                });
            },
            tpl : function(){
                var tpl=[
                    '{@each users as user}',
                    '   <tr data-id="${user.date}">',
                    '       <td>${user.date}</td>',
                    '       <td>${user.num}</td>',
                    '       <td>${user.total}</td>',
                    '   </tr>',
                    '{@/each}'
                ].join('\n');
                User.compiledTpl = juicer(tpl);
            },
            getNewUser:function (dataunit){
                if(!dataunit){
                	dataunit = $(".on").attr("data-unit");
                }
            	var keystarttime = $('.keystarttime').val();
            	var keyendtime = $('.keyendtime').val();
            	
            	$.getJSON('/newuser/getnewuser', {
            		keystarttime : keystarttime,keyendtime : keyendtime,dataunit:dataunit
                }, function(da) {
                	User.getNewTpl(da);
        		});
            },
            getNewTpl:function (da){
            	User.tpl();
                var _tpl = User.compiledTpl.render(da);
                $("tbody").html(_tpl);

                
				//设置是否允许点击
				$.each(da.dataunitfalse, function(i, item) {
					$("."+item).removeClass('on').addClass('off').attr('data-enable', 'false').attr('data-on', 'false');
	            })

				$.each(da.dataunitrue, function(i, item) {
					$("."+item).removeClass('on').removeClass('off').attr('data-enable', 'true');
					$("."+item).attr('data-on', 'true')
	            })
	            
	            $("."+da.dataunit).addClass('on');

                //重置画布
            	$('#canvas').remove();
            	$('#canvasParent').append('<canvas id="canvas" height="450" width="1000"></canvas>');
                var lineChartData = {
					labels : da.labels,//["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23"],
					datasets : [
						{
							fillColor : "rgba(151,187,205,0.5)",
							strokeColor : "rgba(151,187,205,1)",
							pointColor : "rgba(151,187,205,1)",
							pointStrokeColor : "#fff",
							data : da.data//[99,66,55,28,48,40,19,96,27,100,28,48,40,19,96,27,100,28,48,40,19,96,27,100]
						}
					]
				}
	            var defaults = {
		    	    //Boolean - If we show the scale above the chart data			
		    	    scaleOverlay : false,
		    	    
		    	    //Boolean - If we want to override with a hard coded scale
		    	    //是否用硬编码重写y轴网格线
		    	    scaleOverride : true,
		    	    
		    	    //** Required if scaleOverride is true **
		    	    //Number - The number of steps in a hard coded scale
		      	    //y轴刻度的个数
		    	    scaleSteps : da.scaleSteps,
		    	    
		    	    //Number - The value jump in the hard coded scale
		    	    //y轴每个刻度的宽度
		    	    scaleStepWidth : da.scaleStepWidth,
		    	    
		    	    // Y 轴的起始值
		    	    scaleStartValue : null,
	
			    	 // 是否使用贝塞尔曲线? 即:线条是否弯曲
	        	    bezierCurve : false,
	            }
				var myLine = new Chart(document.getElementById("canvas").getContext("2d")).Line(lineChartData,defaults);

            },
            getBeforeDate:function(n){
                var n = n;
                var d = new Date();
                var year = d.getFullYear();
                var mon=d.getMonth()+1;
                var day=d.getDate();
                if(day <= n){
                	if(mon>1) {
                    	mon=mon-1;
                    }else {
                        year = year-1;
                        mon = 12;
                    }
                }
                d.setDate(d.getDate()-n);
                year = d.getFullYear();
                mon=d.getMonth()+1;
                day=d.getDate();
                s = year+"-"+(mon<10?('0'+mon):mon)+"-"+(day<10?('0'+day):day);
                return s;
            },
            dateselect:function(dataunit){
    			//当前选择日期
            	var dateselect = $(".dateselect option:selected").val();
            	if(dateselect=='99'){
            		$(".search_span").show();
            	}else{
            		$(".search_span").hide();
            		var start_time = User.getBeforeDate(dateselect);
    	        	var end_time = User.getBeforeDate(0);
    	        	
    	        	$.getJSON('/newuser/getnewuser', {
    	        		keystarttime : start_time,keyendtime : end_time,dataunit:dataunit
    	            }, function(da) {
    	            	User.getNewTpl(da);
    	    		});
            	}
            }
        }
        
        $(function(){
           // User.getUser();

            User.getNewUser();
            
            $('.searchkey').live('click',function(){
            	//获取新数据
            	$('.js-timeUnit li').removeClass('on');
            	$(".daily").addClass('on');
            	
            	User.getNewUser("daily");
            });

            $('.js-timeUnit li').live('click',function(){
            	if($(this).attr('data-enable') === 'true'){
	            	$('.js-timeUnit li').removeClass('on');
	            	$(this).addClass('on');

	            	var dateselect = $(".dateselect option:selected").val();
	            	if(dateselect=='99'){
	            		//获取新数据
		            	User.getNewUser();
	            	}else{
	            		User.dateselect($(this).data('unit'));
	            	}
	            	
            	}
            });
        })
    </script>

	<script>


    /*
    * Time Unit Widget
    *
    * Author: wangfang@umeng.com
    * Date: 2013.12.09
    * Dependencies:
    */
    /*;(function($) {
        $.Widget("umeng.timeUnit", {
            options: {
                onClickItem: function() {}
            },
            _create: function() {
                var self  = this,
                    o     = self.options,
                    el    = self.element,
                    elID  = el.get(0).id,
                    items = el.find('li');
                // init style
                $.each(items, function(i, item) {
                    var $item = $(item);
                    if($item.attr('data-on') === 'true') $item.addClass('on');
                    if($item.attr('data-enable') === 'true') self._setItemEnable($item);
                    else if($item.attr('data-enable') === 'false') self._setItemDisable($item);
                });
                // set on item
                $.subscribe("timeunit.on", function(event, unit, callback) {
                    items.removeClass('on').filter('[data-unit="' + unit + '"]').addClass('on');
                    if (typeof callback == 'function') {
                        try {
                            callback();
                        } catch (e) {}
                    }
                });
                // set enable items
                $.subscribe("timeunit.disable", function(event, units, callback) {
                    $.each(items, function(i, item) {
                        var $item = $(item);
                        (units.indexOf($item.attr('data-unit')) != -1) ? self._setItemDisable($item) : self._setItemEnable($item);
                    });
                    if (typeof callback == 'function') {
                        try {
                            callback();
                        } catch (e) {}
                    }
                });
            },
            _setItemEnable: function($item) {
                var self = this,
                    o    = self.options;  
                $item
                    .removeClass('off')
                    .attr('data-enable', 'true')
                    .off().on('click.timeunit', function(e) {
                        if(!$item.hasClass('on')) {
                            $item.addClass('on').siblings('li[data-enable="true"]').removeClass('on');
                            o.onClickItem($item.attr('data-unit'));
                        }
                    });
            },
            _setItemDisable: function($item) {
                var self  = this,
                    o     = self.options;
                if ($item.hasClass('on')) {
                    $item.off().removeClass('on').addClass('off').attr('data-enable', 'false');
                    if ($item.next().length !== 0) {
                        self._setItemEnable($item.next());
                    } else {
                        var $first = $item.siblings('li[data-enable="true"]:eq(0)');
                        if ($first.length !== 0) {
                            $first.addClass('on');
                            self._setItemEnable($first);
                        }
                    }
                } else {
                    $item.off().addClass('off').attr('data-enable', 'false');
                }
            },
            getUnit: function() {
                return this.element.find('li.on').data('unit');
            },
            set: function(opts) {
                var self  = this,
                    el    = self.element;
                    items = el.find('li');
                $.each(items, function(i, item) {
                    var $item = $(item).removeClass('on');
                    if (opts.disable.indexOf($item.data('unit')) != -1 ) {
                        self._setItemDisable($item);
                    } else {
                        self._setItemEnable($item);
                        if($item.data('unit') === opts.on) $item.addClass('on');
                    }
                });
            },
            onClickItem: function(callback) {
                var self  = this,
                    el    = self.element;
                    items = el.find('li');
                $.each(items, function(i, item) {
                    var $item = $(item);
                        self.options.onClickItem = callback;
                    if($item.attr('data-enable') === 'true') {
                        $item.off().on('click.timeunit', function(e) {
                            _track_userEvent('选择错误', $item.data('unit'), UMENG.Agent.getDate().counts);
                            $item.addClass('on').siblings().removeClass('on');
                            self.options.onClickItem($item.attr('data-unit'));
                        });
                    }
                });
            }
        });
        // auto init
        $(function () {
            $('.js-um-timeUnit').timeUnit();
        });
    })(jQuery);
    */
    </script>
	<style>
.add_user_div .div_20 {
	margin-top: 20px;
}

.radio span {
	display: inline-block;
	width: 50px;
}

.add_user_div div.radio {
	margin: 0;
}

.add_user_div input,.radio,.add_user_div select {
	width: 300px;
	float: left;
}

.radio input {
	width: 20px;
}

/*particle*/
.particle {
	display: inline-block;
	zoom: 1;
	height: 30px;
	line-height: 30px;
}

.particle li {
	padding: 0 3px;
	margin-right: 22px;
	cursor: pointer;
	display: inline-block;
	zoom: 1;
}

.particle li.on {
	border-bottom: 8px solid #CCC;
}

.particle li.off {
	color: #999;
	cursor: default;
}
</style>
</aside>
