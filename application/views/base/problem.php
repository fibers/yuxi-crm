<aside class="right-side">
    <section class="content-header">
        <h1>
           消息反馈
            <small>欢迎来到IM</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> 首页</a></li>
            <li class="active">消息反馈管理</li>
        </ol>
    </section>
    <section class="content">
    	<select id="keyisdeal" name="keyisdeal" style="height:26px;">
    		<option value="">--请选择是否处理信息--</option>
    		<option value="2">是</option>
    		<option value="1">否</option>
    	</select>
    	
        <button style="margin-bottom:6px;" class="btn btn-primary btn-sm searchkey pull-center">搜索</button>
        
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>内容</th>
                    <th>语戏ID</th>
                    <th>QQ</th>
                    <th width="8%">是否处理</th>
                    <th>编辑</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <ul class="pagination">
            <li class="prev_page">
                <a href="javascript:;">上一页</a>
            </li>
            <li class="curr_page">
              	 <a href="javascript:;"></a>
            </li>
            <li class="show_page">
                <a href="javascript:;"></a>
            </li>
            <li class="next_page">
                <a href="javascript:;">下一页</a>
            </li>
        </ul>
    </section>
    <script>
        var Problem = {
            compiledTpl : null,
            compiledAddTpl : null,
            compiledSignTpl : null,
            compiledallWords : null,
            getProblem : function(page){
                if(!page){
                    page = 0;
                }

                var keyisdeal = $('#keyisdeal').val();
               
                $.getJSON('/problem/all', {
                    start : page,keyisdeal: keyisdeal
                }, function(data) {
                    Problem.tpl();
                    var _tpl = Problem.compiledTpl.render(data);
                
                    $("tbody").html(_tpl);
                    if(data.page == 0){
                        $(".pagination .prev_page").addClass("disabled");   
                    }else{
                        $(".pagination .prev_page").removeClass("disabled").data('page',data.page-0-1);   
                    }
                    if(data.page == (data.count-1)){
                        $(".pagination .next_page").addClass("disabled");   
                    }else{
                        $(".pagination .next_page").removeClass("disabled").data('page',data.page-0+1);   
                    }
                    $(".pagination .show_page a").text('共'+data.count+'页');
                    $(".pagination .curr_page a").text('第'+(data.page-0+1)+'页');
                });
            },
            tpl : function(){
                var tpl=[
                    '{@each problem as pro}',
                    '   <tr data-id="${pro.id}">',
                    '       <td>${pro.id}</td>',
                    '       <td>${pro.content}</td>',
                    '       <td>${pro.uid}</td>',
                    '       <td>${pro.qqno}</td>',
                    '       <td class="is_problem_deal">{@if(pro.is_deal==0)}未处理{@else}已处理{@/if}</td>',
                    '       <td class="_problem_deal">{@if(pro.is_deal==0)}<button style="margin-right:10px;" class="btn btn-warning btn-sm problem_deal">处理</button>{@/if}</td>',
                    '   </tr>',
                    '{@/each}'
                ].join('\n');
                Problem.compiledTpl = juicer(tpl);
            },
            problemDeal : function(node){
            	var issigned = node.attr('data-issigned');
                $.post('/problem/is_deal', {id: node.data('id')}, function(data) {
                    if($.trim(data) == 'success'){
                    	node.find('.is_problem_deal').html('已处理');
                    	node.find('._problem_deal').html('');
                    	
                    	$(".content").before('<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>操作成功</div>')
                        setTimeout(function(){
                            $(".alert").remove();
                        },3000);
                    }else{
                    	$(".content").before('<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>操作失败</div>')
                        setTimeout(function(){
                            $(".alert").remove();
                        },3000);
                    }
                });
            },    
        }
        
        
        $(function(){
            Problem.getProblem();

            //签约写手
            $(".problem_deal").live("click",function(){
            	Problem.problemDeal($(this).parents('tr'));
            })
            
            $(".next_page,.prev_page").live("click",function(){
                Problem.getProblem($(this).data('page'));
            })
            
            //搜索
            $('.searchkey').live('click',function(){
            	Problem.getProblem();
            });
        })
    </script>
    <style>
        .add_user_div .div_20{
            margin-top: 20px;
        }
        .radio span{
            display: inline-block;
            
        }
        .add_user_div div.radio{
            margin: 0;
        }
        .add_user_div input,.radio,.add_user_div select{
            width: 300px;
            float: left;
        }
        .radio input{
            width: 20px;
        }
        .ml20{margin-left:20px;}
    </style>
</aside>