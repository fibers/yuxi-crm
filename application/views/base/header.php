
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>IM 管理后台</title>
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>
<link href="/ui/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="/ui/css/main.css" rel="stylesheet" type="text/css" />
<link href="/ui/css/font-awesome.min.css" rel="stylesheet"
	type="text/css" />
<link href="/ui/css/ionicons.min.css" rel="stylesheet" type="text/css" />
<link href="/ui/css/simplemodal.css" rel="stylesheet" type="text/css" />
<script src="/ui/js/jquery-1.11.1.min.js"></script>
<script src="/ui/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/ui/js/juicer.js" type="text/javascript"></script>
<script src="/ui/js/simple-modal.js" type="text/javascript"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
</head>
<body class="skin-blue">
	<header class="header">
		<a href="/" class="logo"> IM </a>
		<!-- Header Navbar: style can be found in header.less -->
		<nav class="navbar navbar-static-top" role="navigation">
			<!-- Sidebar toggle button-->
			<!-- <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas"
				role="button" id="drop"> <span class="sr-only">Toggle navigation</span> <span
				class="icon-bar menu_icon"></span> <span class="icon-bar"></span> <span
				class="icon-bar"></span>
			</a> -->
			<div class="navbar-right">
				<ul class="nav navbar-nav">
					<li class="dropdown"><a id="drop" href="#" role="button"
						class="dropdown-toggle" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false">Menu <b class="caret"></b></a>
						<ul class="dropdown-menu" aria-labelledby="drop">
							<?php $rolecode=$this->session->userdata['rolecode'];?>
							<?php if($rolecode==='all'):?>
								<li><a href="/admin"> <span>管理员管理</span></a></li>
							<?php endif;?>
							<?php
							$navList = array (
									'1' => array ('name' => '用户管理','module_name' => 'user','action_name' => '' ),
									'2' => array ('name' => '组织架构','module_name' => 'depart','action_name' => ''),
									'3' => array ('name' => '群组管理','module_name' => 'group','action_name' => '' ),
									'4' => array ('name' => '系统设置','module_name' => 'discovery','action_name' => ''),
									'5' => array ('name' => '剧管理','module_name' => 'story','action_name' => '' ),
									'6' => array ('name' => '自戏管理','module_name' => 'story','action_name' => 'indexSelf'),
									'7' => array ('name' => '话题管理','module_name' => 'topic','action_name' => '' ),
									'8' => array ('name' => '广告管理','module_name' => 'banners','action_name' => ''),
									'9' => array ('name' => '新增用户数统计','module_name' => 'newuser','action_name' => '' ),
									'10' => array ('name' => '今日在线统计','module_name' => 'online','action_name' => ''),
									'11' => array ('name' => '总数据统计','module_name' => 'http://123.57.136.1:10001/GetTable','action_name' => '' ),
									'12' => array ('name' => '版本管理','module_name' => 'packageinfo','action_name' => ''),
									'13' => array ('name' => '用户活跃度统计','module_name' => 'kibana','action_name' => '' ),
									'14' => array ('name' => '新增剧统计','module_name' => 'kibana','action_name' => 'newStory'),
									'15' => array ('name' => '新增家族统计','module_name' => 'kibana','action_name' => 'newFamilies' ),
									'19' => array ('name' => '新增自戏统计','module_name' => 'kibana','action_name' => 'newSelf'),
									'20' => array ('name' => '用户消息反馈','module_name' => 'problem','action_name' => ''),
									'21' => array ('name' => '修改密码','module_name' => 'auth','action_name' => 'auth_pwd'),
									'22' => array ('name' => '二维码管理','module_name' => 'qr_code','action_name' => ''),
									);

									$rolecode = $this->session->userdata['rolecode'];
									if($rolecode==='all') $rolecode='1,2,3,4,5,6,7,8,9,10,11,12,13,14,19,15,20,21,22';
									$rolecode = explode(',', $rolecode);
									$html = '';
									foreach ($rolecode as $v){
										if(isset($navList[$v])){
											if ( ! preg_match('#^https?://#i', $navList[$v]['module_name'])){
												$uri = site_url($navList[$v]['module_name']);
												if($navList[$v]['action_name']){
													$uri = $uri.'/'.$navList[$v]['action_name'];}
													
													$html .= "<li><a href='".$uri."'> <span>".$navList[$v]['name']."</span></a></li>";
											}else{
												$uri = $navList[$v]['module_name'];
												$html .= "<li><a href='".$uri."' target='_blank'> <span>".$navList[$v]['name']."</span></a></li>";
											}
										}
									}
									echo $html;
							?>
						</ul></li>
					<li class="dropdown user user-menu"><a href="/auth/logout"> <span>退出</span></a></li>
				</ul>
			</div>
		</nav>
	</header>
	<div class="wrapper row-offcanvas row-offcanvas-left">
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="left-side sidebar-offcanvas">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu">
					<?php $rolecode=$this->session->userdata['rolecode'];?>
					<?php if($rolecode==='all'):?>
						<li><a href="/admin"> <span>管理员管理</span></a></li>
					<?php endif;?>
					<?php echo $html;?>

					<!-- 
					<li><a href="/admin"> <span>管理员管理</span></a></li>
					<li><a href="/user"> <span>用户管理</span></a></li>
					<li><a href="/depart"> <span>组织架构</span></a></li>
					<li><a href="/group"> <span>群组管理</span></a></li>
					<li><a href="/discovery"> <span>系统设置</span></a></li>
					<li><a href="/story?self=0"> <span>剧管理</span></a></li>
					<li><a href="/story?self=1"> <span>自戏管理</span></a></li>
					<li><a href="/topic"> <span>话题管理</span></a></li>
					<li><a href="/banners"> <span>广告管理</span></a></li>
					<li><a href="/newuser"> <span>新增用户数统计</span></a></li>
					<li><a href="/online"> <span>今日在线统计</span></a></li>
					<li><a href="http://123.57.136.1:10001/GetTable" target="_blank"> <span>总数据统计</span></a></li>
					<li><a href="/packageinfo"> <span>版本管理</span></a></li>
					<li><a href="/kibana"> <span>用户活跃度统计</span></a></li>
					<li><a href="/kibana/newStory"> <span>新增剧统计</span></a></li>
					<li><a href="/kibana/newFamilies"> <span>新增家族统计</span></a></li> -->
				</ul>
			</section>
		</aside>