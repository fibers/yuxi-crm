<aside class="right-side">
    <section class="content-header">
        <h1>
            版本管理
            <small>欢迎来到IM</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> 首页</a></li>
            <li class="active">版本管理</li>
        </ol>
    </section>
    <section class="content">
    	<input id="keyID" placeholder="ID" value="" type="text" />
        <button style="margin-bottom:6px;" class="btn btn-primary btn-sm searchkey pull-center">搜索</button>
        <!-- <button style="margin-bottom:10px;" class="btn btn-primary btn-sm add_user pull-right">新增</button> -->
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>安卓大版本号</th>
                    <th>安卓版本号</th>
                    <th>苹果版本号</th>
                    <th>安卓下载链接</th>
                    <th>苹果下载链接</th>
                    <th>编辑</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <ul class="pagination">
            <li class="prev_page">
                <a href="javascript:;">上一页</a>
            </li>
            <li class="curr_page">
              	 <a href="javascript:;"></a>
            </li>
            <li class="show_page">
                <a href="javascript:;"></a>
            </li>
            <li class="next_page">
                <a href="javascript:;">下一页</a>
            </li>
        </ul>
    </section>
    <script>
        var Packageinfo = {
            compiledTpl : null,
            compiledAddTpl : null,
            getPackageinfo : function(page){
                if(!page){
                    page = 0;
                }
                var keyID = $('#keyID').val();
                
                if(!keyID){
                	keyID='';
				}
                
                $.getJSON('/packageinfo/all', {
                    start : page,keyID: keyID
                }, function(data) {
                    Packageinfo.tpl();
                    var _tpl = Packageinfo.compiledTpl.render(data);
                    $("tbody").html(_tpl);
                    if(data.page == 0){
                        $(".pagination .prev_page").addClass("disabled");   
                    }else{
                        $(".pagination .prev_page").removeClass("disabled").data('page',data.page-0-1);   
                    }
                    if(data.page == (data.count-1)){
                        $(".pagination .next_page").addClass("disabled");   
                    }else{
                        $(".pagination .next_page").removeClass("disabled").data('page',data.page-0+1);   
                    }
                    $(".pagination .show_page a").text('共'+data.count+'页');
                    $(".pagination .curr_page a").text('第'+(data.page-0+1)+'页');
                });
            },
            delPackageinfo : function(node){
                $.post('/packageinfo/del', {id: node.data('id')}, function(data) {
                    if($.trim(data) == 'success'){
                        node.fadeOut();
                    }
                });
            },
            editPackageinfo : function(){
                $.post('/packageinfo/edit', {
                    id:$(".btn-margin-edit").data('id'),
                    versioncode: $(".versioncode").val(),
        			versionnumber_android: $(".versionnumber_android").val(),
        			versionnumber_ios: $(".versionnumber_ios").val(),
        			//producer: $(".producer").val(),
        			downurl_android: $(".downurl_android").val(),
        			downurl_ios: $(".downurl_ios").val(),
        			desc_android: $(".desc_android").val(),
        			desc_ios: $(".desc_ios").val(),
        			mustflag_android: $(".mustflag_android:checked").val(),
        			mustflag_ios: $(".mustflag_ios:checked").val()
                }, function(data) {
                    if($.trim(data) == 'success'){
                        $.fn.hideModal();
                        $(".content").before('<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>编辑成功</div>')
                        setTimeout(function(){
                            $(".alert").remove();
                        },3000);
                        Packageinfo.getPackageinfo();
                    }else{
                        $(".btn-margin-edit").text('编辑失败');
                    }
                });
            },
            addPackageinfo : function(){
                $.post('/packageinfo/add', {
        			versioncode: $(".versioncode").val(),
        			versionnumber_android: $(".versionnumber_android").val(),
        			versionnumber_ios: $(".versionnumber_ios").val(),
        			//producer: $(".producer").val(),
        			downurl_android: $(".downurl_android").val(),
        			downurl_ios: $(".downurl_ios").val(),
        			desc_android: $(".desc_android").val(),
        			desc_ios: $(".desc_ios").val(),
        			mustflag_android: $(".mustflag_android:checked").val(),
        			mustflag_ios: $(".mustflag_ios:checked").val()
                }, function(data) {
                    if($.trim(data) == 'success'){
                        $.fn.hideModal();
                        $(".content").before('<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>添加成功</div>')
                        setTimeout(function(){
                            $(".alert").remove();
                        },3000);
                        Packageinfo.getPackageinfo();
                    }else{
                        $(".btn-margin").text('添加失败');
                    }
                });
            },
            editPackageinfoAlert : function(node){
                Packageinfo.addTpl();
                $.fn.SimpleModal({
                    btn_ok: '编辑',
                    model: 'confirm',
                    callback: function(node){
                        Packageinfo.editPackageinfo();
                    },
                    overlayClick: false,
                    width: 660,
                    title: '编辑版本',
                    contents: Packageinfo.compiledAddTpl.render()
                }).showModal();
                $.post('/packageinfo/get', {
                    id:node.data('id')
                }, function(data) {
                    var data = JSON.parse(data);
                    $(".versioncode").val(data.versioncode);
                    $(".versionnumber_android").val(data.versionnumber_android);
                    $(".versionnumber_ios").val(data.versionnumber_ios);
                    //$(".producer").val(data.producer);
                    $(".downurl_android").val(data.downurl_android);
                    $(".downurl_ios").val(data.downurl_ios);
                    $(".desc_android").val(data.desc_android);
                    $(".desc_ios").val(data.desc_ios);
                    $("#mustflag_android"+data.mustflag_android).attr("checked","checked");
                    $("#mustflag_ios"+data.mustflag_ios).attr("checked","checked");
                    $(".btn-margin").addClass("btn-margin-edit");
                    $(".btn-margin-edit").data('id',node.data('id'));
                });
                
            },
            addPackageinfoAlert : function(){
                Packageinfo.addTpl();
                $.fn.SimpleModal({
                    btn_ok: '添加',
                    model: 'confirm',
                    callback: function(node){
                        Packageinfo.addPackageinfo();
                    },
                    overlayClick: false,
                    width: 660,
                    title: '添加版本',
                    contents: Packageinfo.compiledAddTpl.render()
                }).showModal();
            },
            addTpl : function(){
                var tpl =[
                    '<div class="add_user_div" role="form">',
                    '    <div class="clearfix div_20">',
                    '		<div><span class="div_span">安卓大版本号:</span><span style="margin-left:20px;" class="div_span">安卓版本号:</span></div>',
                    '        <div><input type="text" class="form-control input-sm versioncode pull-left" placeholder="安卓大版本号">',
                    '        <input type="text" style="margin-left:20px;" class="form-control input-sm versionnumber_android" placeholder="安卓版本号"></div>',
                    '    </div>',
                    '    <div class="clearfix div_20">',
                    '		<div><span class="div_span" style="width:400px;" >苹果版本号:</span></div>',
                    '       <div><input type="text" class="form-control input-sm versionnumber_ios" placeholder="苹果版本号"></div>',
                   // '        <input type="text" style="margin-left:20px;" class="form-control input-sm producer" placeholder="渠道商名字">',
                    '    </div>',
                    '    <div class="clearfix div_20">',
                    '		<div><span class="div_span">安卓下载地址:</span><span style="margin-left:20px;" class="div_span">苹果下载地址:</span></div>',
                    '        <input type="text" class="form-control input-sm downurl_android" placeholder="安卓下载地址">',
                    '        <input type="text" style="margin-left:20px;" class="form-control input-sm downurl_ios" placeholder="苹果下载地址">',
                    '    </div>',
                    '    <div class="clearfix div_20">',
                    '		<div><span class="div_span"  >安卓升级信息描述:</span></div>',
                    '		 <textarea type="text" class="form-control input-sm desc_android" placeholder="安卓升级信息描述"></textarea>',
                    '    </div>',
                    '    <div class="clearfix div_20">',
                    '		<div><span class="div_span"  >苹果升级信息描述:</span></div>',
                    '		 <textarea type="text" class="form-control input-sm desc_ios" placeholder="苹果升级信息描述"></textarea>',
                    '    </div>',
                    '    <div class="clearfix div_20">',
                    '        <div class="radio">',
                    '            <span class="text">安卓是否必须更新:</span>',
                    '            <label>',
                    '                <input type="radio" id="mustflag_android1" class="mustflag_android" name="mustflag_android[]" value="1">是',
                    '            </label>',
                    '            <label>',
                    '                <input type="radio" id="mustflag_android0" class="mustflag_android" name="mustflag_android[]" value="0" checked="">否',
                    '            </label>',
                    '        </div>',
                    '        <div class="radio">',
                    '            <span class="text">苹果是否必须更新:</span>',
                    '            <label>',
                    '                <input type="radio" id="mustflag_ios1" class="mustflag_ios" name="mustflag_ios[]" value="1">是',
                    '            </label>',
                    '            <label>',
                    '                <input type="radio" id="mustflag_ios0" class="mustflag_ios" name="mustflag_ios[]" value="0" checked="">否',
                    '            </label>',
                    '        </div>',
                    '    </div>',
                    '</div>'
                ].join('\n');
                Packageinfo.compiledAddTpl = juicer(tpl);
            },
            tpl : function(){
                var tpl=[
                    '{@each infos as info}',
                    '   <tr data-id="${info.id}">',
                    '       <td>${info.id}</td>',
                    '       <td>${info.versioncode}</td>',
                    '       <td>${info.versionnumber_android}</td>',
                    '       <td>${info.versionnumber_ios}</td>',
                    '       <td>${info.downurl_android}</td>',
                    '       <td>${info.downurl_ios}</td>',
                    '       <td><button style="margin-right:10px;" class="btn btn-warning btn-sm edit_user">编辑</button><button href="javascript:;" class="btn btn-danger btn-sm del_user">删除</button></td>',
                    '   </tr>',
                    '{@/each}'
                ].join('\n');
                Packageinfo.compiledTpl = juicer(tpl);
            }
        }
        $('.searchkey').live('click',function(){
        	Packageinfo.getPackageinfo();
        });
        
        $(function(){

            Packageinfo.getPackageinfo();

            $(".next_page,.prev_page").live("click",function(){
                Packageinfo.getPackageinfo($(this).data('page'));
            })

            $(".del_user").live("click",function(){
                Packageinfo.delPackageinfo($(this).parents('tr'));
            })
            
            $(".add_user").click(function(){
                Packageinfo.addPackageinfoAlert();
            })

            $(".edit_user").live("click",function(){
                Packageinfo.editPackageinfoAlert($(this).parents('tr'));
            })
        })
    </script>
    <style>
        .add_user_div .div_20{
            margin-top: 20px;
        }
        .radio span{
            display: inline-block;
        }
        .add_user_div div.radio{
            margin: 0;
        }
        .add_user_div input,.radio,.add_user_div select{
            width: 300px;
            float: left;
        }
        .radio input{
            width: 20px;
        }
        .div_span{width: 300px; float: left;display: inline-block;}
    </style>
</aside>