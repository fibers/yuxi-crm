<aside class="right-side">
    <section class="content-header">
        <h1>
            <?php if($self==1):?>自戏<?php else:?>剧<?php endif;?>管理
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> 首页</a></li>
            <li class="active">剧管理</li>
        </ol>
    </section>
    <section class="content">
       <!-- <button style="margin-bottom:10px;" class="btn btn-primary btn-sm add_discovery pull-right">新增</button>-->
       <table class="table table-bordered">
            <thead>
                <tr>
                    <th  style="border-bottom-width:0px;">
                    <input type="text" id="title" name="title" value=""/>
                    
	                    <select id="keycategory" name="keycategory" style="height:26px;">
				    		<option value="">--标签--</option>
				    		<?php if(isset($cate_list)):?>
				    		<?php foreach($cate_list as $k=>$v):?>
				    			<option value="<?php echo $v['id'];?>"><?php echo $v['name'];?></option>
				    		<?php endforeach;?>
				    		<?php endif;?>
				    	</select>
    				
                    <button style="margin-left:20px;" class="btn btn-primary btn-sm search_story">搜索</button>
                   	<button style="margin-left:20px;" class="btn btn-primary btn-sm odword_story"><?php if($self==0):?>剧<?php else:?>自戏<?php endif;?>字数排序</button>
                   	<a href="/story/export" style="margin-left:20px;" class="btn btn-primary btn-sm pull-right">导出昨日剧</a>
                    </th>
                </tr>
                
            </thead>
        </table>
        <table class="table table-bordered order_type">
            <thead>
                <tr>
                    <th><a class="idclassbtn" style="cursor:pointer;">剧ID</a></th>
                    <th>标题</th>
                    <th>创建者</th>
                    <th>图片</th>
                    <th>简介</th>
                    <th>版本号</th>
                    <th>种类</th>
                    <th>内容</th>
                    <th>总字数</th>
                    <th><a class="praisenumbtn" style="cursor:pointer;">点赞数</a></th>
                    <th><a class="odclassbtn" style="cursor:pointer;">排序号</a></th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <input type="hidden" id="od_name" value="" />
        <input type="hidden" id="od_type" value="" />
        <ul class="pagination all_list">
            <li class="prev_page">
                <a href="javascript:;">上一页</a>
            </li>
            <li class="curr_page">
              	 <a href="javascript:;"></a>
            </li>
            <li class="show_page">
                <a href="javascript:;"></a>
            </li>
            <li class="next_page">
                <a href="javascript:;">下一页</a>
            </li>
            <li class="jump_page">
                <span><input type="text" value="" class="width20 go_page_input" /></span>
                <span class="go_page cursor">go</span>
            </li>
        </ul>
    </section>
     <script>
        var Story = {
            compiledTpl : null,
            compiledAddTpl : null,
            getStory : function(page,od,od_type){
                if(!page || page<0){
                    page = 0;
                }
                if(!od){
                	od = $('#od_name').val();
                }
                /*搜索关键字*/
                var title = $('#title').val();
				if(!title){
					title='';
				}
				var category = $('#keycategory').val();
				
                $.getJSON('/story/all', {
                    self:<?php echo $self?$self:'0';?>,
                    start : page,
                    od:od,
					title : title,
					category:category
                }, function(data) {
                    Story.tpl();
                    var _tpl = Story.compiledTpl.render(data);
                    $("tbody").html(_tpl);
					$('#title').val(data.title);
					$('#od_name').val(data.od);
					$('#od_type').val(data.od_type);
					$(".pagination").addClass("all_list").removeClass("od_list");   
                    if(data.page == 0){
                        $(".all_list .prev_page").addClass("disabled");   
                    }else{
                        $(".all_list .prev_page").removeClass("disabled").data('page',data.page-0-1);   
                    }
                    if(data.count==0 || data.page == (data.count-1)){
                        $(".all_list .next_page").addClass("disabled");   
                    }else{
                        $(".all_list .next_page").removeClass("disabled").data('page',data.page-0+1);   
                    }
                    $(".all_list .show_page a").text('共'+data.count+'页');
                    $(".all_list .curr_page a").text('第'+(data.page-0+1)+'页');
                    $(".all_list .curr_page").data('page',data.page);
                });
            },
			tpl : function(){
                var tpl=[
                    '{@each story as s}',
                    '   <tr data-id="${s.id}" data-permission="${s.permission}">',
                    '       <td>${s.id}</td>',
                    '       <td>${s.title}</td>',
                    '       <td>${s.creatorname}</td>',
                    '       <td>{@if s.portrait}<img style="height:30px;" src="${s.portrait}!w60">{@/if}</td>',
                    '       <td class="word-break">${s.intro}</td>',
                    '       <td>${s.version}</td>',
                    '       <td>${s.category_name}</td>',
                    '       <td class="word-break">${s.content}</td>',
                    '       <td>${s.text_cnt}</td>',
                    '       <td>${s.praisenum}</td>',
                    '       <td>${s.priority}</td>',
                    '       <td><button style="margin-right:10px;" class="btn btn-warning btn-sm del_Story">删除</button><button style="margin-right:10px;" class="btn btn-warning btn-sm edit_story">编辑</button>',
                    '		<button style="margin-right:10px;" class="btn btn-warning btn-sm close_story">{@if s.permission==1}打开{@else}禁用{@/if}阅读</button></td>',
                    '   </tr>',
                    '{@/each}'
                ].join('\n');
                Story.compiledTpl = juicer(tpl);
            },
            odwordStory : function(page){
                if(!page || page<0){
                    page = 0;
                }

                /*搜索关键字*/
                var title = $('#title').val();
				if(!title){
					title='';
				}
				var category = $('#keycategory').val();
				
				<?php if($self==0):?>
					var url='/story/odword';
				<?php else:?>
					var url='/story/odword_self';
				<?php endif;?>
                $.getJSON(url, {
                    start : page,title:title,category:category
                   
                }, function(data) {
                    Story.tpl();
                    var _tpl = Story.compiledTpl.render(data);
                    $("tbody").html(_tpl);
                    $('#od_name').val('');
                    $(".pagination").addClass("od_list").removeClass("all_list");   
                    if(data.page == 0){
                        $(".od_list .prev_page").addClass("disabled");   
                    }else{
                        $(".od_list .prev_page").removeClass("disabled").data('page',data.page-0-1);   
                    }
                    if(data.count==0 || data.page == (data.count-1)){
                        $(".od_list .next_page").addClass("disabled");   
                    }else{
                        $(".od_list .next_page").removeClass("disabled").data('page',data.page-0+1);   
                    }
                    $(".od_list .show_page a").text('共'+data.count+'页');
                    $(".od_list .curr_page a").text('第'+(data.page-0+1)+'页');
                    $(".od_list .curr_page").data('page',data.page);
                });
            },
            editStoryAlert : function(node){
                Story.addTpl();
                $.fn.SimpleModal({
                    btn_ok: '编辑',
                    model: 'confirm',
                    callback: function(node){
                        Story.editStory();
                    },
                    overlayClick: false,
                    width: 660,
                    title: '编辑排名顺序',
                    contents: Story.compiledAddTpl.render({'departs':$("table").data('departs')})
                }).showModal();
                $.post('/story/get', {
                    id:node.data('id')
                }, function(data) {
                    var data = JSON.parse(data);
                    $(".priority").val(data.priority);
                   
                    $(".btn-margin").addClass("btn-margin-edit");
                    $(".btn-margin-edit").data('id',node.data('id'));

                    // $('.avatar_show').Jcrop({
                    //     setSelect:   [ 0, 0, 100, 100 ],
                    //     aspectRatio: 1/1,
                    //     onChange: function(c){
                    //         $(".crop_info").val(JSON.stringify(c));
                    //     }
                    // });
                });
                
            },
            editStory : function(){
                $.post('/story/edit', {
                    id:$(".btn-margin-edit").data('id'),
                    priority: $(".priority").val()
                }, function(data) {
                    if($.trim(data) == 'success'){
                        $.fn.hideModal();
                        $(".add_user").before('<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>编辑成功</div>')
                        setTimeout(function(){
                            $(".alert").remove();
                        },3000);
                        var page=$(".curr_page").data('page');

                        var od_name = $('#od_name').val();
                        if(!od_name){
                        	Story.odwordStory(page);
                        }else{
                        	Story.getStory(page);
                        }
                    }else{
                        $(".btn-margin-edit").text('编辑失败');
                    }
                });
            },
             addTpl : function(){
                var tpl =[
                    '<div class="add_user_div" role="form">',
                    '    <div class="clearfix div_20">',
                    '        <input type="text" class="form-control input-sm priority pull-left" placeholder="排序号">',                  
                    '    </div>',
                    '   <div class="hide crop_info"></div>',
                    // '    <div class="clearfix">',
                    // '        <img src="" style="height:100px;margin-left:20px;" class="avatar_show hide">',
                    '        <img src=""  class="avatar_show hide" style="max-width:100px;">',
                    // '       <a href="javascript:;" class="start_upload">上传</a>',
                    // '    </div>',
                    '</div>'
                ].join('\n');
                Story.compiledAddTpl = juicer(tpl);
            },
            delStory : function(node){
                $.post('/story/del', {id: node.data('id')}, function(data) {
                    if($.trim(data) == 'success'){
                        node.fadeOut();
                    }
                });
            },
            closeStory : function(node){
            	var permission = node.attr('data-permission');
                $.post('/story/permission', {id: node.data('id'),permission:permission}, function(data) {
                    if($.trim(data) == 'success'){
                        //node.fadeOut();
                    	var close_story = node.find('.close_story').html();
                        if(permission=='1'){
                    		node.attr('data-permission','0');
                    		node.find('.close_story').html('禁用阅读');
                    	}else{
                    		node.attr('data-permission','1');
                    		node.find('.close_story').html('打开阅读');
                    	}
                        
	                    $(".content").before('<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+close_story+'成功</div>')
	                    setTimeout(function(){
	                        $(".alert").remove();
	                    },3000);
	                }else{
	                	$(".content").before('<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>操作失败</div>')
	                    setTimeout(function(){
	                        $(".alert").remove();
	                    },3000);
	                }
                });
            },
//			chktopStory : function(){
//                $.post('/Story/chktop', {id:''}, function(data) {
//                    if($.trim(data) == 'success'){
//                        node.fadeOut();
//                    }
//                });
//            },
			/*topStory : function(node){
                $.post('/story/top', {id:node.data('id'),rel:$(node).find('.top_Story').attr('rel')}, function(data) {
                    if($.trim(data) == 'success'){
						if($(node).find('.top_Story').attr('rel')>0){
                        	$(node).find('.top_Story').html('置顶').attr('rel',0);
						}else{
							$(node).find('.top_Story').html('取消置顶').attr('rel',1);
						}
						return false;
                    }
					if($.trim(data) == 'overtop'){
                       alert('剧最多只能置顶3个，请您取消已置顶剧，或取消此操作');
					   return false;
                    }
                });
            },*/
        }
        $(function(){

            Story.getStory();

            $(".all_list .go_page").click(function(){ 
                var page=$(".pagination .go_page_input").val();
                var od_name = $('#od_name').val();
                if(!od_name){
                	Story.odwordStory(page-1);
                }else{
                	Story.getStory(page-1);
                }
            })

            $(".all_list .next_page,.all_list .prev_page").live("click",function(){
				var li=$(this).closest('li');
				if(!$(this).hasClass("disabled")){
                	Story.getStory($(this).data('page'));
				}
            })
            
            /*剧字数排序*/
            $(".od_list .go_page").click(function(){
                var page=$(".pagination .go_page_input").val();
                Story.odwordStory(page-1);
            })

            $(".od_list .next_page,.od_list .prev_page").live("click",function(){
				var li=$(this).closest('li');
				if(!$(this).hasClass("disabled")){
                	Story.odwordStory($(this).data('page'));
				}
            })
            
            $(".odword_story").live("click",function(){
                Story.odwordStory(0);
            })
            
			$(".search_story").live("click",function(){
                Story.getStory(0);
            })

            $(".del_Story").live("click",function(){
                if(confirm('确认要删除？')){
                    Story.delStory($(this).parents('tr'));
                }
            })
			
	  		/*$(".top_Story").live("click",function(){
                Story.topStory($(this).parents('tr'));
            })*/
            
            $(".close_story").live("click",function(){
            	Story.closeStory($(this).parents('tr'));
            })
            
            $(".edit_story").live("click",function(){
                Story.editStoryAlert($(this).parents('tr'));
            })
            
            /*收藏排序*/
            $(".praisenumbtn").live("click",function(){
            	Story.getStory(0,'praisenum');
			});

            $(".odclassbtn").live("click",function(){
            	Story.getStory(0,'priority');
			});
			
			$(".idclassbtn").live("click",function(){
				Story.getStory(0,'id');
			});
        })
    </script>
    <style>
        .add_discovery_div input{ 
            margin-top: 20px;
        }
        .word-break{word-break:break-all}
		.width20{width:40px;height:20px;}
        .cursor{cursor: pointer;}
    </style>
</aside>    