<aside class="right-side">
    <section class="content-header">
        <h1>
           二维码管理
            <small>欢迎来到IM</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> 首页</a></li>
            <li class="active">二维码管理管理</li>
        </ol>
    </section>
    <section class="content">
    	<input id="keywords" placeholder="关键词" value="" type="text" />
    	
        <button style="margin-bottom:6px;" class="btn btn-primary btn-sm searchkey pull-center">搜索</button>
        <button style="margin-bottom:6px;" class="btn btn-primary btn-sm add_qrcode pull-right">添加</button>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>项目名称</th>
                    <th>形式</th>
                    <th>关键字</th>
                    <th>网址</th>
                    <th>扫描次数</th>
                    <th width="8%">二维码</th>
                    <th>编辑</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <ul class="pagination">
            <li class="prev_page">
                <a href="javascript:;">上一页</a>
            </li>
            <li class="curr_page">
              	 <a href="javascript:;"></a>
            </li>
            <li class="show_page">
                <a href="javascript:;"></a>
            </li>
            <li class="next_page">
                <a href="javascript:;">下一页</a>
            </li>
        </ul>
    </section>
    <script>
        var Qrcode = {
            compiledTpl : null,
            compiledAddTpl : null,
            compiledSignTpl : null,
            compiledallWords : null,
            getQrcode : function(page){
                if(!page){
                    page = 0;
                }

                var keywords = $('#keywords').val();
               
                $.getJSON('/qr_code/all', {
                    start : page,keywords: keywords
                }, function(data) {
                    Qrcode.tpl();
                    var _tpl = Qrcode.compiledTpl.render(data);
                
                    $("tbody").html(_tpl);
                    if(data.page == 0){
                        $(".pagination .prev_page").addClass("disabled");   
                    }else{
                        $(".pagination .prev_page").removeClass("disabled").data('page',data.page-0-1);   
                    }
                    if(data.page == (data.count-1)){
                        $(".pagination .next_page").addClass("disabled");   
                    }else{
                        $(".pagination .next_page").removeClass("disabled").data('page',data.page-0+1);   
                    }
                    $(".pagination .show_page a").text('共'+data.count+'页');
                    $(".pagination .curr_page a").text('第'+(data.page-0+1)+'页');
                });
            },
            tpl : function(){
                var tpl=[
                    '{@each qrcode as pro}',
                    '   <tr data-id="${pro.id}">',
                    '       <td>${pro.id}</td>',
                    '       <td>${pro.item}</td>',
                    '       <td>${pro.form}</td>',
                    '       <td>${pro.keywords}</td>',
                    '       <td>${pro.url}</td>',
                    '       <td>${pro.nums}</td>',
                    '       <td><a href="/qr_code/downjpg?file=${pro.code}">下载</a></td>',
                    '       <td><button class="btn btn-warning btn-sm del_qrcode">删除</button></td>',
                    '   </tr>',
                    '{@/each}'
                ].join('\n');
                Qrcode.compiledTpl = juicer(tpl);
            },
            addQrcodeAlert : function(){
                Qrcode.addTpl();
                $.fn.SimpleModal({
                    btn_ok: '添加',
                    model: 'confirm',
                    callback: function(node){
                        Qrcode.addQrcode();
                    },
                    overlayClick: false,
                    width: 660,
                    title: '添加二维码',
                    contents: Qrcode.compiledAddTpl.render()
                }).showModal();
            },
            addTpl : function(){
                var tpl =[
                    '<div class="add_user_div" role="form">',
                    '    <div class="clearfix div_20">',
                    '		<div><span class="div_span">项目名称:</span><span style="margin-left:20px;" class="div_span">形式:</span></div>',
                    '        <div><input type="text" class="form-control input-sm qrcode_item pull-left" placeholder="项目名称">',
                    '        <input type="text" style="margin-left:20px;" class="form-control input-sm qrcode_form" placeholder="形式"></div>',
                    '    </div>',
                    '    <div class="clearfix div_20">',
                    '		<div><span class="div_span" style="width:400px;" >关键词（唯一标识该二维码字母）:</span></div>',
                    '       <div><input type="text" class="form-control input-sm qrcode_keywords" placeholder="关键字"></div>',
                   // '        <input type="text" style="margin-left:20px;" class="form-control input-sm producer" placeholder="渠道商名字">',
                    '    </div>',
                    '</div>'
                ].join('\n');
                Qrcode.compiledAddTpl = juicer(tpl);
            },
            addQrcode : function(){
            	var qrcode_keywords=$(".qrcode_keywords").val();
                if(qrcode_keywords=='' || qrcode_keywords==null){
					alert('关键词不能为空');
					return false;
                }
                
                $.post('/qr_code/add', {
                	keywords:qrcode_keywords,
                    item: $(".qrcode_item").val(),
                    form: $(".qrcode_form").val()
                }, function(data) {
                    if($.trim(data) == 'success'){
                        $.fn.hideModal();
                        $(".content").before('<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>添加成功</div>')
                        setTimeout(function(){
                            $(".alert").remove();
                        },3000);
                        Qrcode.getQrcode();
                    }else{
                        $(".btn-margin").text($.trim(data));
                    }
                });
            },
            delQrcode : function(node){
            	if(confirm('确认要删除吗？')){
	                $.post('/qr_code/del', {id: node.data('id')}, function(data) {
	                    if($.trim(data) == 'success'){
	                        node.fadeOut();
	                    }
	                });
            	}
            },
        }
        
        
        $(function(){
            Qrcode.getQrcode();
            
            $(".next_page,.prev_page").live("click",function(){
                Qrcode.getQrcode($(this).data('page'));
            })
            
            //搜索
            $('.searchkey').live('click',function(){
            	Qrcode.getQrcode();
            });

            $(".del_qrcode").live("click",function(){
            	Qrcode.delQrcode($(this).parents('tr'));
            })

            $(".add_qrcode").click(function(){
            	Qrcode.addQrcodeAlert();
            })

            $(".edit_user").live("click",function(){
            	Qrcode.editQrcodeAlert($(this).parents('tr'));
            })
        })
    </script>
    <style>
        .add_user_div .div_20{
            margin-top: 20px;
        }
        .radio span{
            display: inline-block;
            
        }
        .add_user_div div.radio{
            margin: 0;
        }
        .add_user_div input,.radio,.add_user_div select{
            width: 300px;
            float: left;
        }
        .radio input{
            width: 20px;
        }
        .ml20{margin-left:20px;}
		.div_span{width: 300px; float: left;display: inline-block;}
    </style>
</aside>