<aside class="right-side">
    <section class="content-header">
        <h1>
           禁用字查询
            <small>欢迎来到IM</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> 首页</a></li>
            <li class="active">禁用字查询</li>
        </ol>
    </section>
    <section class="content">
    	<select id="keytype" name="keytype" style="height:26px;">
    		<option value="2">标题</option>
    		<option value="1">简介</option>
    	</select><br/><br/>
    	<textarea rows="20" cols="30" id="keycontent"></textarea><br/><br/>
    	<div class="content_w"></div><br/><br/>
        <button style="margin-bottom:6px;" class="btn btn-primary btn-sm searchkey pull-center">验证</button>
        
    </section>
    <script>
        var Problem = {
            compiledTpl : null,
            compiledAddTpl : null,
            compiledSignTpl : null,
            compiledallWords : null,
            getShield : function(){
                var type = $('#keytype').val();
                var content = $('#keycontent').val()
               
                $.getJSON('/problem/shield_word', {
                	type : type,content: content
                }, function(data) {
                    $(".content_w").html(data.new_content);
                });
            }
        }
        
        
        $(function(){
            
            //搜索
            $('.searchkey').live('click',function(){
            	Problem.getShield();
            });
        })
    </script>
    <style>
        .add_user_div .div_20{
            margin-top: 20px;
        }
        .radio span{
            display: inline-block;
            
        }
        .add_user_div div.radio{
            margin: 0;
        }
        .add_user_div input,.radio,.add_user_div select{
            width: 300px;
            float: left;
        }
        .radio input{
            width: 20px;
        }
        .ml20{margin-left:20px;}
    </style>
</aside>