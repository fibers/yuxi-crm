<aside class="right-side">
    <section class="content-header">
        <h1>
            推荐管理
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> 首页</a></li>
            <li class="active">推荐管理</li>
        </ol>
    </section>
    <section class="content">
    <?php //print_r($selfstory);?>
		<div class="left">
			<p>剧的推荐</p>
			<ul>
		    	<!-- <li><span>搜索：</span><input type="text" value="" /><button class="search_1" type="button">搜索</button></li> -->
		    	<?php for($i=0;$i<3;$i++): ?>
		        	<li><span>ID：</span><input type="text" class="story_array" value="<?php echo isset($story[$i]['id'])?$story[$i]['id']:'';?>" /></li>
		        <?php endfor; ?>
		        
		        <li class="sure"><button class="sure_1 story_button" type="button">确定</button></li>
		    </ul>
		</div>
		<div class="left right">
			<p>自戏的推荐</p>
			<ul>
		    	<!-- <li><span>搜索：</span><input type="text" value="" /><button class="search_1" type="button">搜索</button></li> -->
		        <?php for($i=0;$i<3;$i++): ?>
		        	<li><span>ID：</span><input type="text" class="selfstory_array" value="<?php echo isset($selfstory[$i]['id'])?$selfstory[$i]['id']:'';?>" /></li>
		        <?php endfor; ?>
		        <li class="sure"><button class="sure_1 selfstory_button" type="button">确定</button></li>
		    </ul>
		</div>
		<div class="clear"></div>
    </section>
    <script type="text/javascript">

    	$(function(){
            $(".story_button").click(function(){
            	var value = '';
            	$(".story_array").each(function(){
                	if($(this).val()!='' && $(this).val()!=null )
                		value += $(this).val()+',';
                	
            	})
            	
                $.post('/priority/updatepriority', {selfstory: '0',ids:value}, function(data) {
                    if($.trim(data) == 'ok'){
                        alert('ok');
                    }else{
                    	alert('设置错误请重试');
                    }
                });
            })
			

            $(".selfstory_button").live("click",function(){
            	var value = '';
            	$(".selfstory_array").each(function(){
                	if($(this).val()!='' && $(this).val()!=null )
            			value += $(this).val()+',';
                	
            	})
            	
                $.post('/priority/updatepriority', {selfstory: '1',ids:value}, function(data) {
                    if($.trim(data) == 'ok'){
                        alert('ok');
                    }else{
                    	alert('设置错误请重试');
                    }
                });
            })
			
        })
    </script>
    <style>
		ul, ol, dl, dt, dd {list-style: none outside none;}
		.content{margin:0px auto;}
		.left{float:left;width:399px;border-right:#CCC 1px solid;}
		.left p{padding-top:20px;padding-left:20px;font-weight:bold;}
		.left ul{padding:20px;}
		.left ul li{padding-bottom:10px;}
		.left ul li input{border:none;border:#CCC 1px solid;height:24px;line-height:24px;padding:0px 4px;}
		.left ul li span{width:46px;float:left;text-align:right;}
		.left ul li button.search_1{border:none;width:70px;height:25px;background:#eb71a8;margin-left:4px;border-radius:4px;color:#FFF;font-weight:bold;cursor:pointer;} 
		.left ul li.sure{margin-left:45px;}
		.left ul li.sure button.sure_1{border:none;width:70px;height:25px;background:#eb71a8;margin-left:4px;border-radius:4px;color:#FFF;font-weight:bold;cursor:pointer;}
		.right{float:left;width:400px;border-right:0px;}
		.clear{clear:both;}
    </style>
</aside>    