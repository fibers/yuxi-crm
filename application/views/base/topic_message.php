<aside class="right-side">
	<section class="content-header">
		<h1>话题管理</h1>
		<ol class="breadcrumb">
			<li><a href="/"><i class="fa fa-dashboard"></i> 首页</a></li>
			<li class="active">话题管理</li>
		</ol>
	</section>
	<?php if($topic):?>
	<section class="content">
		<table class="table table-bordered" width='60%'>
			<thead>
				<tr>
					<th style="width:20%;">话题ID</th>
					<th><?php echo $topic['id'];?></th>
				</tr>
				<tr>
					<th style="width:20%;">主题名字</th>
					<th><?php echo $topic['name'];?></th>
				</tr>
			</thead>
		</table>
		<div class="heaher-title">评论信息</div>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>ID</th>
					<th>内容</th>
					<th>评论人</th>
					<th>评论时间</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<!-- <tr align="center"><td colspan="5"><img src="/ichees/images/loading.gif" /></td></tr> -->
				<?php if($message):?>
					<?php foreach ($message as $m):?>
					<tr data-id="<?php echo $m['id']?>">
						<td><?php echo $m['id']?></td>
						<td><?php echo $m['content']?></td>
						<td><?php echo $m['nick']?></td>
						<td><?php echo $m['created']?></td>
						<td><!-- <button class="mr10 btn btn-warning btn-sm edit_message">编辑</button> -->
							<button class="mr10 btn btn-danger btn-sm message_del">删除</button></td>
					</tr>
					<?php endforeach;?>
				<?php else:?>
					<tr align="center"><td colspan="5"><div class="heaher-title">数据为空</div></td></tr>
				<?php endif;?>
			</tbody>
		</table>
	</section>
	<?php else:?>
		<div class="heaher-title">数据错误！</div>
	<?php endif;?>
	<script type="text/javascript">
        $(function(){
            $(".message_del").live("click",function(){
                if(confirm('确认要删除吗？')){
                    var node = $(this).parents('tr')
                    $.post('/topic/message_del', {id: node.data('id'),groupId:<?php echo $topic['id'];?>}, function(data) {
                        if($.trim(data) == 'success'){
                            node.fadeOut();
                        }
                    });
                }
            });
        })
    </script>
<style>
.add_discovery_div input {
	margin-top: 20px;
}

.mr10 {
	margin-right: 10px;
	margin-top: 10px;
}
.heaher-title{
	font-size:18px;font-weight: bold;
}
</style>
</aside>
