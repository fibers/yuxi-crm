<aside class="right-side">
    <section class="content-header">
        <h1>
            话题管理
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> 首页</a></li>
            <li class="active">话题管理</li>
        </ol>
    </section>
    <section class="content">
        <input name="title" id="kwdtitle" value="" type="text" />
        <button style="margin-bottom:10px;" class="btn btn-primary btn-sm searchkey pull-center">搜索</button>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th><a class="idclassbtn" style="cursor:pointer;">ID</a></th>
                    <th>主题名字</th>
                    <th>主题的图片</th>
                    <th>创建者名字</th>    
                    <th>内容</th>  
                    <th><a class="odclassbtn" style="cursor:pointer;">排序号<a></th>
                    <th width="12%">操作</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        
        
         <ul class="pagination">
            <li class="prev_page">
                <a href="javascript:;">上一页</a>
            </li>
            <li class="curr_page">
              	 <a href="javascript:;"></a>
            </li>
            <li class="show_page">
                <a href="javascript:;"></a>
            </li>
            <li class="next_page">
                <a href="javascript:;">下一页</a>
            </li>
            <li class="jump_page">
                <span><input type="text" value="" class="width20 go_page_input" /></span>
                <span class="go_page cursor">go</span>
            </li>
        </ul>
    </section>
    <script type="text/javascript">
        var Topic = {
            compiledTpl : null,
            compiledAddTpl : null,
            getTopic : function(page,od){
                if(!page || page<0){
                    page = 0;
                }

                /*搜索关键字*/
                var name = $('#kwdtitle').val();
                if(!name){
                	name ='';
                }
				var  gjurl = (od!=''?'/topic/all/od/priority':'/topic/all/');
                $.getJSON('/topic/all', {
                    start : page,od:od,name:name
                }, function(data) {
                    
                     $("table").data('topics',data.departs);
                    Topic.tpl(data.groups);
                   // console.log(data);
                    var _tpl = Topic.compiledTpl;
                    $("tbody").html(_tpl);
                    $(".pagination").data('page',data.page);
                    if(data.page == 0){
                        $(".pagination .prev_page").addClass("disabled");   
                    }else{
                        $(".pagination .prev_page").removeClass("disabled").data('page',data.page-0-1);   
                    }
                    if(data.page == (data.count-1)){
                        $(".pagination .next_page").addClass("disabled");   
                    }else{
                        $(".pagination .next_page").removeClass("disabled").data('page',data.page-0+1);   
                    }
                    $(".pagination .show_page a").text('共'+data.count+'页');
                    $(".pagination .curr_page a").text('第'+(data.page-0+1)+'页');
                    $(".pagination .curr_page").data('page',data.page);
                });
            },
            
            tpl : function(data){
                var tplstr = '';
                $.each(data,function(i,dom){
                   tplstr+=' <tr data-id="'+dom.id+'">';
                   tplstr+= '      <td>'+dom.id+'</td>';
                   tplstr+= '      <td>'+dom.name+'</td>';
                   tplstr+= '      <td><img width="50" height="50" src="'+dom.avatar+'" /></td>';
                   tplstr+= '      <td>'+dom.nick+'</td>';    
                   tplstr+='       <td>'+dom.intro+'</td>';
                   tplstr+='       <td>'+dom.priority+'</td>';
                   tplstr+='       <td><button href="javascript:;" class="mr10 btn btn-danger btn-sm del_discovery">删除</button>';
                   tplstr+='<button class="mr10 btn btn-warning btn-sm edit_topic">编辑</button><a class=" mr10 btn btn-warning btn-sm" target="_blank" href="/topic/topicMessage?groupid='+dom.id+'">查看评论</a></td>';
                   tplstr+= '   </tr>';
                     });
                     Topic.compiledTpl = tplstr; 
                
            },
           editTopicAlert : function(node){
                Topic.addTpl();
                $.fn.SimpleModal({
                    btn_ok: '编辑',
                    model: 'confirm',
                    callback: function(node){
                        Topic.editStory();
                    },
                    overlayClick: false,
                    width: 660,
                    title: '编辑排名顺序',
                    contents: Topic.compiledAddTpl.render({'departs':$("table").data('departs')})
                }).showModal();
                $.post('/topic/get', {
                    id:node.data('id')
                }, function(data) {
                    var data = JSON.parse(data);
                    $(".priority").val(data.priority);
                    $(".image").val(data.avatar);
                    $(".btn-margin").addClass("btn-margin-edit");
                    $(".btn-margin-edit").data('id',node.data('id'));

                    // $('.avatar_show').Jcrop({
                    //     setSelect:   [ 0, 0, 100, 100 ],
                    //     aspectRatio: 1/1,
                    //     onChange: function(c){
                    //         $(".crop_info").val(JSON.stringify(c));
                    //     }
                    // });
                });
                
            },
            editStory : function(){
                $.post('/topic/edit', {
                    id:$(".btn-margin-edit").data('id'),
                    avatar:$(".image").val(),
                    priority: $(".priority").val()
                }, function(data) {
                    if($.trim(data) == 'success'){
                        $.fn.hideModal();
                        $(".content").before('<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>编辑成功</div>')
                        setTimeout(function(){
                            $(".alert").remove();
                        },3000);
                        var page=$(".curr_page").data('page');
                        Topic.getTopic(page);
                    }else{
                        $(".btn-margin-edit").text('编辑失败');
                    }
                });
            },
             addTpl : function(){
                var tpl =[
                    '<div class="add_user_div" role="form">',
                    '    <div class="clearfix div_20">',
                    '        <input type="text" class="form-control input-sm priority pull-left" placeholder="排序号">',  
                    '		 <span class="span pull-left">图片：</span>',
                    '   	 <input type="file" id="file_image" class="file_image" name="file_image">',
                    '		 <span class="span-load"></span><img src="/ichees/images/loading.gif" class="image_loading" />',
					'   	 <input type="hidden" id="image" class="image" name="image">',                
                    '    </div>',
                    '   <div class="hide crop_info"></div>',
                    // '    <div class="clearfix">',
                    // '        <img src="" style="height:100px;margin-left:20px;" class="avatar_show hide">',
                    '        <img src=""  class="avatar_show hide" style="max-width:100px;">',
                    // '       <a href="javascript:;" class="start_upload">上传</a>',
                    // '    </div>',
                    '</div>'
                ].join('\n');
                Topic.compiledAddTpl = juicer(tpl);
            },

            upload : function(node){
                var file = $('.file_image')[0].files[0];
                var reader = new FileReader();
                reader.onload = function (rResult) {
                	var strload = '正在上传图片，请稍后...';
                	$('.image_loading').show();
                	$(".span-load").text(strload);
                    var filename = file.name;
                    var options = {
                        type: 'POST',
                        url: '/common/upload?filename='+filename,
                        data: reader.result,
                        success:function(result){
                            var data = result;
                            if(data.status == 'success'){
                                $(".image").val(data.file);
                                $('.image_loading').hide();
                                $(".span-load").html('<span style="color:green">图片上传成功</span>');
                            }else{
                                $('.image_loading').hide();
                                $(".span-load").html('<span style="color:red">图片上传失败</span>');
                            }
                        },
                        processData: false,
                        contentType: false,
                        dataType:"json"
                    };
                    $.ajax(options);
                };
                reader.readAsArrayBuffer(file);
           }
           
        }
        
        $(function(){
        	var page = $(".pagination").data('page');
        	if(!page)
            	page = 0;
            Topic.getTopic(page,'');

			$(".file_image").live("change",function(){
				Topic.upload();
            })

            $(".edit_discovery").live("click",function(){
                Topic.editiscoveryAlert($(this).parents('tr'));
            })

            $(".del_discovery").live("click",function(){
                if(confirm('确认要删除吗？')){
                    var node = $(this).parents('tr')
                    $.post('/topic/del', {id: node.data('id')}, function(data) {
                        if($.trim(data) == 'success'){
                            node.fadeOut();
                        }
                    });
                }
                
            });
            
            
            //set recommond
           /*   $(".settop").live("click",function(){
                  var _this = $(this);
                if(confirm('确认要置顶？')){
                    var node = $(this).parents('tr')
                    $.post('/topic/settop', {id: node.data('id')}, function(data) {
                        if($.trim(data) == 'success'){
                         _this.attr('class','mr10 btn btn-warning btn-sm unsettop');
                          _this.text('取消置顶');
                        }else if($.trim(data) == 'full'){
                            alert('置顶总数不能超过三个');
                        }else{
                            alert('置顶失败，请稍后重试');
                        }
                    });
                }
                
            });
            
            
            $(".unsettop").live("click",function(){
                var _this = $(this);
                if(confirm('确认要取消置顶？')){
                    var node = $(this).parents('tr')
                    $.post('/topic/unsettop', {id: node.data('id')}, function(data) {
                        if($.trim(data) == 'success'){
                             _this.attr('class','mr10 btn btn-warning btn-sm settop');
                           _this.text('置顶');
                        }
                    });
                }
                
            });*/
            
            
			$(".go_page").click(function(){
                var page=$(".go_page_input").val();
                Topic.getTopic(page-1);
            })
            
            $(".next_page,.prev_page").live("click",function(){
                Topic.getTopic($(this).data('page'),'');
            });
            
            
            $('.searchkey').live('click',function(){
                Topic.getTopic(0,'');
                
                /*if(keywd == '' || keywd == null){
                    alert('请输入关键词');return ;
                }
               $.getJSON('/topic/search', {kwd: keywd}, function(data) {
                            $('.pagination').hide();
                            if(data.status == 'ok'){
                                Topic.tpl(data.groups);
                                var _tpl = Topic.compiledTpl;
                                $("tbody").html(_tpl);
                            }else{
                                 $("tbody").html('');
                            }
                             
                        
                    });*/
                
                
            });
            
            
             $(".edit_topic").live("click",function(){
                Topic.editTopicAlert($(this).parents('tr'));
            });


			$(".odclassbtn").live("click",function(){
			
			    Topic.getTopic(0,'priority');
			});

			$(".idclassbtn").live("click",function(){
			
			    Topic.getTopic(0,'');
			});
            
        })
    </script>
    <style>
        .add_discovery_div input,.file_image,.span{ 
            margin-top: 20px;
        }
        .mr10{margin-right: 10px;margin-top: 10px;}
		.file_image{margin-left:20px;margin-bottom:20px;float:left;}
		.span{display: block; clear: both;}
        .image_loading{width:20px;height:20px;float:left;display:none;}
        .span-load span{margin-top: 20px; display: inline-block; clear: both;}
		.width20{width:40px;height:20px;}
        .cursor{cursor: pointer;}
    </style>
</aside>    