<script src="/ui/js/jquery.Jcrop.js"></script>
<link rel="stylesheet" href="/ui/css/jquery.Jcrop.css" type="text/css" />
<aside class="right-side">
    <section class="content-header">
        <h1>
            新增家族统计
            <small>欢迎来到IM</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> 首页</a></li>
            <li class="active">新增家族统计</li>
        </ol>
    </section>
    <section class="content">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>
						<iframe src="http://123.57.177.2:5601/#/dashboard/New-Family-Dashboard?embed&_g=(refreshInterval:(display:'1%20minute',pause:!f,section:2,value:60000),time:(from:now%2Fy,mode:quick,to:now%2Fy))&_a=(filters:!(),panels:!((col:1,id:Daily-New-Families,row:1,size_x:12,size_y:5,type:visualization),(col:8,id:Monthly-New-Families,row:6,size_x:5,size_y:4,type:visualization),(col:1,id:Weekly-New-Families,row:6,size_x:7,size_y:4,type:visualization)),query:(query_string:(analyze_wildcard:!t,query:'*')),title:'New%20Families%20Dashboard')" 
						height="900" width="100%"></iframe>
                    </th>
                   
                </tr>
                 
            </thead>
            <tbody>
            </tbody>
        </table>
    </section>
   
    <style>
        .add_user_div .div_20{
            margin-top: 20px;
        }
        .radio span{
            display: inline-block;
            width: 50px;
        }
        .add_user_div div.radio{
            margin: 0;
        }
        .add_user_div input,.radio,.add_user_div select{
            width: 300px;
            float: left;
        }
        .radio input{
            width: 20px;
        }
    </style>
</aside>