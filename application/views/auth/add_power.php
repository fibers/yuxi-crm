<aside class="right-side">
    <section class="content-header">
        <h1>权限管理</h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> 首页</a></li>
            <li class="active">权限管理</li>
        </ol>
    </section>
    <section class="content">
    <form action="/auth_power/save_power" method="post" >
        <!-- 权限显示 -->
	    <div id="power" style="margin-left:120px;clear: both;">
	      <div style="display:block; overflow:auto; width:500px; height:460px;border:solid 1px #CCC; line-height:21px;">
	        <table style="margin-left:20px;">
	        <?php $rolecode = ','.$permission['rolecode'].',';
	        	foreach ($plist as $l):?>
	          <tr>
	            <td ><input type="checkbox" name="power[]"  onclick="selectChild(this);" value="<?php echo $l['id'];?>" <?php echo strstr($rolecode, ','.$l['id'].',')?"checked":'';?>   />
	              <span><?php echo $l['name']; ?></span></td>
	          </tr>
	          <tr>
		      	<td>
			        <?php if(isset($l[1])):?>
		          	<?php foreach ($l[1] as $ll):?>
		          		<table>
			                <tr>
			                  <td>&nbsp;&nbsp;&nbsp;&nbsp;
			                    <input type="checkbox" name="power[]"  onclick="selectChild2(this);" value="<?php echo $ll['id'];?>" <?php echo strstr($rolecode, ','.$ll['id'].',')?"checked":'';?> />
			                    <span><?php echo $ll['name'];?></span></td>
			                </tr>
			                <!-- 三级循环
			                <tr>
			                  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			                    <input type="checkbox" name="power[]"   onclick="selectChild3(this);" value="7" checked='checked' />
			                    修改密码<br /></td>
			                </tr> -->
			            </table>
			          <?php endforeach; ?>
			          <?php endif; ?>
		          </td>
		       </tr>
	          <?php endforeach; ?>
	        </table>
	      </div>
	      <input type="hidden" name="hid" value="<?php echo $permission['id'];?>" /> 
	      <input type="submit" style="margin-top:10px;" class="btn btn-primary btn-sm pull-center" value="保存" />
	      
	    </div>
	    <!-- 权限显示END -->
    </form>
    </section>
	<script>
        //一级菜单
        function selectChild(o){
            //获得本菜单的tr
            //alert($(o).attr("checked"));
            var thisTr=$(o).parent().parent();
            //获得子菜单的tr
            var i=0;
            //alert(thisTr.next().children('td').children().html());
            if(thisTr.next().children('td').children().is('table')){
                thisTr.next().contents().find('input').each(function (){
                    //遍历子菜单的checkbox
                    //alert(this);
                    if($(o).attr("checked")==='checked'){
                        this.checked='checked';
                    }else{
                        this.checked=false;
                    }
                });
            }else{
                //无子级时，不让选中
                //alert(thisTr.next().children('td').children().html());
                //$(o).attr("checked",false);
            }

        }
        function selectChild2(o){
            //获得当前所在TR
            var thisTr=$(o).parent().parent();
            //获取一级菜单

            var parentTr= thisTr.parent().parent().parent().parent().prev();	//这里为什么有4个parent,是因为table里有个默认的tbody。

            if(thisTr.next().children('td').children().is('div')){

            }else{
                thisTr.next().children('td').children('input').each(function (){
                    //遍历子菜单的checkbox
                    if($(o).attr("checked")==='checked'){
                        this.checked='checked';
                    }else{
                        this.checked=false;
                    }
                });
            }

            //处理父级
            if($(o).attr("checked")==='checked'){
                if(parentTr.children('td').children('input').attr("checked")===undefined ){
                    parentTr.children('td').children('input').attr("checked", $(o).attr("checked"));
                }
            }else{
                //当二级被点击取消的时候，一级是不是被选中状态，如果是，则看一级下面的checkbox是否有的处于选中状态

                if(parentTr.children('td').children('input').attr("checked")==='checked'){
                    var i=0;
                    parentTr.next().contents().find('table tr td input').each(function (){
                        //遍历子菜单的checkbox
                        if($(this).attr("checked")==='checked'){
                            i++;
                        }
                    });
                    if(i===0){
                        parentTr.children('td').children('input').attr("checked", false);
                    }else{
                        parentTr.children('td').children('input').attr("checked", true);
                    }
                }
            }
        }

        //处理三级
        function selectChild3(o){
            //alert('w');
            //获得当前所在TR
            var thisTr=$(o).parent().parent();
            //处理二级
            var TwoTr=thisTr.prev();
            var parentTr= thisTr.parent().parent().parent().parent().prev();
            //判断是选中还是取消选中
            if($(o).attr("checked")==='checked'){
                //如果是选中看二级是不是选中
                if(TwoTr.children('td').children('input').attr("checked")===undefined){
                    TwoTr.children('td').children('input').attr("checked",true);
                }
                //判断一级是否被选中
                if(parentTr.children('td').children('input').attr("checked")===undefined ){
                    parentTr.children('td').children('input').attr("checked", true);
                }
            }else{
                //如果是取消选中，如果上一级是选中，看该级下面是否有选中的
                if(TwoTr.children('td').children('input').attr("checked")==='checked'){
                    //TwoTr.children('td').children('input').attr("checked",true);
                    var i=0;
                    thisTr.children('td').children('input').each(function (){
                        if($(this).attr('checked')==='checked'){
                            i++;
                        }
                    });
                    if(i===0){
                        TwoTr.children('td').children('input').attr("checked",false);
                    }else{
                        TwoTr.children('td').children('input').attr("checked",true);
                    }
                }
                //selectChild2(o,'333');
                if(parentTr.children('td').children('input').attr("checked")==='checked'){
                    var i=0;
                    parentTr.next().contents().find('table tr td input').each(function (){
                        //遍历子菜单的checkbox
                        if($(this).attr("checked")==='checked'){
                            i++;
                        }
                    });
                    if(i===0){
                        parentTr.children('td').children('input').attr("checked", false);
                    }else{
                        parentTr.children('td').children('input').attr("checked", true);
                    }
                }
            }

        }

        //============================checkbox三级联动JS操作END=======================
    </script>
  
</aside>
