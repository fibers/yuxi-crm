<script src="/ui/js/jquery.Jcrop.js"></script>
<link rel="stylesheet" href="/ui/css/jquery.Jcrop.css" type="text/css" />
<aside class="right-side">
    <section class="content-header">
        <h1>
            权限设置
            <small>欢迎来到IM</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> 首页</a></li>
            <li class="active">权限设置</li>
        </ol>
    </section>
    <section class="content">
    	<input id="keyrolename" placeholder="名称" value="" type="text" />
        <button style="margin-bottom:6px;" class="btn btn-primary btn-sm searchkey pull-center">搜索</button>
        <button style="margin-bottom:10px;" class="btn btn-primary btn-sm add_Admin pull-right">新增</button>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>名称</th>
                    <th>编辑</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <ul class="pagination">
            <li class="prev_page">
                <a href="javascript:;">上一页</a>
            </li>
            <li class="show_page">
                <a href="javascript:;"></a>
            </li>
            <li class="next_page">
                <a href="javascript:;">下一页</a>
            </li>
        </ul>
    </section>
    <script>
        var Admin = {
            compiledTpl : null,
            compiledAddTpl : null,
            getAdmin : function(page){
                if(!page){
                    page = 0;
                }
                var keyrolename = $('#keyrolename').val();
                
                $.getJSON('/auth_power/all', {
                    start : page,keyrolename: keyrolename
                }, function(data) {
                    Admin.tpl();
                    var _tpl = Admin.compiledTpl.render(data);
                    $("tbody").html(_tpl);
                    if(data.page == 0){
                        $(".pagination .prev_page").addClass("disabled");   
                    }else{
                        $(".pagination .prev_page").removeClass("disabled").data('page',data.page-0-1);   
                    }
                    if(data.page == (data.count-1)){
                        $(".pagination .next_page").addClass("disabled");   
                    }else{
                        $(".pagination .next_page").removeClass("disabled").data('page',data.page-0+1);   
                    }
                    $(".pagination .show_page a").text('共'+data.count+'页');
                });
            },
            tpl : function(){
                var tpl=[
                    '{@each permissions as admin}',
                    '   <tr data-id="${admin.id}" >',
                    '       <td>${admin.id}</td>',
                    '       <td>${admin.rolename}</td>',
                    '       <td>{@if(admin.rolecode!="all")}<button style="margin-right:10px;" class="btn btn-warning btn-sm edit_Admin">编辑</button>',
                    '		<button href="javascript:;" class="btn btn-danger btn-sm del_Admin">删除</button>',
                    '		<a href="/auth_power/add_power?id=${admin.id}" style="margin-left:10px;" class="btn btn-warning btn-sm edit_pession">编辑权限</a>{@/if}</td>',
                    '   </tr>',
                    '{@/each}'
                ].join('\n');
                Admin.compiledTpl = juicer(tpl);
            },
            delAdmin : function(node){
                $.post('/auth_power/del', {id: node.data('id')}, function(data) {
                    if($.trim(data) == 'success'){
                        node.fadeOut();
                    }
                });
            },
            editAdmin : function(){
            	var rolename = $(".rolename").val();
            	
                $.post('/auth_power/edit', {
                    id:$(".btn-margin-edit").data('id'),
                    rolename: rolename
                }, function(data) {
                    if($.trim(data) == 'success'){
                        $.fn.hideModal();
                        $(".content").before('<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>编辑成功</div>')
                        setTimeout(function(){
                            $(".alert").remove();
                        },3000);
                        Admin.getAdmin();
                    }else{
                        $(".btn-margin-edit").text('编辑失败');
                    }
                });
            },
            addAdmin : function(){
            	var rolename = $(".rolename").val();
            	
                $.post('/auth_power/add', {
                	rolename: rolename,
                }, function(data) {
                    if($.trim(data) == 'success'){
                        $.fn.hideModal();
                        $(".content").before('<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>添加成功</div>')
                        setTimeout(function(){
                            $(".alert").remove();
                            Admin.getAdmin();
                        },3000);
                    }else{
                        $(".btn-margin").text('添加失败');
                    }
                });
            },
            editAdminAlert : function(node){
                Admin.addTpl();
                $.fn.SimpleModal({
                    btn_ok: '编辑',
                    model: 'confirm',
                    callback: function(node){
                        Admin.editAdmin();
                    },
                    overlayClick: false,
                    width: 660,
                    title: '编辑权限',
                    contents: Admin.compiledAddTpl.render({'permissions':$("table").data('permissions')})
                }).showModal();
                $.post('/auth_power/get', {
                    id:node.data('id')
                }, function(data) {
                    var data = JSON.parse(data);
                    //$("#radio"+data.sex).attr("checked","checked");
                    $(".rolename").val(data.rolename);
					//$(".uname").val(data.uname);
					//$(".permission").val(data.permission);
                    $(".btn-margin").addClass("btn-margin-edit");
                    $(".btn-margin-edit").data('id',node.data('id'));
                });
                
            },
            addAdminAlert : function(){
                Admin.addTpl();
                $.fn.SimpleModal({
                    btn_ok: '添加',
                    model: 'confirm',
                    callback: function(node){
                        Admin.addAdmin();
                    },
                    overlayClick: false,
                    width: 660,
                    title: '添加权限',
                    contents: Admin.compiledAddTpl.render({'permissions':$("table").data('permissions')})
                }).showModal();
            },
            addTpl : function(){
                var tpl =[
                    '<div class="add_Admin_div" role="form">',
                    '    <div class="clearfix div_20">',
                    '        <input type="text" class="form-control input-sm rolename pull-left" placeholder="名称">',
                    //'        <input type="text" style="margin-left:20px;" class="form-control input-sm uname" placeholder="登录账号">',
                    '    </div>',
                   /* '    <div class="clearfix div_20">',
                    '        <input type="password" class="form-control input-sm pwd" placeholder="密码">',
                    '        <select class="form-control permission input-sm" style="margin-left:20px;">',
                    '            {@each permissions as permission}',
                    '                <option value="${permission.id}">${permission.rolename}</option>',
                    '            {@/each}',
                    '        </select>',
                    // '        <input type="text" style="margin-left:20px;" class="form-control input-sm position" placeholder="地址">',
                    '    </div>',*/
                    '</div>'
                ].join('\n');
                Admin.compiledAddTpl = juicer(tpl);
            }
        }
        $('.searchkey').live('click',function(){
        	Admin.getAdmin();
        });
        
        $(function(){

            Admin.getAdmin();

            $(".next_page,.prev_page").live("click",function(){
                Admin.getAdmin($(this).data('page'));
            })

            $(".del_Admin").live("click",function(){
                Admin.delAdmin($(this).parents('tr'));
            })

            $(".add_Admin").click(function(){
                Admin.addAdminAlert()
            })

            $(".edit_Admin").live("click",function(){
                Admin.editAdminAlert($(this).parents('tr'));
            })
        })
    </script>
    <style>
        .add_Admin_div .div_20{
            margin-top: 20px;
        }
        .radio span{
            display: inline-block;
            width: 50px;
        }
        .add_Admin_div div.radio{
            margin: 0;
        }
        .add_Admin_div input,.radio,.add_Admin_div select{
            width: 300px;
            float: left;
        }
        .radio input{
            width: 20px;
        }
    </style>
</aside>