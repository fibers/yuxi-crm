<script src="/ui/js/jquery.Jcrop.js"></script>
<link rel="stylesheet" href="/ui/css/jquery.Jcrop.css" type="text/css" />
<aside class="right-side">
    <section class="content-header">
        <h1>
            修改密码
            <small>欢迎来到IM</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> 首页</a></li>
            <li class="active">修改密码</li>
        </ol>
    </section>
    <section class="content">
    	
        
        <table class="table table-bordered" style="width:40%">
            <tbody>
            	<tr>
                    <th>原密码：</th>
                    <th><input type="password" class="form-control input-sm yuanpwd"></th>
                </tr>
                <tr>
                    <th>新密码：</th>
                    <th><input type="password" class="form-control input-sm newpwd"></th>
                </tr>
                <tr>
                    <th>确认密码：</th>
                    <th><input type="password" class="form-control input-sm aginpwd"></th>
                </tr>
                
            </tbody>
        </table>
        <button style="margin-left:200px;" class="btn btn-primary btn-sm save_pwd">保存</button>
    </section>
    <script>
        var Pwd = {
            savePwd : function(){
            	var yuanpwd=$('.yuanpwd').val();
            	var newpwd=$('.newpwd').val();
            	var aginpwd=$('.aginpwd').val();
                $.post('/auth/save_pwd', {yuanpwd:yuanpwd,newpwd:newpwd,aginpwd:aginpwd}, function(data) {
                	alert($.trim(data));
                    if($.trim(data)=='success'){
                    	window.location.href="/home";
                    }
                });
            }
        }
        
        $(function(){
            $(".save_pwd").live("click",function(){
                Pwd.savePwd();
            })
        })
    </script>
    <style>
        .add_Admin_div .div_20{
            margin-top: 20px;
        }
        .radio span{
            display: inline-block;
            width: 50px;
        }
        .add_Admin_div div.radio{
            margin: 0;
        }
        .add_Admin_div input,.radio,.add_Admin_div select{
            width: 300px;
            float: left;
        }
        .radio input{
            width: 20px;
        }
    </style>
</aside>