<script src="/ui/js/jquery.Jcrop.js"></script>
<link rel="stylesheet" href="/ui/css/jquery.Jcrop.css" type="text/css" />
<aside class="right-side">
    <section class="content-header">
        <h1>
            管理员管理
            <small>欢迎来到IM</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> 首页</a></li>
            <li class="active">管理员管理</li>
        </ol>
    </section>
    <section class="content">
    	<a href="auth_power" style="margin-bottom:10px;" class="btn btn-primary btn-sm auth_power pull-right">权限设置</a><br/><br/>
    	<input id="keyID" placeholder="ID" value="" type="text" />
    	<input id="keyuname" placeholder="登陆账号" value="" type="text" />
        <button style="margin-bottom:6px;" class="btn btn-primary btn-sm searchkey pull-center">搜索</button>
        <button style="margin-bottom:10px;" class="btn btn-primary btn-sm add_Admin pull-right">新增</button>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>权限</th>
                    <th>姓名</th>
                    <th>登陆账号</th>
                    <th>最后一次登陆</th>
                    <th>编辑</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <ul class="pagination">
            <li class="prev_page">
                <a href="javascript:;">上一页</a>
            </li>
            <li class="show_page">
                <a href="javascript:;"></a>
            </li>
            <li class="next_page">
                <a href="javascript:;">下一页</a>
            </li>
        </ul>
    </section>
    <script>
        var Admin = {
            compiledTpl : null,
            compiledAddTpl : null,
            getAdmin : function(page){
                if(!page){
                    page = 0;
                }
                var keyID = $('#keyID').val();
                var keyuname = $('#keyuname').val();
                if(!keyID){
                	keyID='';
				}
                if(!keyuname){
                	keyuname='';
				}
                
                $.getJSON('/admin/all', {
                    start : page,keyID: keyID,keyuname: keyuname
                }, function(data) {
                    Admin.tpl();
                    var _tpl = Admin.compiledTpl.render(data);
                    $("table").data('permissions',data.permissions);
                    $("tbody").html(_tpl);
                    if(data.page == 0){
                        $(".pagination .prev_page").addClass("disabled");   
                    }else{
                        $(".pagination .prev_page").removeClass("disabled").data('page',data.page-0-1);   
                    }
                    if(data.page == (data.count-1)){
                        $(".pagination .next_page").addClass("disabled");   
                    }else{
                        $(".pagination .next_page").removeClass("disabled").data('page',data.page-0+1);   
                    }
                    $(".pagination .show_page a").text('共'+data.count+'页');
                });
            },
            delAdmin : function(node){
                $.post('/admin/del', {id: node.data('id')}, function(data) {
                    if($.trim(data) == 'success'){
                        node.fadeOut();
                    }
                });
            },
            editAdmin : function(){
            	var uname = $(".uname").val();
            	var permission = $(".permission").val();
            	if(uname=='' || uname==null || permission=='' || permission==null){
					alert('信息不完善');
					return false;
                }
                $.post('/admin/edit', {
                    id:$(".btn-margin-edit").data('id'),
                    permission: permission,
                    uname: uname,
					nick: $(".nick").val(),
                    pwd: $(".pwd").val(),
                }, function(data) {
                    if($.trim(data) == 'success'){
                        $.fn.hideModal();
                        $(".content").before('<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>编辑成功</div>')
                        setTimeout(function(){
                            $(".alert").remove();
                        },3000);
                        Admin.getAdmin();
                    }else{
                        $(".btn-margin-edit").text('编辑失败');
                    }
                });
            },
            addAdmin : function(){
            	var uname = $(".uname").val();
            	var permission = $(".permission").val();
            	var pwd = $(".pwd").val();
            	if(uname=='' || uname==null || pwd=='' || pwd==null || permission=='' || permission==null){
					alert('信息不完善');
					return false;
                }
                $.post('/admin/add', {
                	permission: permission,
                    uname: $(".uname").val(),
					nick: $(".nick").val(),
                    pwd: pwd,
                }, function(data) {
                    if($.trim(data) == 'success'){
                        $.fn.hideModal();
                        $(".content").before('<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>添加成功</div>')
                        setTimeout(function(){
                            $(".alert").remove();
                            Admin.getAdmin();
                        },3000);
                    }else{
                        $(".btn-margin").text('添加失败');
                    }
                });
            },
            editAdminAlert : function(node){
                Admin.addTpl();
                $.fn.SimpleModal({
                    btn_ok: '编辑',
                    model: 'confirm',
                    callback: function(node){
                        Admin.editAdmin();
                    },
                    overlayClick: false,
                    width: 660,
                    title: '编辑管理员',
                    contents: Admin.compiledAddTpl.render({'permissions':$("table").data('permissions')})
                }).showModal();
                $.post('/admin/get', {
                    id:node.data('id')
                }, function(data) {
                    var data = JSON.parse(data);
                    $("#radio"+data.sex).attr("checked","checked");
                    $(".nick").val(data.nick);
					$(".uname").val(data.uname);
					$(".permission").val(data.permission);
                    $(".btn-margin").addClass("btn-margin-edit");
                    $(".btn-margin-edit").data('id',node.data('id'));
                });
                
            },
            addAdminAlert : function(){
                Admin.addTpl();
                $.fn.SimpleModal({
                    btn_ok: '添加',
                    model: 'confirm',
                    callback: function(node){
                        Admin.addAdmin();
                    },
                    overlayClick: false,
                    width: 660,
                    title: '添加管理员',
                    contents: Admin.compiledAddTpl.render({'permissions':$("table").data('permissions')})
                }).showModal();
            },
            addTpl : function(){
                var tpl =[
                    '<div class="add_Admin_div" role="form">',
                    '    <div class="clearfix div_20">',
                    '        <input type="text" class="form-control input-sm nick pull-left" placeholder="姓名">',
                    '        <input type="text" style="margin-left:20px;" class="form-control input-sm uname" placeholder="登录账号">',
                    '    </div>',
                    '    <div class="clearfix div_20">',
                    '        <input type="password" class="form-control input-sm pwd" placeholder="密码">',
                    '        <select class="form-control permission input-sm" style="margin-left:20px;">',
                    '            {@each permissions as permission}',
                    '                <option value="${permission.id}">${permission.rolename}</option>',
                    '            {@/each}',
                    '        </select>',
                    // '        <input type="text" style="margin-left:20px;" class="form-control input-sm position" placeholder="地址">',
                    '    </div>',
                    '</div>'
                ].join('\n');
                Admin.compiledAddTpl = juicer(tpl);
            },
            tpl : function(){
                var tpl=[
                    '{@each admins as admin}',
                    '   <tr data-id="${admin.id}" >',
                    '       <td>${admin.id}</td>',
                    '       <td>${admin.rolename}</td>',
                    '       <td>${admin.nick}</td>',
                    '       <td>${admin.uname}</td>',
                    '       <td>${admin.updated}</td>',
                    '       <td>{@if(admin.permission!=1)}<button style="margin-right:10px;" class="btn btn-warning btn-sm edit_Admin">编辑</button><button href="javascript:;" class="btn btn-danger btn-sm del_Admin">删除</button>{@/if}</td>',
                    '   </tr>',
                    '{@/each}'
                ].join('\n');
                Admin.compiledTpl = juicer(tpl);
            }
        }
        $('.searchkey').live('click',function(){
        	Admin.getAdmin();
        });
        
        $(function(){

            Admin.getAdmin();

            $(".next_page,.prev_page").live("click",function(){
                Admin.getAdmin($(this).data('page'));
            })

            $(".del_Admin").live("click",function(){
                Admin.delAdmin($(this).parents('tr'));
            })

            $(".add_Admin").click(function(){
                Admin.addAdminAlert()
            })

            $(".edit_Admin").live("click",function(){
                Admin.editAdminAlert($(this).parents('tr'));
            })
        })
    </script>
    <style>
        .add_Admin_div .div_20{
            margin-top: 20px;
        }
        .radio span{
            display: inline-block;
            width: 50px;
        }
        .add_Admin_div div.radio{
            margin: 0;
        }
        .add_Admin_div input,.radio,.add_Admin_div select{
            width: 300px;
            float: left;
        }
        .radio input{
            width: 20px;
        }
    </style>
</aside>